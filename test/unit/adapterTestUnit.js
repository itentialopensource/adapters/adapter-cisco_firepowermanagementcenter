/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-cisco_firepowermanagementcenter',
      type: 'FirepowerManagementCenter',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const FirepowerManagementCenter = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Firepower_management_center Adapter Test', () => {
  describe('FirepowerManagementCenter Class Tests', () => {
    const a = new FirepowerManagementCenter(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('cisco_firepowermanagementcenter'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('cisco_firepowermanagementcenter'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('FirepowerManagementCenter', pronghornDotJson.export);
          assert.equal('Firepower_management_center', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-cisco_firepowermanagementcenter', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('cisco_firepowermanagementcenter'));
          assert.equal('FirepowerManagementCenter', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-cisco_firepowermanagementcenter', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-cisco_firepowermanagementcenter', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#createDeviceCopyConfigRequest - errors', () => {
      it('should have a createDeviceCopyConfigRequest function', (done) => {
        try {
          assert.equal(true, typeof a.createDeviceCopyConfigRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createDeviceCopyConfigRequest(null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createDeviceCopyConfigRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDeviceCopyConfigRequest('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createDeviceCopyConfigRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllDevice - errors', () => {
      it('should have a getAllDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getAllDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllDevice(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDevice - errors', () => {
      it('should have a createDevice function', (done) => {
        try {
          assert.equal(true, typeof a.createDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createDevice(null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFTDBridgeGroupInterface - errors', () => {
      it('should have a getAllFTDBridgeGroupInterface function', (done) => {
        try {
          assert.equal(true, typeof a.getAllFTDBridgeGroupInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllFTDBridgeGroupInterface(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFTDBridgeGroupInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllFTDBridgeGroupInterface('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFTDBridgeGroupInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFTDBridgeGroupInterface - errors', () => {
      it('should have a createFTDBridgeGroupInterface function', (done) => {
        try {
          assert.equal(true, typeof a.createFTDBridgeGroupInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.createFTDBridgeGroupInterface(null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFTDBridgeGroupInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createFTDBridgeGroupInterface('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFTDBridgeGroupInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createFTDBridgeGroupInterface('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFTDBridgeGroupInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFTDBridgeGroupInterface - errors', () => {
      it('should have a getFTDBridgeGroupInterface function', (done) => {
        try {
          assert.equal(true, typeof a.getFTDBridgeGroupInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getFTDBridgeGroupInterface(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDBridgeGroupInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getFTDBridgeGroupInterface('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDBridgeGroupInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getFTDBridgeGroupInterface('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDBridgeGroupInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFTDBridgeGroupInterface - errors', () => {
      it('should have a updateFTDBridgeGroupInterface function', (done) => {
        try {
          assert.equal(true, typeof a.updateFTDBridgeGroupInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateFTDBridgeGroupInterface(null, null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDBridgeGroupInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.updateFTDBridgeGroupInterface('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDBridgeGroupInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateFTDBridgeGroupInterface('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDBridgeGroupInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateFTDBridgeGroupInterface('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDBridgeGroupInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFTDBridgeGroupInterface - errors', () => {
      it('should have a deleteFTDBridgeGroupInterface function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFTDBridgeGroupInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteFTDBridgeGroupInterface(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFTDBridgeGroupInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.deleteFTDBridgeGroupInterface('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFTDBridgeGroupInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteFTDBridgeGroupInterface('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFTDBridgeGroupInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFTDEtherChannelInterface - errors', () => {
      it('should have a getAllFTDEtherChannelInterface function', (done) => {
        try {
          assert.equal(true, typeof a.getAllFTDEtherChannelInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllFTDEtherChannelInterface(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFTDEtherChannelInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllFTDEtherChannelInterface('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFTDEtherChannelInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFTDEtherChannelInterface - errors', () => {
      it('should have a createFTDEtherChannelInterface function', (done) => {
        try {
          assert.equal(true, typeof a.createFTDEtherChannelInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.createFTDEtherChannelInterface(null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFTDEtherChannelInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createFTDEtherChannelInterface('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFTDEtherChannelInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createFTDEtherChannelInterface('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFTDEtherChannelInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFTDEtherChannelInterface - errors', () => {
      it('should have a getFTDEtherChannelInterface function', (done) => {
        try {
          assert.equal(true, typeof a.getFTDEtherChannelInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getFTDEtherChannelInterface(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDEtherChannelInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getFTDEtherChannelInterface('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDEtherChannelInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getFTDEtherChannelInterface('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDEtherChannelInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFTDEtherChannelInterface - errors', () => {
      it('should have a updateFTDEtherChannelInterface function', (done) => {
        try {
          assert.equal(true, typeof a.updateFTDEtherChannelInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateFTDEtherChannelInterface(null, null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDEtherChannelInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.updateFTDEtherChannelInterface('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDEtherChannelInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateFTDEtherChannelInterface('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDEtherChannelInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateFTDEtherChannelInterface('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDEtherChannelInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFTDEtherChannelInterface - errors', () => {
      it('should have a deleteFTDEtherChannelInterface function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFTDEtherChannelInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteFTDEtherChannelInterface(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFTDEtherChannelInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.deleteFTDEtherChannelInterface('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFTDEtherChannelInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteFTDEtherChannelInterface('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFTDEtherChannelInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFPInterfaceStatistics - errors', () => {
      it('should have a getFPInterfaceStatistics function', (done) => {
        try {
          assert.equal(true, typeof a.getFPInterfaceStatistics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getFPInterfaceStatistics(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFPInterfaceStatistics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getFPInterfaceStatistics('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFPInterfaceStatistics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFPLogicalInterface - errors', () => {
      it('should have a getAllFPLogicalInterface function', (done) => {
        try {
          assert.equal(true, typeof a.getAllFPLogicalInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllFPLogicalInterface(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFPLogicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllFPLogicalInterface('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFPLogicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFPLogicalInterface - errors', () => {
      it('should have a createFPLogicalInterface function', (done) => {
        try {
          assert.equal(true, typeof a.createFPLogicalInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.createFPLogicalInterface(null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFPLogicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createFPLogicalInterface('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFPLogicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createFPLogicalInterface('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFPLogicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFPLogicalInterface - errors', () => {
      it('should have a getFPLogicalInterface function', (done) => {
        try {
          assert.equal(true, typeof a.getFPLogicalInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getFPLogicalInterface(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFPLogicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getFPLogicalInterface('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFPLogicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getFPLogicalInterface('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFPLogicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFPLogicalInterface - errors', () => {
      it('should have a updateFPLogicalInterface function', (done) => {
        try {
          assert.equal(true, typeof a.updateFPLogicalInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateFPLogicalInterface(null, null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFPLogicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.updateFPLogicalInterface('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFPLogicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateFPLogicalInterface('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFPLogicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateFPLogicalInterface('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFPLogicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFPLogicalInterface - errors', () => {
      it('should have a deleteFPLogicalInterface function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFPLogicalInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteFPLogicalInterface(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFPLogicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.deleteFPLogicalInterface('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFPLogicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteFPLogicalInterface('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFPLogicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFPPhysicalInterface - errors', () => {
      it('should have a getAllFPPhysicalInterface function', (done) => {
        try {
          assert.equal(true, typeof a.getAllFPPhysicalInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllFPPhysicalInterface(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFPPhysicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllFPPhysicalInterface('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFPPhysicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFPPhysicalInterface - errors', () => {
      it('should have a getFPPhysicalInterface function', (done) => {
        try {
          assert.equal(true, typeof a.getFPPhysicalInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getFPPhysicalInterface(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFPPhysicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getFPPhysicalInterface('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFPPhysicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getFPPhysicalInterface('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFPPhysicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFPPhysicalInterface - errors', () => {
      it('should have a updateFPPhysicalInterface function', (done) => {
        try {
          assert.equal(true, typeof a.updateFPPhysicalInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateFPPhysicalInterface(null, null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFPPhysicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.updateFPPhysicalInterface('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFPPhysicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateFPPhysicalInterface('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFPPhysicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateFPPhysicalInterface('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFPPhysicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllInlineSet - errors', () => {
      it('should have a getAllInlineSet function', (done) => {
        try {
          assert.equal(true, typeof a.getAllInlineSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllInlineSet(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllInlineSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllInlineSet('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllInlineSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createInlineSet - errors', () => {
      it('should have a createInlineSet function', (done) => {
        try {
          assert.equal(true, typeof a.createInlineSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.createInlineSet(null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createInlineSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createInlineSet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createInlineSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createInlineSet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createInlineSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInlineSet - errors', () => {
      it('should have a getInlineSet function', (done) => {
        try {
          assert.equal(true, typeof a.getInlineSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getInlineSet(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getInlineSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getInlineSet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getInlineSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getInlineSet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getInlineSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateInlineSet - errors', () => {
      it('should have a updateInlineSet function', (done) => {
        try {
          assert.equal(true, typeof a.updateInlineSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateInlineSet(null, null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateInlineSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.updateInlineSet('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateInlineSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateInlineSet('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateInlineSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateInlineSet('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateInlineSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInlineSet - errors', () => {
      it('should have a deleteInlineSet function', (done) => {
        try {
          assert.equal(true, typeof a.deleteInlineSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteInlineSet(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteInlineSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.deleteInlineSet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteInlineSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteInlineSet('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteInlineSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterfaceEvent - errors', () => {
      it('should have a getInterfaceEvent function', (done) => {
        try {
          assert.equal(true, typeof a.getInterfaceEvent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getInterfaceEvent(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getInterfaceEvent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getInterfaceEvent('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getInterfaceEvent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createInterfaceEvent - errors', () => {
      it('should have a createInterfaceEvent function', (done) => {
        try {
          assert.equal(true, typeof a.createInterfaceEvent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.createInterfaceEvent(null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createInterfaceEvent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createInterfaceEvent('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createInterfaceEvent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createInterfaceEvent('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createInterfaceEvent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFTDPhysicalInterface - errors', () => {
      it('should have a getAllFTDPhysicalInterface function', (done) => {
        try {
          assert.equal(true, typeof a.getAllFTDPhysicalInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllFTDPhysicalInterface(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFTDPhysicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllFTDPhysicalInterface('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFTDPhysicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFTDPhysicalInterface - errors', () => {
      it('should have a getFTDPhysicalInterface function', (done) => {
        try {
          assert.equal(true, typeof a.getFTDPhysicalInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getFTDPhysicalInterface(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDPhysicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getFTDPhysicalInterface('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDPhysicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getFTDPhysicalInterface('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDPhysicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFTDPhysicalInterface - errors', () => {
      it('should have a updateFTDPhysicalInterface function', (done) => {
        try {
          assert.equal(true, typeof a.updateFTDPhysicalInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateFTDPhysicalInterface(null, null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDPhysicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.updateFTDPhysicalInterface('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDPhysicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateFTDPhysicalInterface('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDPhysicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateFTDPhysicalInterface('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDPhysicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFTDRedundantInterface - errors', () => {
      it('should have a getAllFTDRedundantInterface function', (done) => {
        try {
          assert.equal(true, typeof a.getAllFTDRedundantInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllFTDRedundantInterface(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFTDRedundantInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllFTDRedundantInterface('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFTDRedundantInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFTDRedundantInterface - errors', () => {
      it('should have a createFTDRedundantInterface function', (done) => {
        try {
          assert.equal(true, typeof a.createFTDRedundantInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.createFTDRedundantInterface(null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFTDRedundantInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createFTDRedundantInterface('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFTDRedundantInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createFTDRedundantInterface('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFTDRedundantInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFTDRedundantInterface - errors', () => {
      it('should have a getFTDRedundantInterface function', (done) => {
        try {
          assert.equal(true, typeof a.getFTDRedundantInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getFTDRedundantInterface(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDRedundantInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getFTDRedundantInterface('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDRedundantInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getFTDRedundantInterface('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDRedundantInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFTDRedundantInterface - errors', () => {
      it('should have a updateFTDRedundantInterface function', (done) => {
        try {
          assert.equal(true, typeof a.updateFTDRedundantInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateFTDRedundantInterface(null, null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDRedundantInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.updateFTDRedundantInterface('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDRedundantInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateFTDRedundantInterface('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDRedundantInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateFTDRedundantInterface('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDRedundantInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFTDRedundantInterface - errors', () => {
      it('should have a deleteFTDRedundantInterface function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFTDRedundantInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteFTDRedundantInterface(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFTDRedundantInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.deleteFTDRedundantInterface('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFTDRedundantInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteFTDRedundantInterface('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFTDRedundantInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllIPv4StaticRouteModel - errors', () => {
      it('should have a getAllIPv4StaticRouteModel function', (done) => {
        try {
          assert.equal(true, typeof a.getAllIPv4StaticRouteModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllIPv4StaticRouteModel(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllIPv4StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllIPv4StaticRouteModel('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllIPv4StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIPv4StaticRouteModel - errors', () => {
      it('should have a createIPv4StaticRouteModel function', (done) => {
        try {
          assert.equal(true, typeof a.createIPv4StaticRouteModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.createIPv4StaticRouteModel('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createIPv4StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createIPv4StaticRouteModel('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createIPv4StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIPv4StaticRouteModel('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createIPv4StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPv4StaticRouteModel - errors', () => {
      it('should have a getIPv4StaticRouteModel function', (done) => {
        try {
          assert.equal(true, typeof a.getIPv4StaticRouteModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getIPv4StaticRouteModel(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getIPv4StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getIPv4StaticRouteModel('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getIPv4StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getIPv4StaticRouteModel('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getIPv4StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIPv4StaticRouteModel - errors', () => {
      it('should have a updateIPv4StaticRouteModel function', (done) => {
        try {
          assert.equal(true, typeof a.updateIPv4StaticRouteModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateIPv4StaticRouteModel(null, null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateIPv4StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.updateIPv4StaticRouteModel('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateIPv4StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateIPv4StaticRouteModel('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateIPv4StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIPv4StaticRouteModel('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateIPv4StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIPv4StaticRouteModel - errors', () => {
      it('should have a deleteIPv4StaticRouteModel function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIPv4StaticRouteModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteIPv4StaticRouteModel(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteIPv4StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.deleteIPv4StaticRouteModel('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteIPv4StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteIPv4StaticRouteModel('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteIPv4StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllIPv6StaticRouteModel - errors', () => {
      it('should have a getAllIPv6StaticRouteModel function', (done) => {
        try {
          assert.equal(true, typeof a.getAllIPv6StaticRouteModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllIPv6StaticRouteModel(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllIPv6StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllIPv6StaticRouteModel('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllIPv6StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIPv6StaticRouteModel - errors', () => {
      it('should have a createIPv6StaticRouteModel function', (done) => {
        try {
          assert.equal(true, typeof a.createIPv6StaticRouteModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.createIPv6StaticRouteModel('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createIPv6StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createIPv6StaticRouteModel('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createIPv6StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIPv6StaticRouteModel('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createIPv6StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPv6StaticRouteModel - errors', () => {
      it('should have a getIPv6StaticRouteModel function', (done) => {
        try {
          assert.equal(true, typeof a.getIPv6StaticRouteModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getIPv6StaticRouteModel(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getIPv6StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getIPv6StaticRouteModel('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getIPv6StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getIPv6StaticRouteModel('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getIPv6StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIPv6StaticRouteModel - errors', () => {
      it('should have a updateIPv6StaticRouteModel function', (done) => {
        try {
          assert.equal(true, typeof a.updateIPv6StaticRouteModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateIPv6StaticRouteModel(null, null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateIPv6StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.updateIPv6StaticRouteModel('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateIPv6StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateIPv6StaticRouteModel('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateIPv6StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIPv6StaticRouteModel('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateIPv6StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIPv6StaticRouteModel - errors', () => {
      it('should have a deleteIPv6StaticRouteModel function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIPv6StaticRouteModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteIPv6StaticRouteModel(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteIPv6StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.deleteIPv6StaticRouteModel('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteIPv6StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteIPv6StaticRouteModel('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteIPv6StaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllStaticRouteModel - errors', () => {
      it('should have a getAllStaticRouteModel function', (done) => {
        try {
          assert.equal(true, typeof a.getAllStaticRouteModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllStaticRouteModel(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllStaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllStaticRouteModel('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllStaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStaticRouteModel - errors', () => {
      it('should have a getStaticRouteModel function', (done) => {
        try {
          assert.equal(true, typeof a.getStaticRouteModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getStaticRouteModel(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getStaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getStaticRouteModel('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getStaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getStaticRouteModel('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getStaticRouteModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFTDSubInterface - errors', () => {
      it('should have a getAllFTDSubInterface function', (done) => {
        try {
          assert.equal(true, typeof a.getAllFTDSubInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllFTDSubInterface(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFTDSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllFTDSubInterface('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFTDSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFTDSubInterface - errors', () => {
      it('should have a createFTDSubInterface function', (done) => {
        try {
          assert.equal(true, typeof a.createFTDSubInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.createFTDSubInterface('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFTDSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createFTDSubInterface('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFTDSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createFTDSubInterface('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFTDSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFTDSubInterface - errors', () => {
      it('should have a getFTDSubInterface function', (done) => {
        try {
          assert.equal(true, typeof a.getFTDSubInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getFTDSubInterface(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getFTDSubInterface('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getFTDSubInterface('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFTDSubInterface - errors', () => {
      it('should have a updateFTDSubInterface function', (done) => {
        try {
          assert.equal(true, typeof a.updateFTDSubInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateFTDSubInterface(null, null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.updateFTDSubInterface('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateFTDSubInterface('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateFTDSubInterface('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFTDSubInterface - errors', () => {
      it('should have a deleteFTDSubInterface function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFTDSubInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteFTDSubInterface(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFTDSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.deleteFTDSubInterface('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFTDSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteFTDSubInterface('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFTDSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllVirtualSwitch - errors', () => {
      it('should have a getAllVirtualSwitch function', (done) => {
        try {
          assert.equal(true, typeof a.getAllVirtualSwitch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllVirtualSwitch(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllVirtualSwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllVirtualSwitch('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllVirtualSwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVirtualSwitch - errors', () => {
      it('should have a createVirtualSwitch function', (done) => {
        try {
          assert.equal(true, typeof a.createVirtualSwitch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.createVirtualSwitch(null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createVirtualSwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createVirtualSwitch('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createVirtualSwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createVirtualSwitch('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createVirtualSwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualSwitch - errors', () => {
      it('should have a getVirtualSwitch function', (done) => {
        try {
          assert.equal(true, typeof a.getVirtualSwitch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getVirtualSwitch(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getVirtualSwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getVirtualSwitch('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getVirtualSwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getVirtualSwitch('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getVirtualSwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateVirtualSwitch - errors', () => {
      it('should have a updateVirtualSwitch function', (done) => {
        try {
          assert.equal(true, typeof a.updateVirtualSwitch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateVirtualSwitch(null, null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateVirtualSwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.updateVirtualSwitch('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateVirtualSwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateVirtualSwitch('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateVirtualSwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateVirtualSwitch('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateVirtualSwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualSwitch - errors', () => {
      it('should have a deleteVirtualSwitch function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVirtualSwitch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteVirtualSwitch(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteVirtualSwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.deleteVirtualSwitch('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteVirtualSwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteVirtualSwitch('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteVirtualSwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFpmDevice - errors', () => {
      it('should have a getFpmDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getFpmDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getFpmDevice(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFpmDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getFpmDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFpmDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDevice - errors', () => {
      it('should have a updateDevice function', (done) => {
        try {
          assert.equal(true, typeof a.updateDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateDevice(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateDevice('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateDevice('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDevice - errors', () => {
      it('should have a deleteDevice function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteDevice(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPolicyAssignment - errors', () => {
      it('should have a getAllPolicyAssignment function', (done) => {
        try {
          assert.equal(true, typeof a.getAllPolicyAssignment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllPolicyAssignment(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllPolicyAssignment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPolicyAssignment - errors', () => {
      it('should have a createPolicyAssignment function', (done) => {
        try {
          assert.equal(true, typeof a.createPolicyAssignment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createPolicyAssignment(null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createPolicyAssignment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createPolicyAssignment('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createPolicyAssignment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyAssignment - errors', () => {
      it('should have a getPolicyAssignment function', (done) => {
        try {
          assert.equal(true, typeof a.getPolicyAssignment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getPolicyAssignment(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getPolicyAssignment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getPolicyAssignment('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getPolicyAssignment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePolicyAssignment - errors', () => {
      it('should have a updatePolicyAssignment function', (done) => {
        try {
          assert.equal(true, typeof a.updatePolicyAssignment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updatePolicyAssignment(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updatePolicyAssignment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updatePolicyAssignment('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updatePolicyAssignment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePolicyAssignment('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updatePolicyAssignment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFTDHADeviceContainer - errors', () => {
      it('should have a getAllFTDHADeviceContainer function', (done) => {
        try {
          assert.equal(true, typeof a.getAllFTDHADeviceContainer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllFTDHADeviceContainer(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFTDHADeviceContainer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFTDHADeviceContainer - errors', () => {
      it('should have a createFTDHADeviceContainer function', (done) => {
        try {
          assert.equal(true, typeof a.createFTDHADeviceContainer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createFTDHADeviceContainer(null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFTDHADeviceContainer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createFTDHADeviceContainer('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFTDHADeviceContainer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFTDHAInterfaceMACAddresses - errors', () => {
      it('should have a getAllFTDHAInterfaceMACAddresses function', (done) => {
        try {
          assert.equal(true, typeof a.getAllFTDHAInterfaceMACAddresses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllFTDHAInterfaceMACAddresses(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFTDHAInterfaceMACAddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllFTDHAInterfaceMACAddresses('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFTDHAInterfaceMACAddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFTDHAInterfaceMACAddresses - errors', () => {
      it('should have a createFTDHAInterfaceMACAddresses function', (done) => {
        try {
          assert.equal(true, typeof a.createFTDHAInterfaceMACAddresses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.createFTDHAInterfaceMACAddresses(null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFTDHAInterfaceMACAddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createFTDHAInterfaceMACAddresses('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFTDHAInterfaceMACAddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createFTDHAInterfaceMACAddresses('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFTDHAInterfaceMACAddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFTDHAInterfaceMACAddresses - errors', () => {
      it('should have a getFTDHAInterfaceMACAddresses function', (done) => {
        try {
          assert.equal(true, typeof a.getFTDHAInterfaceMACAddresses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getFTDHAInterfaceMACAddresses(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDHAInterfaceMACAddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getFTDHAInterfaceMACAddresses('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDHAInterfaceMACAddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getFTDHAInterfaceMACAddresses('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDHAInterfaceMACAddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFTDHAInterfaceMACAddresses - errors', () => {
      it('should have a updateFTDHAInterfaceMACAddresses function', (done) => {
        try {
          assert.equal(true, typeof a.updateFTDHAInterfaceMACAddresses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateFTDHAInterfaceMACAddresses(null, null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDHAInterfaceMACAddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.updateFTDHAInterfaceMACAddresses('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDHAInterfaceMACAddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateFTDHAInterfaceMACAddresses('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDHAInterfaceMACAddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateFTDHAInterfaceMACAddresses('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDHAInterfaceMACAddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFTDHAInterfaceMACAddresses - errors', () => {
      it('should have a deleteFTDHAInterfaceMACAddresses function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFTDHAInterfaceMACAddresses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteFTDHAInterfaceMACAddresses(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFTDHAInterfaceMACAddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.deleteFTDHAInterfaceMACAddresses('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFTDHAInterfaceMACAddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteFTDHAInterfaceMACAddresses('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFTDHAInterfaceMACAddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFTDHAMonitoredInterfaces - errors', () => {
      it('should have a getAllFTDHAMonitoredInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.getAllFTDHAMonitoredInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllFTDHAMonitoredInterfaces(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFTDHAMonitoredInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllFTDHAMonitoredInterfaces('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFTDHAMonitoredInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFTDHAMonitoredInterfaces - errors', () => {
      it('should have a getFTDHAMonitoredInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.getFTDHAMonitoredInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getFTDHAMonitoredInterfaces(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDHAMonitoredInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getFTDHAMonitoredInterfaces('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDHAMonitoredInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getFTDHAMonitoredInterfaces('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDHAMonitoredInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFTDHAMonitoredInterfaces - errors', () => {
      it('should have a updateFTDHAMonitoredInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.updateFTDHAMonitoredInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateFTDHAMonitoredInterfaces(null, null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDHAMonitoredInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.updateFTDHAMonitoredInterfaces('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDHAMonitoredInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateFTDHAMonitoredInterfaces('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDHAMonitoredInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateFTDHAMonitoredInterfaces('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDHAMonitoredInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFTDHADeviceContainer - errors', () => {
      it('should have a getFTDHADeviceContainer function', (done) => {
        try {
          assert.equal(true, typeof a.getFTDHADeviceContainer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getFTDHADeviceContainer(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDHADeviceContainer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getFTDHADeviceContainer('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDHADeviceContainer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFTDHADeviceContainer - errors', () => {
      it('should have a updateFTDHADeviceContainer function', (done) => {
        try {
          assert.equal(true, typeof a.updateFTDHADeviceContainer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateFTDHADeviceContainer(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDHADeviceContainer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateFTDHADeviceContainer('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDHADeviceContainer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateFTDHADeviceContainer('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDHADeviceContainer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFTDHADeviceContainer - errors', () => {
      it('should have a deleteFTDHADeviceContainer function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFTDHADeviceContainer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteFTDHADeviceContainer(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFTDHADeviceContainer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteFTDHADeviceContainer('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFTDHADeviceContainer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllUpgradePackage - errors', () => {
      it('should have a getAllUpgradePackage function', (done) => {
        try {
          assert.equal(true, typeof a.getAllUpgradePackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllApplicableDevice - errors', () => {
      it('should have a getAllApplicableDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getAllApplicableDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllApplicableDevice(null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllApplicableDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUpgradePackage - errors', () => {
      it('should have a getUpgradePackage function', (done) => {
        try {
          assert.equal(true, typeof a.getUpgradePackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getUpgradePackage(null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getUpgradePackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUpgradePackage - errors', () => {
      it('should have a deleteUpgradePackage function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUpgradePackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteUpgradePackage(null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteUpgradePackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createUpgrade - errors', () => {
      it('should have a createUpgrade function', (done) => {
        try {
          assert.equal(true, typeof a.createUpgrade === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createUpgrade(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createUpgrade', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRESTTaxiiCollection - errors', () => {
      it('should have a createRESTTaxiiCollection function', (done) => {
        try {
          assert.equal(true, typeof a.createRESTTaxiiCollection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createRESTTaxiiCollection(null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createRESTTaxiiCollection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRESTTaxiiCollection('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createRESTTaxiiCollection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRESTDiscoveryInfo - errors', () => {
      it('should have a createRESTDiscoveryInfo function', (done) => {
        try {
          assert.equal(true, typeof a.createRESTDiscoveryInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createRESTDiscoveryInfo(null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createRESTDiscoveryInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRESTDiscoveryInfo('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createRESTDiscoveryInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRESTElement - errors', () => {
      it('should have a getAllRESTElement function', (done) => {
        try {
          assert.equal(true, typeof a.getAllRESTElement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllRESTElement(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllRESTElement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRESTElement - errors', () => {
      it('should have a getRESTElement function', (done) => {
        try {
          assert.equal(true, typeof a.getRESTElement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getRESTElement(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getRESTElement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getRESTElement('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getRESTElement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRESTIncident - errors', () => {
      it('should have a getAllRESTIncident function', (done) => {
        try {
          assert.equal(true, typeof a.getAllRESTIncident === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllRESTIncident(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllRESTIncident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRESTIncident - errors', () => {
      it('should have a getRESTIncident function', (done) => {
        try {
          assert.equal(true, typeof a.getRESTIncident === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getRESTIncident(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getRESTIncident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getRESTIncident('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getRESTIncident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRESTIncident - errors', () => {
      it('should have a updateRESTIncident function', (done) => {
        try {
          assert.equal(true, typeof a.updateRESTIncident === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateRESTIncident(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateRESTIncident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateRESTIncident('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateRESTIncident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateRESTIncident('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateRESTIncident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRESTIncident - errors', () => {
      it('should have a deleteRESTIncident function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRESTIncident === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteRESTIncident(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteRESTIncident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteRESTIncident('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteRESTIncident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRESTIndicator - errors', () => {
      it('should have a getAllRESTIndicator function', (done) => {
        try {
          assert.equal(true, typeof a.getAllRESTIndicator === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllRESTIndicator(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllRESTIndicator', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRESTIndicator - errors', () => {
      it('should have a getRESTIndicator function', (done) => {
        try {
          assert.equal(true, typeof a.getRESTIndicator === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getRESTIndicator(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getRESTIndicator', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getRESTIndicator('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getRESTIndicator', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRESTIndicator - errors', () => {
      it('should have a updateRESTIndicator function', (done) => {
        try {
          assert.equal(true, typeof a.updateRESTIndicator === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateRESTIndicator(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateRESTIndicator', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateRESTIndicator('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateRESTIndicator', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateRESTIndicator('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateRESTIndicator', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRESTObservable - errors', () => {
      it('should have a getAllRESTObservable function', (done) => {
        try {
          assert.equal(true, typeof a.getAllRESTObservable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllRESTObservable(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllRESTObservable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRESTObservable - errors', () => {
      it('should have a getRESTObservable function', (done) => {
        try {
          assert.equal(true, typeof a.getRESTObservable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getRESTObservable(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getRESTObservable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getRESTObservable('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getRESTObservable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRESTObservable - errors', () => {
      it('should have a updateRESTObservable function', (done) => {
        try {
          assert.equal(true, typeof a.updateRESTObservable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateRESTObservable(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateRESTObservable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateRESTObservable('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateRESTObservable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateRESTObservable('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateRESTObservable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRESTSettings - errors', () => {
      it('should have a getRESTSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getRESTSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getRESTSettings(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getRESTSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getRESTSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getRESTSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRESTSettings - errors', () => {
      it('should have a updateRESTSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateRESTSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateRESTSettings(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateRESTSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateRESTSettings('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateRESTSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateRESTSettings('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateRESTSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRESTTidSource - errors', () => {
      it('should have a getAllRESTTidSource function', (done) => {
        try {
          assert.equal(true, typeof a.getAllRESTTidSource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllRESTTidSource(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllRESTTidSource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRESTTidSource - errors', () => {
      it('should have a createRESTTidSource function', (done) => {
        try {
          assert.equal(true, typeof a.createRESTTidSource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createRESTTidSource(null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createRESTTidSource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRESTTidSource('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createRESTTidSource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRESTTidSource - errors', () => {
      it('should have a getRESTTidSource function', (done) => {
        try {
          assert.equal(true, typeof a.getRESTTidSource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getRESTTidSource(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getRESTTidSource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getRESTTidSource('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getRESTTidSource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRESTTidSource - errors', () => {
      it('should have a updateRESTTidSource function', (done) => {
        try {
          assert.equal(true, typeof a.updateRESTTidSource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateRESTTidSource(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateRESTTidSource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateRESTTidSource('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateRESTTidSource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateRESTTidSource('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateRESTTidSource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRESTTidSource - errors', () => {
      it('should have a deleteRESTTidSource function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRESTTidSource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteRESTTidSource(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteRESTTidSource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteRESTTidSource('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteRESTTidSource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllAuditModel - errors', () => {
      it('should have a getAllAuditModel function', (done) => {
        try {
          assert.equal(true, typeof a.getAllAuditModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllAuditModel(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllAuditModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuditModel - errors', () => {
      it('should have a getAuditModel function', (done) => {
        try {
          assert.equal(true, typeof a.getAuditModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getAuditModel(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAuditModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAuditModel('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAuditModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllDeviceGroup - errors', () => {
      it('should have a getAllDeviceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getAllDeviceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllDeviceGroup(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDeviceGroup - errors', () => {
      it('should have a createDeviceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createDeviceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createDeviceGroup(null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDeviceGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroup - errors', () => {
      it('should have a getDeviceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getDeviceGroup(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getDeviceGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDeviceGroup - errors', () => {
      it('should have a updateDeviceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.updateDeviceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateDeviceGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateDeviceGroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateDeviceGroup('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceGroup - errors', () => {
      it('should have a deleteDeviceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDeviceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteDeviceGroup(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteDeviceGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllCloudEvents - errors', () => {
      it('should have a getAllCloudEvents function', (done) => {
        try {
          assert.equal(true, typeof a.getAllCloudEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllCloudEvents(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllCloudEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudEvents - errors', () => {
      it('should have a getCloudEvents function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getCloudEvents(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getCloudEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getCloudEvents('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getCloudEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCloudEvents - errors', () => {
      it('should have a updateCloudEvents function', (done) => {
        try {
          assert.equal(true, typeof a.updateCloudEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateCloudEvents(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateCloudEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateCloudEvents('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateCloudEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateCloudEvents('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateCloudEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllExternalLookup - errors', () => {
      it('should have a getAllExternalLookup function', (done) => {
        try {
          assert.equal(true, typeof a.getAllExternalLookup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllExternalLookup(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllExternalLookup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createExternalLookup - errors', () => {
      it('should have a createExternalLookup function', (done) => {
        try {
          assert.equal(true, typeof a.createExternalLookup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createExternalLookup(null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createExternalLookup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createExternalLookup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createExternalLookup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExternalLookup - errors', () => {
      it('should have a getExternalLookup function', (done) => {
        try {
          assert.equal(true, typeof a.getExternalLookup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getExternalLookup(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getExternalLookup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getExternalLookup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getExternalLookup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateExternalLookup - errors', () => {
      it('should have a updateExternalLookup function', (done) => {
        try {
          assert.equal(true, typeof a.updateExternalLookup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateExternalLookup(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateExternalLookup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateExternalLookup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateExternalLookup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateExternalLookup('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateExternalLookup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExternalLookup - errors', () => {
      it('should have a deleteExternalLookup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExternalLookup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteExternalLookup(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteExternalLookup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteExternalLookup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteExternalLookup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPacketAnalyzerDevice - errors', () => {
      it('should have a getAllPacketAnalyzerDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getAllPacketAnalyzerDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllPacketAnalyzerDevice(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllPacketAnalyzerDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPacketAnalyzerDevice - errors', () => {
      it('should have a createPacketAnalyzerDevice function', (done) => {
        try {
          assert.equal(true, typeof a.createPacketAnalyzerDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createPacketAnalyzerDevice(null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createPacketAnalyzerDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createPacketAnalyzerDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createPacketAnalyzerDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPacketAnalyzerDevice - errors', () => {
      it('should have a getPacketAnalyzerDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getPacketAnalyzerDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getPacketAnalyzerDevice(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getPacketAnalyzerDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getPacketAnalyzerDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getPacketAnalyzerDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePacketAnalyzerDevice - errors', () => {
      it('should have a updatePacketAnalyzerDevice function', (done) => {
        try {
          assert.equal(true, typeof a.updatePacketAnalyzerDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updatePacketAnalyzerDevice(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updatePacketAnalyzerDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updatePacketAnalyzerDevice('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updatePacketAnalyzerDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePacketAnalyzerDevice('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updatePacketAnalyzerDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePacketAnalyzerDevice - errors', () => {
      it('should have a deletePacketAnalyzerDevice function', (done) => {
        try {
          assert.equal(true, typeof a.deletePacketAnalyzerDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deletePacketAnalyzerDevice(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deletePacketAnalyzerDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deletePacketAnalyzerDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deletePacketAnalyzerDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTaskStatus - errors', () => {
      it('should have a getTaskStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getTaskStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getTaskStatus(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getTaskStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getTaskStatus('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getTaskStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFTDClusterDeviceContainer - errors', () => {
      it('should have a getAllFTDClusterDeviceContainer function', (done) => {
        try {
          assert.equal(true, typeof a.getAllFTDClusterDeviceContainer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllFTDClusterDeviceContainer(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFTDClusterDeviceContainer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFTDClusterDeviceContainer - errors', () => {
      it('should have a getFTDClusterDeviceContainer function', (done) => {
        try {
          assert.equal(true, typeof a.getFTDClusterDeviceContainer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getFTDClusterDeviceContainer(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDClusterDeviceContainer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getFTDClusterDeviceContainer('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDClusterDeviceContainer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllServerVersion - errors', () => {
      it('should have a getAllServerVersion function', (done) => {
        try {
          assert.equal(true, typeof a.getAllServerVersion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServerVersion - errors', () => {
      it('should have a getServerVersion function', (done) => {
        try {
          assert.equal(true, typeof a.getServerVersion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getServerVersion(null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getServerVersion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllAccessPolicy - errors', () => {
      it('should have a getAllAccessPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getAllAccessPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllAccessPolicy(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllAccessPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAccessPolicy - errors', () => {
      it('should have a createAccessPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.createAccessPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createAccessPolicy(null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createAccessPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAccessPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createAccessPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllAccessRule - errors', () => {
      it('should have a getAllAccessRule function', (done) => {
        try {
          assert.equal(true, typeof a.getAllAccessRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllAccessRule(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllAccessRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllAccessRule('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllAccessRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAccessRule - errors', () => {
      it('should have a createAccessRule function', (done) => {
        try {
          assert.equal(true, typeof a.createAccessRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.createAccessRule('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createAccessRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createAccessRule('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createAccessRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAccessRule('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createAccessRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAccessRule1 - errors', () => {
      it('should have a updateAccessRule1 function', (done) => {
        try {
          assert.equal(true, typeof a.updateAccessRule1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bulk', (done) => {
        try {
          a.updateAccessRule1(null, null, null, null, (data, error) => {
            try {
              const displayE = 'bulk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateAccessRule1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.updateAccessRule1('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateAccessRule1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateAccessRule1('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateAccessRule1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAccessRule1('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateAccessRule1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccessRule - errors', () => {
      it('should have a getAccessRule function', (done) => {
        try {
          assert.equal(true, typeof a.getAccessRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getAccessRule(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAccessRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAccessRule('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAccessRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAccessRule('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAccessRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAccessRule - errors', () => {
      it('should have a updateAccessRule function', (done) => {
        try {
          assert.equal(true, typeof a.updateAccessRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateAccessRule(null, null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateAccessRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.updateAccessRule('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateAccessRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateAccessRule('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateAccessRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAccessRule('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateAccessRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAccessRule - errors', () => {
      it('should have a deleteAccessRule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAccessRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteAccessRule(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteAccessRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.deleteAccessRule('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteAccessRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteAccessRule('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteAccessRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllDefaultAction - errors', () => {
      it('should have a getAllDefaultAction function', (done) => {
        try {
          assert.equal(true, typeof a.getAllDefaultAction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllDefaultAction(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllDefaultAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllDefaultAction('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllDefaultAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDefaultAction - errors', () => {
      it('should have a getDefaultAction function', (done) => {
        try {
          assert.equal(true, typeof a.getDefaultAction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getDefaultAction(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getDefaultAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getDefaultAction('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getDefaultAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getDefaultAction('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getDefaultAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDefaultAction - errors', () => {
      it('should have a updateDefaultAction function', (done) => {
        try {
          assert.equal(true, typeof a.updateDefaultAction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateDefaultAction(null, null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateDefaultAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.updateDefaultAction('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateDefaultAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateDefaultAction('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateDefaultAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateDefaultAction('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateDefaultAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllAccessPolicyLoggingSettingModel - errors', () => {
      it('should have a getAllAccessPolicyLoggingSettingModel function', (done) => {
        try {
          assert.equal(true, typeof a.getAllAccessPolicyLoggingSettingModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllAccessPolicyLoggingSettingModel(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllAccessPolicyLoggingSettingModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllAccessPolicyLoggingSettingModel('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllAccessPolicyLoggingSettingModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccessPolicyLoggingSettingModel - errors', () => {
      it('should have a getAccessPolicyLoggingSettingModel function', (done) => {
        try {
          assert.equal(true, typeof a.getAccessPolicyLoggingSettingModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getAccessPolicyLoggingSettingModel(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAccessPolicyLoggingSettingModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAccessPolicyLoggingSettingModel('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAccessPolicyLoggingSettingModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAccessPolicyLoggingSettingModel('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAccessPolicyLoggingSettingModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAccessPolicyLoggingSettingModel - errors', () => {
      it('should have a updateAccessPolicyLoggingSettingModel function', (done) => {
        try {
          assert.equal(true, typeof a.updateAccessPolicyLoggingSettingModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateAccessPolicyLoggingSettingModel(null, null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateAccessPolicyLoggingSettingModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.updateAccessPolicyLoggingSettingModel('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateAccessPolicyLoggingSettingModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateAccessPolicyLoggingSettingModel('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateAccessPolicyLoggingSettingModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAccessPolicyLoggingSettingModel('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateAccessPolicyLoggingSettingModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHitCount - errors', () => {
      it('should have a getHitCount function', (done) => {
        try {
          assert.equal(true, typeof a.getHitCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.getHitCount(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getHitCount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getHitCount('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getHitCount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getHitCount('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getHitCount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHitCount - errors', () => {
      it('should have a updateHitCount function', (done) => {
        try {
          assert.equal(true, typeof a.updateHitCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.updateHitCount(null, null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateHitCount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.updateHitCount('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateHitCount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateHitCount('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateHitCount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHitCount - errors', () => {
      it('should have a deleteHitCount function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHitCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.deleteHitCount(null, null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteHitCount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.deleteHitCount('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteHitCount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteHitCount('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteHitCount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccessPolicy - errors', () => {
      it('should have a getAccessPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getAccessPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getAccessPolicy(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAccessPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAccessPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAccessPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAccessPolicy - errors', () => {
      it('should have a updateAccessPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.updateAccessPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateAccessPolicy(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateAccessPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateAccessPolicy('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateAccessPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAccessPolicy('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateAccessPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAccessPolicy - errors', () => {
      it('should have a deleteAccessPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAccessPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteAccessPolicy(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteAccessPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteAccessPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteAccessPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFilePolicy - errors', () => {
      it('should have a getAllFilePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getAllFilePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllFilePolicy(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFilePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFilePolicy - errors', () => {
      it('should have a getFilePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getFilePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getFilePolicy(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFilePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getFilePolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFilePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFTDNatPolicy - errors', () => {
      it('should have a getAllFTDNatPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getAllFTDNatPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllFTDNatPolicy(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFTDNatPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFTDNatPolicy - errors', () => {
      it('should have a createFTDNatPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.createFTDNatPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createFTDNatPolicy(null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFTDNatPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createFTDNatPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFTDNatPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFTDAutoNatRule - errors', () => {
      it('should have a getAllFTDAutoNatRule function', (done) => {
        try {
          assert.equal(true, typeof a.getAllFTDAutoNatRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllFTDAutoNatRule(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFTDAutoNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllFTDAutoNatRule('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFTDAutoNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFTDAutoNatRule - errors', () => {
      it('should have a createFTDAutoNatRule function', (done) => {
        try {
          assert.equal(true, typeof a.createFTDAutoNatRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.createFTDAutoNatRule('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFTDAutoNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createFTDAutoNatRule('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFTDAutoNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createFTDAutoNatRule('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFTDAutoNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFTDAutoNatRule - errors', () => {
      it('should have a getFTDAutoNatRule function', (done) => {
        try {
          assert.equal(true, typeof a.getFTDAutoNatRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getFTDAutoNatRule(null, null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDAutoNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getFTDAutoNatRule('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDAutoNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getFTDAutoNatRule('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDAutoNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFTDAutoNatRule - errors', () => {
      it('should have a updateFTDAutoNatRule function', (done) => {
        try {
          assert.equal(true, typeof a.updateFTDAutoNatRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateFTDAutoNatRule(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDAutoNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.updateFTDAutoNatRule('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDAutoNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateFTDAutoNatRule('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDAutoNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateFTDAutoNatRule('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDAutoNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFTDAutoNatRule - errors', () => {
      it('should have a deleteFTDAutoNatRule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFTDAutoNatRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteFTDAutoNatRule(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFTDAutoNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.deleteFTDAutoNatRule('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFTDAutoNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteFTDAutoNatRule('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFTDAutoNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFTDManualNatRule - errors', () => {
      it('should have a getAllFTDManualNatRule function', (done) => {
        try {
          assert.equal(true, typeof a.getAllFTDManualNatRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllFTDManualNatRule(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFTDManualNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllFTDManualNatRule('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFTDManualNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFTDManualNatRule - errors', () => {
      it('should have a createFTDManualNatRule function', (done) => {
        try {
          assert.equal(true, typeof a.createFTDManualNatRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.createFTDManualNatRule('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFTDManualNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createFTDManualNatRule('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFTDManualNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createFTDManualNatRule('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFTDManualNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFTDManualNatRule - errors', () => {
      it('should have a getFTDManualNatRule function', (done) => {
        try {
          assert.equal(true, typeof a.getFTDManualNatRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getFTDManualNatRule(null, null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDManualNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getFTDManualNatRule('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDManualNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getFTDManualNatRule('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDManualNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFTDManualNatRule - errors', () => {
      it('should have a updateFTDManualNatRule function', (done) => {
        try {
          assert.equal(true, typeof a.updateFTDManualNatRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateFTDManualNatRule(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDManualNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.updateFTDManualNatRule('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDManualNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateFTDManualNatRule('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDManualNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateFTDManualNatRule('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDManualNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFTDManualNatRule - errors', () => {
      it('should have a deleteFTDManualNatRule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFTDManualNatRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteFTDManualNatRule(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFTDManualNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.deleteFTDManualNatRule('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFTDManualNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteFTDManualNatRule('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFTDManualNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFTDNatRule - errors', () => {
      it('should have a getAllFTDNatRule function', (done) => {
        try {
          assert.equal(true, typeof a.getAllFTDNatRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllFTDNatRule(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFTDNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllFTDNatRule('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFTDNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFTDNatRule - errors', () => {
      it('should have a getFTDNatRule function', (done) => {
        try {
          assert.equal(true, typeof a.getFTDNatRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getFTDNatRule(null, null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getFTDNatRule('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getFTDNatRule('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFTDNatPolicy - errors', () => {
      it('should have a getFTDNatPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getFTDNatPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getFTDNatPolicy(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDNatPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getFTDNatPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDNatPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFTDNatPolicy - errors', () => {
      it('should have a updateFTDNatPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.updateFTDNatPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateFTDNatPolicy(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDNatPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateFTDNatPolicy('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDNatPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateFTDNatPolicy('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDNatPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFTDNatPolicy - errors', () => {
      it('should have a deleteFTDNatPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFTDNatPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteFTDNatPolicy(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFTDNatPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteFTDNatPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFTDNatPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFTDS2SVpnModel - errors', () => {
      it('should have a getAllFTDS2SVpnModel function', (done) => {
        try {
          assert.equal(true, typeof a.getAllFTDS2SVpnModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllFTDS2SVpnModel(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFTDS2SVpnModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFTDS2SVpnModel - errors', () => {
      it('should have a createFTDS2SVpnModel function', (done) => {
        try {
          assert.equal(true, typeof a.createFTDS2SVpnModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createFTDS2SVpnModel(null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFTDS2SVpnModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createFTDS2SVpnModel('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFTDS2SVpnModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllVpnAdvancedSettings - errors', () => {
      it('should have a getAllVpnAdvancedSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getAllVpnAdvancedSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllVpnAdvancedSettings(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllVpnAdvancedSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllVpnAdvancedSettings('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllVpnAdvancedSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVpnAdvancedSettings - errors', () => {
      it('should have a getVpnAdvancedSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getVpnAdvancedSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getVpnAdvancedSettings(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getVpnAdvancedSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getVpnAdvancedSettings('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getVpnAdvancedSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getVpnAdvancedSettings('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getVpnAdvancedSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateVpnAdvancedSettings - errors', () => {
      it('should have a updateVpnAdvancedSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateVpnAdvancedSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateVpnAdvancedSettings(null, null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateVpnAdvancedSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.updateVpnAdvancedSettings('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateVpnAdvancedSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateVpnAdvancedSettings('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateVpnAdvancedSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateVpnAdvancedSettings('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateVpnAdvancedSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllVpnEndpoint - errors', () => {
      it('should have a getAllVpnEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.getAllVpnEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllVpnEndpoint(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllVpnEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllVpnEndpoint('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllVpnEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVpnEndpoint - errors', () => {
      it('should have a createVpnEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.createVpnEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.createVpnEndpoint(null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createVpnEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createVpnEndpoint('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createVpnEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createVpnEndpoint('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createVpnEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVpnEndpoint - errors', () => {
      it('should have a getVpnEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.getVpnEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getVpnEndpoint(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getVpnEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getVpnEndpoint('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getVpnEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getVpnEndpoint('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getVpnEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateVpnEndpoint - errors', () => {
      it('should have a updateVpnEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.updateVpnEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateVpnEndpoint(null, null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateVpnEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.updateVpnEndpoint('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateVpnEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateVpnEndpoint('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateVpnEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateVpnEndpoint('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateVpnEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVpnEndpoint - errors', () => {
      it('should have a deleteVpnEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVpnEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteVpnEndpoint(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteVpnEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.deleteVpnEndpoint('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteVpnEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteVpnEndpoint('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteVpnEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllVpnIkeSettings - errors', () => {
      it('should have a getAllVpnIkeSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getAllVpnIkeSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllVpnIkeSettings(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllVpnIkeSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllVpnIkeSettings('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllVpnIkeSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVpnIkeSettings - errors', () => {
      it('should have a getVpnIkeSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getVpnIkeSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getVpnIkeSettings(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getVpnIkeSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getVpnIkeSettings('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getVpnIkeSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getVpnIkeSettings('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getVpnIkeSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateVpnIkeSettings - errors', () => {
      it('should have a updateVpnIkeSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateVpnIkeSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateVpnIkeSettings(null, null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateVpnIkeSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.updateVpnIkeSettings('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateVpnIkeSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateVpnIkeSettings('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateVpnIkeSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateVpnIkeSettings('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateVpnIkeSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllVpnIPSecSettings - errors', () => {
      it('should have a getAllVpnIPSecSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getAllVpnIPSecSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllVpnIPSecSettings(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllVpnIPSecSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllVpnIPSecSettings('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllVpnIPSecSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVpnIPSecSettings - errors', () => {
      it('should have a getVpnIPSecSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getVpnIPSecSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getVpnIPSecSettings(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getVpnIPSecSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getVpnIPSecSettings('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getVpnIPSecSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getVpnIPSecSettings('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getVpnIPSecSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateVpnIPSecSettings - errors', () => {
      it('should have a updateVpnIPSecSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateVpnIPSecSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateVpnIPSecSettings(null, null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateVpnIPSecSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.updateVpnIPSecSettings('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateVpnIPSecSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateVpnIPSecSettings('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateVpnIPSecSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateVpnIPSecSettings('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateVpnIPSecSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFTDS2SVpnModel - errors', () => {
      it('should have a getFTDS2SVpnModel function', (done) => {
        try {
          assert.equal(true, typeof a.getFTDS2SVpnModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getFTDS2SVpnModel(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDS2SVpnModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getFTDS2SVpnModel('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFTDS2SVpnModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFTDS2SVpnModel - errors', () => {
      it('should have a updateFTDS2SVpnModel function', (done) => {
        try {
          assert.equal(true, typeof a.updateFTDS2SVpnModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateFTDS2SVpnModel(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDS2SVpnModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateFTDS2SVpnModel('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDS2SVpnModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateFTDS2SVpnModel('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFTDS2SVpnModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFTDS2SVpnModel - errors', () => {
      it('should have a deleteFTDS2SVpnModel function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFTDS2SVpnModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteFTDS2SVpnModel(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFTDS2SVpnModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteFTDS2SVpnModel('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFTDS2SVpnModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllIntrusionPolicy - errors', () => {
      it('should have a getAllIntrusionPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getAllIntrusionPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllIntrusionPolicy(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllIntrusionPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntrusionPolicy - errors', () => {
      it('should have a getIntrusionPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getIntrusionPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getIntrusionPolicy(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getIntrusionPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getIntrusionPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getIntrusionPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPrefilterPolicy - errors', () => {
      it('should have a getAllPrefilterPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getAllPrefilterPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllPrefilterPolicy(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllPrefilterPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPrefilterHitCount - errors', () => {
      it('should have a getPrefilterHitCount function', (done) => {
        try {
          assert.equal(true, typeof a.getPrefilterHitCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.getPrefilterHitCount(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getPrefilterHitCount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getPrefilterHitCount('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getPrefilterHitCount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getPrefilterHitCount('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getPrefilterHitCount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePrefilterHitCount - errors', () => {
      it('should have a updatePrefilterHitCount function', (done) => {
        try {
          assert.equal(true, typeof a.updatePrefilterHitCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.updatePrefilterHitCount(null, null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updatePrefilterHitCount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.updatePrefilterHitCount('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updatePrefilterHitCount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updatePrefilterHitCount('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updatePrefilterHitCount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePrefilterHitCount - errors', () => {
      it('should have a deletePrefilterHitCount function', (done) => {
        try {
          assert.equal(true, typeof a.deletePrefilterHitCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.deletePrefilterHitCount(null, null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deletePrefilterHitCount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.deletePrefilterHitCount('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deletePrefilterHitCount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deletePrefilterHitCount('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deletePrefilterHitCount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPrefilterPolicy - errors', () => {
      it('should have a getPrefilterPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getPrefilterPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getPrefilterPolicy(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getPrefilterPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getPrefilterPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getPrefilterPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSNMPConfig - errors', () => {
      it('should have a getAllSNMPConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getAllSNMPConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllSNMPConfig(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllSNMPConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSNMPConfig - errors', () => {
      it('should have a getSNMPConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getSNMPConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getSNMPConfig(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getSNMPConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getSNMPConfig('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getSNMPConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSyslogConfig - errors', () => {
      it('should have a getAllSyslogConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getAllSyslogConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllSyslogConfig(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllSyslogConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSyslogConfig - errors', () => {
      it('should have a getSyslogConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getSyslogConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getSyslogConfig(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getSyslogConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getSyslogConfig('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getSyslogConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllAnyProtocolPortObject - errors', () => {
      it('should have a getAllAnyProtocolPortObject function', (done) => {
        try {
          assert.equal(true, typeof a.getAllAnyProtocolPortObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllAnyProtocolPortObject(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllAnyProtocolPortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAnyProtocolPortObject - errors', () => {
      it('should have a getAnyProtocolPortObject function', (done) => {
        try {
          assert.equal(true, typeof a.getAnyProtocolPortObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getAnyProtocolPortObject(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAnyProtocolPortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAnyProtocolPortObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAnyProtocolPortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllApplicationCategory - errors', () => {
      it('should have a getAllApplicationCategory function', (done) => {
        try {
          assert.equal(true, typeof a.getAllApplicationCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllApplicationCategory(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllApplicationCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationCategory - errors', () => {
      it('should have a getApplicationCategory function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getApplicationCategory(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getApplicationCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getApplicationCategory('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getApplicationCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllApplicationFilter - errors', () => {
      it('should have a getAllApplicationFilter function', (done) => {
        try {
          assert.equal(true, typeof a.getAllApplicationFilter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllApplicationFilter(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllApplicationFilter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationFilter - errors', () => {
      it('should have a getApplicationFilter function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationFilter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getApplicationFilter(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getApplicationFilter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getApplicationFilter('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getApplicationFilter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllApplicationProductivity - errors', () => {
      it('should have a getAllApplicationProductivity function', (done) => {
        try {
          assert.equal(true, typeof a.getAllApplicationProductivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllApplicationProductivity(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllApplicationProductivity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationProductivity - errors', () => {
      it('should have a getApplicationProductivity function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationProductivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getApplicationProductivity(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getApplicationProductivity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getApplicationProductivity('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getApplicationProductivity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllApplicationRisk - errors', () => {
      it('should have a getAllApplicationRisk function', (done) => {
        try {
          assert.equal(true, typeof a.getAllApplicationRisk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllApplicationRisk(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllApplicationRisk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationRisk - errors', () => {
      it('should have a getApplicationRisk function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationRisk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getApplicationRisk(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getApplicationRisk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getApplicationRisk('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getApplicationRisk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllApplication - errors', () => {
      it('should have a getAllApplication function', (done) => {
        try {
          assert.equal(true, typeof a.getAllApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllApplication(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplication - errors', () => {
      it('should have a getApplication function', (done) => {
        try {
          assert.equal(true, typeof a.getApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getApplication(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getApplication('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllApplicationTag - errors', () => {
      it('should have a getAllApplicationTag function', (done) => {
        try {
          assert.equal(true, typeof a.getAllApplicationTag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllApplicationTag(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllApplicationTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationTag - errors', () => {
      it('should have a getApplicationTag function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationTag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getApplicationTag(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getApplicationTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getApplicationTag('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getApplicationTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllApplicationType - errors', () => {
      it('should have a getAllApplicationType function', (done) => {
        try {
          assert.equal(true, typeof a.getAllApplicationType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllApplicationType(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllApplicationType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationType - errors', () => {
      it('should have a getApplicationType function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getApplicationType(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getApplicationType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getApplicationType('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getApplicationType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllVpnPKIEnrollmentModel - errors', () => {
      it('should have a getAllVpnPKIEnrollmentModel function', (done) => {
        try {
          assert.equal(true, typeof a.getAllVpnPKIEnrollmentModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllVpnPKIEnrollmentModel(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllVpnPKIEnrollmentModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVpnPKIEnrollmentModel - errors', () => {
      it('should have a getVpnPKIEnrollmentModel function', (done) => {
        try {
          assert.equal(true, typeof a.getVpnPKIEnrollmentModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getVpnPKIEnrollmentModel(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getVpnPKIEnrollmentModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getVpnPKIEnrollmentModel('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getVpnPKIEnrollmentModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllContinentObject - errors', () => {
      it('should have a getAllContinentObject function', (done) => {
        try {
          assert.equal(true, typeof a.getAllContinentObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllContinentObject(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllContinentObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getContinentObject - errors', () => {
      it('should have a getContinentObject function', (done) => {
        try {
          assert.equal(true, typeof a.getContinentObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getContinentObject(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getContinentObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getContinentObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getContinentObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllCountryObject - errors', () => {
      it('should have a getAllCountryObject function', (done) => {
        try {
          assert.equal(true, typeof a.getAllCountryObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllCountryObject(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllCountryObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCountryObject - errors', () => {
      it('should have a getCountryObject function', (done) => {
        try {
          assert.equal(true, typeof a.getCountryObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getCountryObject(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getCountryObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getCountryObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getCountryObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllDNSServerGroupObject - errors', () => {
      it('should have a getAllDNSServerGroupObject function', (done) => {
        try {
          assert.equal(true, typeof a.getAllDNSServerGroupObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllDNSServerGroupObject(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllDNSServerGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDNSServerGroupObject - errors', () => {
      it('should have a createDNSServerGroupObject function', (done) => {
        try {
          assert.equal(true, typeof a.createDNSServerGroupObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createDNSServerGroupObject(null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createDNSServerGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDNSServerGroupObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createDNSServerGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDNSServerGroupObject - errors', () => {
      it('should have a getDNSServerGroupObject function', (done) => {
        try {
          assert.equal(true, typeof a.getDNSServerGroupObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getDNSServerGroupObject(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getDNSServerGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getDNSServerGroupObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getDNSServerGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDNSServerGroupObject - errors', () => {
      it('should have a updateDNSServerGroupObject function', (done) => {
        try {
          assert.equal(true, typeof a.updateDNSServerGroupObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateDNSServerGroupObject(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateDNSServerGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateDNSServerGroupObject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateDNSServerGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateDNSServerGroupObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateDNSServerGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDNSServerGroupObject - errors', () => {
      it('should have a deleteDNSServerGroupObject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDNSServerGroupObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteDNSServerGroupObject(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteDNSServerGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteDNSServerGroupObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteDNSServerGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllEndPointDeviceType - errors', () => {
      it('should have a getAllEndPointDeviceType function', (done) => {
        try {
          assert.equal(true, typeof a.getAllEndPointDeviceType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllEndPointDeviceType(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllEndPointDeviceType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEndPointDeviceType - errors', () => {
      it('should have a getEndPointDeviceType function', (done) => {
        try {
          assert.equal(true, typeof a.getEndPointDeviceType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getEndPointDeviceType(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getEndPointDeviceType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getEndPointDeviceType('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getEndPointDeviceType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllExtendedAccessListModel - errors', () => {
      it('should have a getAllExtendedAccessListModel function', (done) => {
        try {
          assert.equal(true, typeof a.getAllExtendedAccessListModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllExtendedAccessListModel(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllExtendedAccessListModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtendedAccessListModel - errors', () => {
      it('should have a getExtendedAccessListModel function', (done) => {
        try {
          assert.equal(true, typeof a.getExtendedAccessListModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getExtendedAccessListModel(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getExtendedAccessListModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getExtendedAccessListModel('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getExtendedAccessListModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFQDNObject - errors', () => {
      it('should have a getAllFQDNObject function', (done) => {
        try {
          assert.equal(true, typeof a.getAllFQDNObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllFQDNObject(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFQDNObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFQDNObject - errors', () => {
      it('should have a createFQDNObject function', (done) => {
        try {
          assert.equal(true, typeof a.createFQDNObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createFQDNObject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFQDNObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createFQDNObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createFQDNObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFQDNOverride - errors', () => {
      it('should have a getAllFQDNOverride function', (done) => {
        try {
          assert.equal(true, typeof a.getAllFQDNOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllFQDNOverride(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFQDNOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllFQDNOverride('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllFQDNOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFQDNOverride - errors', () => {
      it('should have a getFQDNOverride function', (done) => {
        try {
          assert.equal(true, typeof a.getFQDNOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getFQDNOverride(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFQDNOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getFQDNOverride('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFQDNOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getFQDNOverride('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFQDNOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFQDNObject - errors', () => {
      it('should have a getFQDNObject function', (done) => {
        try {
          assert.equal(true, typeof a.getFQDNObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getFQDNObject(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFQDNObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getFQDNObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getFQDNObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFQDNObject - errors', () => {
      it('should have a updateFQDNObject function', (done) => {
        try {
          assert.equal(true, typeof a.updateFQDNObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateFQDNObject(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFQDNObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateFQDNObject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFQDNObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateFQDNObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateFQDNObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFQDNObject - errors', () => {
      it('should have a deleteFQDNObject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFQDNObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteFQDNObject(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFQDNObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteFQDNObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteFQDNObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllGeoLocationObject - errors', () => {
      it('should have a getAllGeoLocationObject function', (done) => {
        try {
          assert.equal(true, typeof a.getAllGeoLocationObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllGeoLocationObject(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllGeoLocationObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGeoLocationObject - errors', () => {
      it('should have a getGeoLocationObject function', (done) => {
        try {
          assert.equal(true, typeof a.getGeoLocationObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getGeoLocationObject(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getGeoLocationObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getGeoLocationObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getGeoLocationObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllHostObject - errors', () => {
      it('should have a getAllHostObject function', (done) => {
        try {
          assert.equal(true, typeof a.getAllHostObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllHostObject(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllHostObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHostObject - errors', () => {
      it('should have a createHostObject function', (done) => {
        try {
          assert.equal(true, typeof a.createHostObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createHostObject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createHostObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHostObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createHostObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllHostOverride - errors', () => {
      it('should have a getAllHostOverride function', (done) => {
        try {
          assert.equal(true, typeof a.getAllHostOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllHostOverride(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllHostOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllHostOverride('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllHostOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostOverride - errors', () => {
      it('should have a getHostOverride function', (done) => {
        try {
          assert.equal(true, typeof a.getHostOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getHostOverride(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getHostOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getHostOverride('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getHostOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getHostOverride('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getHostOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostObject - errors', () => {
      it('should have a getHostObject function', (done) => {
        try {
          assert.equal(true, typeof a.getHostObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getHostObject(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getHostObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getHostObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getHostObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHostObject - errors', () => {
      it('should have a updateHostObject function', (done) => {
        try {
          assert.equal(true, typeof a.updateHostObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateHostObject(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateHostObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateHostObject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateHostObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHostObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateHostObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHostObject - errors', () => {
      it('should have a deleteHostObject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHostObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteHostObject(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteHostObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteHostObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteHostObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllICMPV4Object - errors', () => {
      it('should have a getAllICMPV4Object function', (done) => {
        try {
          assert.equal(true, typeof a.getAllICMPV4Object === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllICMPV4Object(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllICMPV4Object', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createICMPV4Object - errors', () => {
      it('should have a createICMPV4Object function', (done) => {
        try {
          assert.equal(true, typeof a.createICMPV4Object === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createICMPV4Object('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createICMPV4Object', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createICMPV4Object('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createICMPV4Object', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllICMPV4ObjectOverride - errors', () => {
      it('should have a getAllICMPV4ObjectOverride function', (done) => {
        try {
          assert.equal(true, typeof a.getAllICMPV4ObjectOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllICMPV4ObjectOverride(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllICMPV4ObjectOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllICMPV4ObjectOverride('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllICMPV4ObjectOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getICMPV4ObjectOverride - errors', () => {
      it('should have a getICMPV4ObjectOverride function', (done) => {
        try {
          assert.equal(true, typeof a.getICMPV4ObjectOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getICMPV4ObjectOverride(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getICMPV4ObjectOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getICMPV4ObjectOverride('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getICMPV4ObjectOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getICMPV4ObjectOverride('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getICMPV4ObjectOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getICMPV4Object - errors', () => {
      it('should have a getICMPV4Object function', (done) => {
        try {
          assert.equal(true, typeof a.getICMPV4Object === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getICMPV4Object(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getICMPV4Object', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getICMPV4Object('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getICMPV4Object', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateICMPV4Object - errors', () => {
      it('should have a updateICMPV4Object function', (done) => {
        try {
          assert.equal(true, typeof a.updateICMPV4Object === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateICMPV4Object(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateICMPV4Object', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateICMPV4Object('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateICMPV4Object', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateICMPV4Object('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateICMPV4Object', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteICMPV4Object - errors', () => {
      it('should have a deleteICMPV4Object function', (done) => {
        try {
          assert.equal(true, typeof a.deleteICMPV4Object === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteICMPV4Object(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteICMPV4Object', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteICMPV4Object('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteICMPV4Object', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllICMPV6Object - errors', () => {
      it('should have a getAllICMPV6Object function', (done) => {
        try {
          assert.equal(true, typeof a.getAllICMPV6Object === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllICMPV6Object(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllICMPV6Object', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createICMPV6Object - errors', () => {
      it('should have a createICMPV6Object function', (done) => {
        try {
          assert.equal(true, typeof a.createICMPV6Object === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createICMPV6Object('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createICMPV6Object', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createICMPV6Object('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createICMPV6Object', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllICMPV6ObjectOverride - errors', () => {
      it('should have a getAllICMPV6ObjectOverride function', (done) => {
        try {
          assert.equal(true, typeof a.getAllICMPV6ObjectOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllICMPV6ObjectOverride(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllICMPV6ObjectOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllICMPV6ObjectOverride('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllICMPV6ObjectOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getICMPV6ObjectOverride - errors', () => {
      it('should have a getICMPV6ObjectOverride function', (done) => {
        try {
          assert.equal(true, typeof a.getICMPV6ObjectOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getICMPV6ObjectOverride(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getICMPV6ObjectOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getICMPV6ObjectOverride('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getICMPV6ObjectOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getICMPV6ObjectOverride('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getICMPV6ObjectOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getICMPV6Object - errors', () => {
      it('should have a getICMPV6Object function', (done) => {
        try {
          assert.equal(true, typeof a.getICMPV6Object === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getICMPV6Object(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getICMPV6Object', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getICMPV6Object('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getICMPV6Object', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateICMPV6Object - errors', () => {
      it('should have a updateICMPV6Object function', (done) => {
        try {
          assert.equal(true, typeof a.updateICMPV6Object === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateICMPV6Object(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateICMPV6Object', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateICMPV6Object('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateICMPV6Object', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateICMPV6Object('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateICMPV6Object', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteICMPV6Object - errors', () => {
      it('should have a deleteICMPV6Object function', (done) => {
        try {
          assert.equal(true, typeof a.deleteICMPV6Object === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteICMPV6Object(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteICMPV6Object', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteICMPV6Object('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteICMPV6Object', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllIKEv1IPsecProposal - errors', () => {
      it('should have a getAllIKEv1IPsecProposal function', (done) => {
        try {
          assert.equal(true, typeof a.getAllIKEv1IPsecProposal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllIKEv1IPsecProposal(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllIKEv1IPsecProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIKEv1IPsecProposal - errors', () => {
      it('should have a createIKEv1IPsecProposal function', (done) => {
        try {
          assert.equal(true, typeof a.createIKEv1IPsecProposal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createIKEv1IPsecProposal(null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createIKEv1IPsecProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIKEv1IPsecProposal('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createIKEv1IPsecProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIKEv1IPsecProposal - errors', () => {
      it('should have a getIKEv1IPsecProposal function', (done) => {
        try {
          assert.equal(true, typeof a.getIKEv1IPsecProposal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getIKEv1IPsecProposal(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getIKEv1IPsecProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getIKEv1IPsecProposal('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getIKEv1IPsecProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIKEv1IPsecProposal - errors', () => {
      it('should have a updateIKEv1IPsecProposal function', (done) => {
        try {
          assert.equal(true, typeof a.updateIKEv1IPsecProposal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateIKEv1IPsecProposal(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateIKEv1IPsecProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateIKEv1IPsecProposal('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateIKEv1IPsecProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIKEv1IPsecProposal('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateIKEv1IPsecProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIKEv1IPsecProposal - errors', () => {
      it('should have a deleteIKEv1IPsecProposal function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIKEv1IPsecProposal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteIKEv1IPsecProposal(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteIKEv1IPsecProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteIKEv1IPsecProposal('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteIKEv1IPsecProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllIkev1PolicyObject - errors', () => {
      it('should have a getAllIkev1PolicyObject function', (done) => {
        try {
          assert.equal(true, typeof a.getAllIkev1PolicyObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllIkev1PolicyObject(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllIkev1PolicyObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIkev1PolicyObject - errors', () => {
      it('should have a createIkev1PolicyObject function', (done) => {
        try {
          assert.equal(true, typeof a.createIkev1PolicyObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createIkev1PolicyObject(null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createIkev1PolicyObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIkev1PolicyObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createIkev1PolicyObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIkev1PolicyObject - errors', () => {
      it('should have a getIkev1PolicyObject function', (done) => {
        try {
          assert.equal(true, typeof a.getIkev1PolicyObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getIkev1PolicyObject(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getIkev1PolicyObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getIkev1PolicyObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getIkev1PolicyObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIkev1PolicyObject - errors', () => {
      it('should have a updateIkev1PolicyObject function', (done) => {
        try {
          assert.equal(true, typeof a.updateIkev1PolicyObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateIkev1PolicyObject(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateIkev1PolicyObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateIkev1PolicyObject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateIkev1PolicyObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIkev1PolicyObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateIkev1PolicyObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIkev1PolicyObject - errors', () => {
      it('should have a deleteIkev1PolicyObject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIkev1PolicyObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteIkev1PolicyObject(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteIkev1PolicyObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteIkev1PolicyObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteIkev1PolicyObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllIKEv2IPsecProposal - errors', () => {
      it('should have a getAllIKEv2IPsecProposal function', (done) => {
        try {
          assert.equal(true, typeof a.getAllIKEv2IPsecProposal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllIKEv2IPsecProposal(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllIKEv2IPsecProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIKEv2IPsecProposal - errors', () => {
      it('should have a createIKEv2IPsecProposal function', (done) => {
        try {
          assert.equal(true, typeof a.createIKEv2IPsecProposal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createIKEv2IPsecProposal(null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createIKEv2IPsecProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIKEv2IPsecProposal('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createIKEv2IPsecProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIKEv2IPsecProposal - errors', () => {
      it('should have a getIKEv2IPsecProposal function', (done) => {
        try {
          assert.equal(true, typeof a.getIKEv2IPsecProposal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getIKEv2IPsecProposal(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getIKEv2IPsecProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getIKEv2IPsecProposal('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getIKEv2IPsecProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIKEv2IPsecProposal - errors', () => {
      it('should have a updateIKEv2IPsecProposal function', (done) => {
        try {
          assert.equal(true, typeof a.updateIKEv2IPsecProposal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateIKEv2IPsecProposal(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateIKEv2IPsecProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateIKEv2IPsecProposal('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateIKEv2IPsecProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIKEv2IPsecProposal('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateIKEv2IPsecProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIKEv2IPsecProposal - errors', () => {
      it('should have a deleteIKEv2IPsecProposal function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIKEv2IPsecProposal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteIKEv2IPsecProposal(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteIKEv2IPsecProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteIKEv2IPsecProposal('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteIKEv2IPsecProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllIkev2PolicyObject - errors', () => {
      it('should have a getAllIkev2PolicyObject function', (done) => {
        try {
          assert.equal(true, typeof a.getAllIkev2PolicyObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllIkev2PolicyObject(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllIkev2PolicyObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIkev2PolicyObject - errors', () => {
      it('should have a createIkev2PolicyObject function', (done) => {
        try {
          assert.equal(true, typeof a.createIkev2PolicyObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createIkev2PolicyObject(null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createIkev2PolicyObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIkev2PolicyObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createIkev2PolicyObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIkev2PolicyObject - errors', () => {
      it('should have a getIkev2PolicyObject function', (done) => {
        try {
          assert.equal(true, typeof a.getIkev2PolicyObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getIkev2PolicyObject(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getIkev2PolicyObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getIkev2PolicyObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getIkev2PolicyObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIkev2PolicyObject - errors', () => {
      it('should have a updateIkev2PolicyObject function', (done) => {
        try {
          assert.equal(true, typeof a.updateIkev2PolicyObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateIkev2PolicyObject(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateIkev2PolicyObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateIkev2PolicyObject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateIkev2PolicyObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIkev2PolicyObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateIkev2PolicyObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIkev2PolicyObject - errors', () => {
      it('should have a deleteIkev2PolicyObject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIkev2PolicyObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteIkev2PolicyObject(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteIkev2PolicyObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteIkev2PolicyObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteIkev2PolicyObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllInterfaceGroupObject - errors', () => {
      it('should have a getAllInterfaceGroupObject function', (done) => {
        try {
          assert.equal(true, typeof a.getAllInterfaceGroupObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllInterfaceGroupObject(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllInterfaceGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createInterfaceGroupObject - errors', () => {
      it('should have a createInterfaceGroupObject function', (done) => {
        try {
          assert.equal(true, typeof a.createInterfaceGroupObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createInterfaceGroupObject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createInterfaceGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createInterfaceGroupObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createInterfaceGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterfaceGroupObject - errors', () => {
      it('should have a getInterfaceGroupObject function', (done) => {
        try {
          assert.equal(true, typeof a.getInterfaceGroupObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getInterfaceGroupObject(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getInterfaceGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getInterfaceGroupObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getInterfaceGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateInterfaceGroupObject - errors', () => {
      it('should have a updateInterfaceGroupObject function', (done) => {
        try {
          assert.equal(true, typeof a.updateInterfaceGroupObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateInterfaceGroupObject(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateInterfaceGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateInterfaceGroupObject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateInterfaceGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateInterfaceGroupObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateInterfaceGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInterfaceGroupObject - errors', () => {
      it('should have a deleteInterfaceGroupObject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteInterfaceGroupObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteInterfaceGroupObject(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteInterfaceGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteInterfaceGroupObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteInterfaceGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllInterfaceObject - errors', () => {
      it('should have a getAllInterfaceObject function', (done) => {
        try {
          assert.equal(true, typeof a.getAllInterfaceObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllInterfaceObject(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllInterfaceObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterfaceObject - errors', () => {
      it('should have a getInterfaceObject function', (done) => {
        try {
          assert.equal(true, typeof a.getInterfaceObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getInterfaceObject(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getInterfaceObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getInterfaceObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getInterfaceObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllISESecurityGroupTag - errors', () => {
      it('should have a getAllISESecurityGroupTag function', (done) => {
        try {
          assert.equal(true, typeof a.getAllISESecurityGroupTag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllISESecurityGroupTag(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllISESecurityGroupTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getISESecurityGroupTag - errors', () => {
      it('should have a getISESecurityGroupTag function', (done) => {
        try {
          assert.equal(true, typeof a.getISESecurityGroupTag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getISESecurityGroupTag(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getISESecurityGroupTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getISESecurityGroupTag('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getISESecurityGroupTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllKeyChainObject - errors', () => {
      it('should have a getAllKeyChainObject function', (done) => {
        try {
          assert.equal(true, typeof a.getAllKeyChainObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllKeyChainObject(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllKeyChainObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createKeyChainObject - errors', () => {
      it('should have a createKeyChainObject function', (done) => {
        try {
          assert.equal(true, typeof a.createKeyChainObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createKeyChainObject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createKeyChainObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createKeyChainObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createKeyChainObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllKeyChainObjectOverride - errors', () => {
      it('should have a getAllKeyChainObjectOverride function', (done) => {
        try {
          assert.equal(true, typeof a.getAllKeyChainObjectOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllKeyChainObjectOverride(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllKeyChainObjectOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllKeyChainObjectOverride('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllKeyChainObjectOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getKeyChainObjectOverride - errors', () => {
      it('should have a getKeyChainObjectOverride function', (done) => {
        try {
          assert.equal(true, typeof a.getKeyChainObjectOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getKeyChainObjectOverride(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getKeyChainObjectOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getKeyChainObjectOverride('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getKeyChainObjectOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getKeyChainObjectOverride('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getKeyChainObjectOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getKeyChainObject - errors', () => {
      it('should have a getKeyChainObject function', (done) => {
        try {
          assert.equal(true, typeof a.getKeyChainObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getKeyChainObject(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getKeyChainObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getKeyChainObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getKeyChainObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateKeyChainObject - errors', () => {
      it('should have a updateKeyChainObject function', (done) => {
        try {
          assert.equal(true, typeof a.updateKeyChainObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateKeyChainObject(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateKeyChainObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateKeyChainObject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateKeyChainObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateKeyChainObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateKeyChainObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteKeyChainObject - errors', () => {
      it('should have a deleteKeyChainObject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteKeyChainObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteKeyChainObject(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteKeyChainObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteKeyChainObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteKeyChainObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkAddress - errors', () => {
      it('should have a getNetworkAddress function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getNetworkAddress(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getNetworkAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllNetworkGroup - errors', () => {
      it('should have a getAllNetworkGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getAllNetworkGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllNetworkGroup(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllNetworkGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkGroup - errors', () => {
      it('should have a createNetworkGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createNetworkGroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createNetworkGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNetworkGroup('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createNetworkGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllNetworkGroupOverride - errors', () => {
      it('should have a getAllNetworkGroupOverride function', (done) => {
        try {
          assert.equal(true, typeof a.getAllNetworkGroupOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllNetworkGroupOverride(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllNetworkGroupOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllNetworkGroupOverride('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllNetworkGroupOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkGroupOverride - errors', () => {
      it('should have a getNetworkGroupOverride function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkGroupOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getNetworkGroupOverride(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getNetworkGroupOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getNetworkGroupOverride('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getNetworkGroupOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getNetworkGroupOverride('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getNetworkGroupOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkGroup - errors', () => {
      it('should have a getNetworkGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getNetworkGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getNetworkGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getNetworkGroup('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getNetworkGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkGroup - errors', () => {
      it('should have a updateNetworkGroup function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateNetworkGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateNetworkGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateNetworkGroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateNetworkGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateNetworkGroup('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateNetworkGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkGroup - errors', () => {
      it('should have a deleteNetworkGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteNetworkGroup(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteNetworkGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteNetworkGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteNetworkGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllNetworkObject - errors', () => {
      it('should have a getAllNetworkObject function', (done) => {
        try {
          assert.equal(true, typeof a.getAllNetworkObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllNetworkObject(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllNetworkObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkObject - errors', () => {
      it('should have a createNetworkObject function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createNetworkObject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createNetworkObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNetworkObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createNetworkObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllNetworkOverride - errors', () => {
      it('should have a getAllNetworkOverride function', (done) => {
        try {
          assert.equal(true, typeof a.getAllNetworkOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllNetworkOverride(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllNetworkOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllNetworkOverride('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllNetworkOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkObject - errors', () => {
      it('should have a getNetworkObject function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getNetworkObject(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getNetworkObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getNetworkObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getNetworkObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkObject - errors', () => {
      it('should have a updateNetworkObject function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateNetworkObject(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateNetworkObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateNetworkObject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateNetworkObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateNetworkObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateNetworkObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkObject - errors', () => {
      it('should have a deleteNetworkObject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteNetworkObject(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteNetworkObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteNetworkObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteNetworkObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPortObjectGroup - errors', () => {
      it('should have a getAllPortObjectGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getAllPortObjectGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllPortObjectGroup(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllPortObjectGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPortObjectGroup - errors', () => {
      it('should have a createPortObjectGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createPortObjectGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createPortObjectGroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createPortObjectGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createPortObjectGroup('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createPortObjectGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPortObjectGroupOverride - errors', () => {
      it('should have a getAllPortObjectGroupOverride function', (done) => {
        try {
          assert.equal(true, typeof a.getAllPortObjectGroupOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllPortObjectGroupOverride(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllPortObjectGroupOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllPortObjectGroupOverride('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllPortObjectGroupOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPortObjectGroupOverride - errors', () => {
      it('should have a getPortObjectGroupOverride function', (done) => {
        try {
          assert.equal(true, typeof a.getPortObjectGroupOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getPortObjectGroupOverride(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getPortObjectGroupOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getPortObjectGroupOverride('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getPortObjectGroupOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getPortObjectGroupOverride('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getPortObjectGroupOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPortObjectGroup - errors', () => {
      it('should have a getPortObjectGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getPortObjectGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getPortObjectGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getPortObjectGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getPortObjectGroup('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getPortObjectGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePortObjectGroup - errors', () => {
      it('should have a updatePortObjectGroup function', (done) => {
        try {
          assert.equal(true, typeof a.updatePortObjectGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updatePortObjectGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updatePortObjectGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updatePortObjectGroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updatePortObjectGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePortObjectGroup('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updatePortObjectGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePortObjectGroup - errors', () => {
      it('should have a deletePortObjectGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deletePortObjectGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deletePortObjectGroup(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deletePortObjectGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deletePortObjectGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deletePortObjectGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPortObject - errors', () => {
      it('should have a getPortObject function', (done) => {
        try {
          assert.equal(true, typeof a.getPortObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getPortObject(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getPortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllProtocolPortObject - errors', () => {
      it('should have a getAllProtocolPortObject function', (done) => {
        try {
          assert.equal(true, typeof a.getAllProtocolPortObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllProtocolPortObject(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllProtocolPortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createProtocolPortObject - errors', () => {
      it('should have a createProtocolPortObject function', (done) => {
        try {
          assert.equal(true, typeof a.createProtocolPortObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createProtocolPortObject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createProtocolPortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createProtocolPortObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createProtocolPortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllProtocolPortObjectOverride - errors', () => {
      it('should have a getAllProtocolPortObjectOverride function', (done) => {
        try {
          assert.equal(true, typeof a.getAllProtocolPortObjectOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllProtocolPortObjectOverride(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllProtocolPortObjectOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllProtocolPortObjectOverride('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllProtocolPortObjectOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProtocolPortObjectOverride - errors', () => {
      it('should have a getProtocolPortObjectOverride function', (done) => {
        try {
          assert.equal(true, typeof a.getProtocolPortObjectOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getProtocolPortObjectOverride(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getProtocolPortObjectOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getProtocolPortObjectOverride('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getProtocolPortObjectOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getProtocolPortObjectOverride('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getProtocolPortObjectOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProtocolPortObject - errors', () => {
      it('should have a getProtocolPortObject function', (done) => {
        try {
          assert.equal(true, typeof a.getProtocolPortObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getProtocolPortObject(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getProtocolPortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getProtocolPortObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getProtocolPortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateProtocolPortObject - errors', () => {
      it('should have a updateProtocolPortObject function', (done) => {
        try {
          assert.equal(true, typeof a.updateProtocolPortObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateProtocolPortObject(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateProtocolPortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateProtocolPortObject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateProtocolPortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateProtocolPortObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateProtocolPortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteProtocolPortObject - errors', () => {
      it('should have a deleteProtocolPortObject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteProtocolPortObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteProtocolPortObject(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteProtocolPortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteProtocolPortObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteProtocolPortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRangeObject - errors', () => {
      it('should have a getAllRangeObject function', (done) => {
        try {
          assert.equal(true, typeof a.getAllRangeObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllRangeObject(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllRangeObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRangeObject - errors', () => {
      it('should have a createRangeObject function', (done) => {
        try {
          assert.equal(true, typeof a.createRangeObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createRangeObject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createRangeObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRangeObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createRangeObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRangeOverride - errors', () => {
      it('should have a getAllRangeOverride function', (done) => {
        try {
          assert.equal(true, typeof a.getAllRangeOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllRangeOverride(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllRangeOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllRangeOverride('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllRangeOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRangeOverride - errors', () => {
      it('should have a getRangeOverride function', (done) => {
        try {
          assert.equal(true, typeof a.getRangeOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getRangeOverride(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getRangeOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getRangeOverride('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getRangeOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getRangeOverride('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getRangeOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRangeObject - errors', () => {
      it('should have a getRangeObject function', (done) => {
        try {
          assert.equal(true, typeof a.getRangeObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getRangeObject(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getRangeObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getRangeObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getRangeObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRangeObject - errors', () => {
      it('should have a updateRangeObject function', (done) => {
        try {
          assert.equal(true, typeof a.updateRangeObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateRangeObject(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateRangeObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateRangeObject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateRangeObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateRangeObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateRangeObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRangeObject - errors', () => {
      it('should have a deleteRangeObject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRangeObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteRangeObject(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteRangeObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteRangeObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteRangeObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRealm - errors', () => {
      it('should have a getAllRealm function', (done) => {
        try {
          assert.equal(true, typeof a.getAllRealm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllRealm(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllRealm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRealm - errors', () => {
      it('should have a getRealm function', (done) => {
        try {
          assert.equal(true, typeof a.getRealm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getRealm(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getRealm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getRealm('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getRealm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRealmUserGroup - errors', () => {
      it('should have a getAllRealmUserGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getAllRealmUserGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllRealmUserGroup(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllRealmUserGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRealmUserGroup - errors', () => {
      it('should have a getRealmUserGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getRealmUserGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getRealmUserGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getRealmUserGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing realmUuid', (done) => {
        try {
          a.getRealmUserGroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'realmUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getRealmUserGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getRealmUserGroup('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getRealmUserGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRealmUser - errors', () => {
      it('should have a getAllRealmUser function', (done) => {
        try {
          assert.equal(true, typeof a.getAllRealmUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllRealmUser(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllRealmUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRealmUser - errors', () => {
      it('should have a getRealmUser function', (done) => {
        try {
          assert.equal(true, typeof a.getRealmUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getRealmUser(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getRealmUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing realmUuid', (done) => {
        try {
          a.getRealmUser('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'realmUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getRealmUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getRealmUser('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getRealmUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSecurityGroupTag - errors', () => {
      it('should have a getAllSecurityGroupTag function', (done) => {
        try {
          assert.equal(true, typeof a.getAllSecurityGroupTag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllSecurityGroupTag(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllSecurityGroupTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityGroupTag - errors', () => {
      it('should have a getSecurityGroupTag function', (done) => {
        try {
          assert.equal(true, typeof a.getSecurityGroupTag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getSecurityGroupTag(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getSecurityGroupTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getSecurityGroupTag('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getSecurityGroupTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSecurityZoneObject - errors', () => {
      it('should have a getAllSecurityZoneObject function', (done) => {
        try {
          assert.equal(true, typeof a.getAllSecurityZoneObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllSecurityZoneObject(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllSecurityZoneObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSecurityZoneObject - errors', () => {
      it('should have a createSecurityZoneObject function', (done) => {
        try {
          assert.equal(true, typeof a.createSecurityZoneObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createSecurityZoneObject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createSecurityZoneObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSecurityZoneObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createSecurityZoneObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityZoneObject - errors', () => {
      it('should have a getSecurityZoneObject function', (done) => {
        try {
          assert.equal(true, typeof a.getSecurityZoneObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getSecurityZoneObject(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getSecurityZoneObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getSecurityZoneObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getSecurityZoneObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSecurityZoneObject - errors', () => {
      it('should have a updateSecurityZoneObject function', (done) => {
        try {
          assert.equal(true, typeof a.updateSecurityZoneObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateSecurityZoneObject(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateSecurityZoneObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateSecurityZoneObject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateSecurityZoneObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSecurityZoneObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateSecurityZoneObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecurityZoneObject - errors', () => {
      it('should have a deleteSecurityZoneObject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecurityZoneObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteSecurityZoneObject(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteSecurityZoneObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteSecurityZoneObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteSecurityZoneObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSIURLFeed - errors', () => {
      it('should have a getAllSIURLFeed function', (done) => {
        try {
          assert.equal(true, typeof a.getAllSIURLFeed === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllSIURLFeed(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllSIURLFeed', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSIURLFeed - errors', () => {
      it('should have a getSIURLFeed function', (done) => {
        try {
          assert.equal(true, typeof a.getSIURLFeed === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getSIURLFeed(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getSIURLFeed', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getSIURLFeed('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getSIURLFeed', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSIURLList - errors', () => {
      it('should have a getAllSIURLList function', (done) => {
        try {
          assert.equal(true, typeof a.getAllSIURLList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllSIURLList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllSIURLList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSIURLList - errors', () => {
      it('should have a getSIURLList function', (done) => {
        try {
          assert.equal(true, typeof a.getSIURLList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getSIURLList(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getSIURLList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getSIURLList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getSIURLList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSLAMonitorObjectModel - errors', () => {
      it('should have a getAllSLAMonitorObjectModel function', (done) => {
        try {
          assert.equal(true, typeof a.getAllSLAMonitorObjectModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllSLAMonitorObjectModel(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllSLAMonitorObjectModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSLAMonitorObjectModel - errors', () => {
      it('should have a createSLAMonitorObjectModel function', (done) => {
        try {
          assert.equal(true, typeof a.createSLAMonitorObjectModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createSLAMonitorObjectModel('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createSLAMonitorObjectModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSLAMonitorObjectModel('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createSLAMonitorObjectModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSLAMonitorObjectModel - errors', () => {
      it('should have a getSLAMonitorObjectModel function', (done) => {
        try {
          assert.equal(true, typeof a.getSLAMonitorObjectModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getSLAMonitorObjectModel(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getSLAMonitorObjectModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getSLAMonitorObjectModel('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getSLAMonitorObjectModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSLAMonitorObjectModel - errors', () => {
      it('should have a updateSLAMonitorObjectModel function', (done) => {
        try {
          assert.equal(true, typeof a.updateSLAMonitorObjectModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateSLAMonitorObjectModel(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateSLAMonitorObjectModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateSLAMonitorObjectModel('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateSLAMonitorObjectModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSLAMonitorObjectModel('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateSLAMonitorObjectModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSLAMonitorObjectModel - errors', () => {
      it('should have a deleteSLAMonitorObjectModel function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSLAMonitorObjectModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteSLAMonitorObjectModel(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteSLAMonitorObjectModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteSLAMonitorObjectModel('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteSLAMonitorObjectModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllTunnelTags - errors', () => {
      it('should have a getAllTunnelTags function', (done) => {
        try {
          assert.equal(true, typeof a.getAllTunnelTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllTunnelTags(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllTunnelTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTunnelTags - errors', () => {
      it('should have a getTunnelTags function', (done) => {
        try {
          assert.equal(true, typeof a.getTunnelTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getTunnelTags(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getTunnelTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getTunnelTags('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getTunnelTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllURLCategory - errors', () => {
      it('should have a getAllURLCategory function', (done) => {
        try {
          assert.equal(true, typeof a.getAllURLCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllURLCategory(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllURLCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getURLCategory - errors', () => {
      it('should have a getURLCategory function', (done) => {
        try {
          assert.equal(true, typeof a.getURLCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getURLCategory(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getURLCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getURLCategory('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getURLCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllURLGroupObject - errors', () => {
      it('should have a getAllURLGroupObject function', (done) => {
        try {
          assert.equal(true, typeof a.getAllURLGroupObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllURLGroupObject(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllURLGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createURLGroupObject - errors', () => {
      it('should have a createURLGroupObject function', (done) => {
        try {
          assert.equal(true, typeof a.createURLGroupObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createURLGroupObject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createURLGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createURLGroupObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createURLGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllUrlGroupOverride - errors', () => {
      it('should have a getAllUrlGroupOverride function', (done) => {
        try {
          assert.equal(true, typeof a.getAllUrlGroupOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllUrlGroupOverride(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllUrlGroupOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllUrlGroupOverride('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllUrlGroupOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUrlGroupOverride - errors', () => {
      it('should have a getUrlGroupOverride function', (done) => {
        try {
          assert.equal(true, typeof a.getUrlGroupOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getUrlGroupOverride(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getUrlGroupOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getUrlGroupOverride('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getUrlGroupOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getUrlGroupOverride('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getUrlGroupOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getURLGroupObject - errors', () => {
      it('should have a getURLGroupObject function', (done) => {
        try {
          assert.equal(true, typeof a.getURLGroupObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getURLGroupObject(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getURLGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getURLGroupObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getURLGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateURLGroupObject - errors', () => {
      it('should have a updateURLGroupObject function', (done) => {
        try {
          assert.equal(true, typeof a.updateURLGroupObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateURLGroupObject(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateURLGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateURLGroupObject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateURLGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateURLGroupObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateURLGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteURLGroupObject - errors', () => {
      it('should have a deleteURLGroupObject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteURLGroupObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteURLGroupObject(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteURLGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteURLGroupObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteURLGroupObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllURLObject - errors', () => {
      it('should have a getAllURLObject function', (done) => {
        try {
          assert.equal(true, typeof a.getAllURLObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllURLObject(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllURLObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createURLObject - errors', () => {
      it('should have a createURLObject function', (done) => {
        try {
          assert.equal(true, typeof a.createURLObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createURLObject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createURLObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createURLObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createURLObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllUrlOverride - errors', () => {
      it('should have a getAllUrlOverride function', (done) => {
        try {
          assert.equal(true, typeof a.getAllUrlOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllUrlOverride(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllUrlOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllUrlOverride('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllUrlOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUrlOverride - errors', () => {
      it('should have a getUrlOverride function', (done) => {
        try {
          assert.equal(true, typeof a.getUrlOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getUrlOverride(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getUrlOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getUrlOverride('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getUrlOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getUrlOverride('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getUrlOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getURLObject - errors', () => {
      it('should have a getURLObject function', (done) => {
        try {
          assert.equal(true, typeof a.getURLObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getURLObject(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getURLObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getURLObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getURLObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateURLObject - errors', () => {
      it('should have a updateURLObject function', (done) => {
        try {
          assert.equal(true, typeof a.updateURLObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateURLObject(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateURLObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateURLObject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateURLObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateURLObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateURLObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteURLObject - errors', () => {
      it('should have a deleteURLObject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteURLObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteURLObject(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteURLObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteURLObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteURLObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllVariableSet - errors', () => {
      it('should have a getAllVariableSet function', (done) => {
        try {
          assert.equal(true, typeof a.getAllVariableSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllVariableSet(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllVariableSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVariableSet - errors', () => {
      it('should have a getVariableSet function', (done) => {
        try {
          assert.equal(true, typeof a.getVariableSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getVariableSet(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getVariableSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getVariableSet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getVariableSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllVlanTagGroup - errors', () => {
      it('should have a getAllVlanTagGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getAllVlanTagGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllVlanTagGroup(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllVlanTagGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVlanTagGroup - errors', () => {
      it('should have a createVlanTagGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createVlanTagGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createVlanTagGroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createVlanTagGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createVlanTagGroup('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createVlanTagGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllVlanTagGroupOverride - errors', () => {
      it('should have a getAllVlanTagGroupOverride function', (done) => {
        try {
          assert.equal(true, typeof a.getAllVlanTagGroupOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllVlanTagGroupOverride(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllVlanTagGroupOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllVlanTagGroupOverride('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllVlanTagGroupOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVlanTagGroupOverride - errors', () => {
      it('should have a getVlanTagGroupOverride function', (done) => {
        try {
          assert.equal(true, typeof a.getVlanTagGroupOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getVlanTagGroupOverride(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getVlanTagGroupOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getVlanTagGroupOverride('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getVlanTagGroupOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getVlanTagGroupOverride('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getVlanTagGroupOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVlanTagGroup - errors', () => {
      it('should have a getVlanTagGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getVlanTagGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getVlanTagGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getVlanTagGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getVlanTagGroup('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getVlanTagGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateVlanTagGroup - errors', () => {
      it('should have a updateVlanTagGroup function', (done) => {
        try {
          assert.equal(true, typeof a.updateVlanTagGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateVlanTagGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateVlanTagGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateVlanTagGroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateVlanTagGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateVlanTagGroup('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateVlanTagGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVlanTagGroup - errors', () => {
      it('should have a deleteVlanTagGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVlanTagGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteVlanTagGroup(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteVlanTagGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteVlanTagGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteVlanTagGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllVlanTag - errors', () => {
      it('should have a getAllVlanTag function', (done) => {
        try {
          assert.equal(true, typeof a.getAllVlanTag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllVlanTag(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllVlanTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVlanTag - errors', () => {
      it('should have a createVlanTag function', (done) => {
        try {
          assert.equal(true, typeof a.createVlanTag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createVlanTag('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createVlanTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createVlanTag('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createVlanTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllVlanOverride - errors', () => {
      it('should have a getAllVlanOverride function', (done) => {
        try {
          assert.equal(true, typeof a.getAllVlanOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getAllVlanOverride(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllVlanOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getAllVlanOverride('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getAllVlanOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVlanOverride - errors', () => {
      it('should have a getVlanOverride function', (done) => {
        try {
          assert.equal(true, typeof a.getVlanOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getVlanOverride(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getVlanOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerUUID', (done) => {
        try {
          a.getVlanOverride('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getVlanOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getVlanOverride('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getVlanOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVlanTag - errors', () => {
      it('should have a getVlanTag function', (done) => {
        try {
          assert.equal(true, typeof a.getVlanTag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.getVlanTag(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getVlanTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getVlanTag('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getVlanTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateVlanTag - errors', () => {
      it('should have a updateVlanTag function', (done) => {
        try {
          assert.equal(true, typeof a.updateVlanTag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.updateVlanTag(null, null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateVlanTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.updateVlanTag('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateVlanTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateVlanTag('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-updateVlanTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVlanTag - errors', () => {
      it('should have a deleteVlanTag function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVlanTag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.deleteVlanTag(null, null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteVlanTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.deleteVlanTag('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-deleteVlanTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeployableDevice - errors', () => {
      it('should have a getDeployableDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDeployableDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.getDeployableDevice(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-getDeployableDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDeploymentRequest - errors', () => {
      it('should have a createDeploymentRequest function', (done) => {
        try {
          assert.equal(true, typeof a.createDeploymentRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainUUID', (done) => {
        try {
          a.createDeploymentRequest(null, null, (data, error) => {
            try {
              const displayE = 'domainUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createDeploymentRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDeploymentRequest('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowermanagementcenter-adapter-createDeploymentRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
