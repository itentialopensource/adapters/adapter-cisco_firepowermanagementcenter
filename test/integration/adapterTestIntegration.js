/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-cisco_firepowermanagementcenter',
      type: 'FirepowerManagementCenter',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const FirepowerManagementCenter = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Firepower_management_center Adapter Test', () => {
  describe('FirepowerManagementCenter Class Tests', () => {
    const a = new FirepowerManagementCenter(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowermanagementcenter-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowermanagementcenter-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const devicesDomainUUID = 'fakedata';
    const devicesCreateDeviceCopyConfigRequestBodyParam = {
      copySharedPolicies: true,
      targetDeviceList: [
        {
          metadata: {
            lastUser: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            domain: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            readOnly: {
              reason: {},
              state: false
            },
            timestamp: 4
          },
          name: 'string',
          description: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string',
          version: 'string'
        }
      ],
      sourceDevice: {
        metadata: {
          lastUser: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: {},
            state: true
          },
          timestamp: 5
        },
        name: 'string',
        description: 'string',
        links: {
          parent: 'string',
          self: 'string'
        },
        id: 'string',
        type: 'string',
        version: 'string'
      }
    };
    describe('#createDeviceCopyConfigRequest - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDeviceCopyConfigRequest(devicesDomainUUID, devicesCreateDeviceCopyConfigRequestBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.sourceDevice);
                assert.equal(true, Array.isArray(data.response.targetDeviceList));
                assert.equal(false, data.response.copySharedPolicies);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'createDeviceCopyConfigRequest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesCreateDeviceBodyParam = {
      hostName: 'string',
      license_caps: [
        'string'
      ],
      regKey: 'string'
    };
    describe('#createDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDevice(devicesDomainUUID, devicesCreateDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('<name>', data.response.name);
                assert.equal('<host name>', data.response.hostName);
                assert.equal('cisco123', data.response.natID);
                assert.equal('regkey', data.response.regKey);
                assert.equal('Device', data.response.type);
                assert.equal(true, Array.isArray(data.response.license_caps));
                assert.equal('object', typeof data.response.accessPolicy);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'createDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesContainerUUID = 'fakedata';
    const devicesCreateFTDBridgeGroupInterfaceBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: false
        },
        timestamp: 9
      },
      description: 'string',
      type: 'string',
      enabled: false,
      MTU: 10,
      mode: 'TAP',
      arpConfig: [
        {
          macAddress: 'string',
          enableAlias: true,
          ipAddress: 'string'
        }
      ],
      selectedInterfaces: [
        {
          metadata: {
            lastUser: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            domain: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            readOnly: {
              reason: {},
              state: false
            },
            timestamp: 8
          },
          name: 'string',
          description: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string',
          version: 'string'
        }
      ],
      ipv4: {
        static: {
          address: 'string',
          netmask: 'string'
        },
        pppoe: {
          pppoePassword: 'string',
          pppoeRouteMetric: 5,
          ipAddress: 'string',
          vpdnGroupName: 'string',
          pppAuth: 'CHAP',
          enableRouteSettings: false,
          storeCredsInFlash: true,
          pppoeUser: 'string'
        },
        dhcp: {
          dhcpRouteMetric: 8,
          enableDefaultRouteDHCP: true
        }
      },
      ifname: 'string',
      ipv6: {
        linkLocalAddress: 'string',
        addresses: [
          {
            address: 'string',
            enforceEUI64: false,
            prefix: 'string'
          }
        ],
        raInterval: 8,
        enableAutoConfig: false,
        nsInterval: 10,
        prefixes: [
          {
            default: true,
            address: 'string',
            advertisement: {
              autoConfig: false,
              offlink: false,
              preferLifeTime: {}
            }
          }
        ],
        enforceEUI64: true,
        enableRA: true,
        dadAttempts: 1,
        enableDHCPAddrConfig: false,
        enableDHCPNonAddrConfig: false,
        raLifeTime: 8,
        enableIPV6: false,
        reachableTime: 8
      },
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      managementOnly: false,
      macLearn: false,
      enableAntiSpoofing: true,
      securityZone: {
        interfaceMode: 'ROUTED',
        interfaces: [
          {
            metadata: {
              lastUser: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              domain: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              readOnly: {
                reason: {},
                state: true
              },
              timestamp: 3
            },
            name: 'string',
            description: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string',
            version: 'string'
          }
        ],
        metadata: {
          lastUser: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: {},
            state: false
          },
          timestamp: 10
        },
        name: 'string',
        overridable: true,
        description: 'string',
        links: {
          parent: 'string',
          self: 'string'
        },
        overrides: {
          parent: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          target: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          }
        },
        id: 'string',
        type: 'string',
        version: 'string'
      },
      activeMACAddress: 'string',
      fragmentReassembly: false,
      macTable: [
        'string'
      ],
      bridgeGroupId: 3,
      standbyMACAddress: 'string',
      ipAddress: 'string',
      enableDNSLookup: true,
      overrideDefaultFragmentSetting: {
        chain: 3,
        size: 2,
        timeout: 7
      },
      version: 'string',
      name: 'string'
    };
    describe('#createFTDBridgeGroupInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createFTDBridgeGroupInterface(devicesContainerUUID, devicesDomainUUID, devicesCreateFTDBridgeGroupInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.type);
                assert.equal(true, data.response.enabled);
                assert.equal(7, data.response.MTU);
                assert.equal('TAP', data.response.mode);
                assert.equal(true, Array.isArray(data.response.arpConfig));
                assert.equal(true, Array.isArray(data.response.selectedInterfaces));
                assert.equal('object', typeof data.response.ipv4);
                assert.equal('string', data.response.ifname);
                assert.equal('object', typeof data.response.ipv6);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal(true, data.response.managementOnly);
                assert.equal(false, data.response.macLearn);
                assert.equal(false, data.response.enableAntiSpoofing);
                assert.equal('object', typeof data.response.securityZone);
                assert.equal('string', data.response.activeMACAddress);
                assert.equal(false, data.response.fragmentReassembly);
                assert.equal(true, Array.isArray(data.response.macTable));
                assert.equal(10, data.response.bridgeGroupId);
                assert.equal('string', data.response.standbyMACAddress);
                assert.equal('string', data.response.ipAddress);
                assert.equal(true, data.response.enableDNSLookup);
                assert.equal('object', typeof data.response.overrideDefaultFragmentSetting);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'createFTDBridgeGroupInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesCreateFTDEtherChannelInterfaceBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: false
        },
        timestamp: 9
      },
      minActivePhysicalInterface: 8,
      description: 'string',
      type: 'string',
      enabled: true,
      MTU: 4,
      mode: 'TAP',
      arpConfig: [
        {
          macAddress: 'string',
          enableAlias: false,
          ipAddress: 'string'
        }
      ],
      selectedInterfaces: [
        {
          metadata: {
            lastUser: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            domain: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            readOnly: {
              reason: {},
              state: false
            },
            timestamp: 1
          },
          name: 'string',
          description: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string',
          version: 'string'
        }
      ],
      etherChannelId: 2,
      ipv4: {
        static: {
          address: 'string',
          netmask: 'string'
        },
        pppoe: {
          pppoePassword: 'string',
          pppoeRouteMetric: 7,
          ipAddress: 'string',
          vpdnGroupName: 'string',
          pppAuth: 'CHAP',
          enableRouteSettings: false,
          storeCredsInFlash: false,
          pppoeUser: 'string'
        },
        dhcp: {
          dhcpRouteMetric: 6,
          enableDefaultRouteDHCP: false
        }
      },
      ifname: 'string',
      ipv6: {
        linkLocalAddress: 'string',
        addresses: [
          {
            address: 'string',
            enforceEUI64: false,
            prefix: 'string'
          }
        ],
        raInterval: 10,
        enableAutoConfig: false,
        nsInterval: 6,
        prefixes: [
          {
            default: false,
            address: 'string',
            advertisement: {
              autoConfig: true,
              offlink: true,
              preferLifeTime: {}
            }
          }
        ],
        enforceEUI64: true,
        enableRA: false,
        dadAttempts: 10,
        enableDHCPAddrConfig: true,
        enableDHCPNonAddrConfig: true,
        raLifeTime: 8,
        enableIPV6: false,
        reachableTime: 5
      },
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      lacpMode: 'ON',
      hardware: {
        duplex: 'AUTO',
        speed: 'LAKH'
      },
      macLearn: true,
      managementOnly: false,
      enableAntiSpoofing: false,
      securityZone: {
        interfaceMode: 'ROUTED',
        interfaces: [
          {
            metadata: {
              lastUser: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              domain: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              readOnly: {
                reason: {},
                state: true
              },
              timestamp: 5
            },
            name: 'string',
            description: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string',
            version: 'string'
          }
        ],
        metadata: {
          lastUser: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: {},
            state: true
          },
          timestamp: 8
        },
        name: 'string',
        overridable: false,
        description: 'string',
        links: {
          parent: 'string',
          self: 'string'
        },
        overrides: {
          parent: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          target: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          }
        },
        id: 'string',
        type: 'string',
        version: 'string'
      },
      activeMACAddress: 'string',
      fragmentReassembly: false,
      macTable: [
        'string'
      ],
      standbyMACAddress: 'string',
      maxActivePhysicalInterface: 9,
      enableDNSLookup: true,
      overrideDefaultFragmentSetting: {
        chain: 4,
        size: 9,
        timeout: 3
      },
      loadBalancing: 'VLAN_DST_IP_PORT',
      version: 'string',
      erspanFlowId: 1,
      erspanSourceIP: 'string',
      name: 'string'
    };
    describe('#createFTDEtherChannelInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createFTDEtherChannelInterface(devicesContainerUUID, devicesDomainUUID, devicesCreateFTDEtherChannelInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(1, data.response.minActivePhysicalInterface);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.type);
                assert.equal(true, data.response.enabled);
                assert.equal(5, data.response.MTU);
                assert.equal('NONE', data.response.mode);
                assert.equal(true, Array.isArray(data.response.arpConfig));
                assert.equal(true, Array.isArray(data.response.selectedInterfaces));
                assert.equal(6, data.response.etherChannelId);
                assert.equal('object', typeof data.response.ipv4);
                assert.equal('string', data.response.ifname);
                assert.equal('object', typeof data.response.ipv6);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal('PASSIVE', data.response.lacpMode);
                assert.equal('object', typeof data.response.hardware);
                assert.equal(false, data.response.macLearn);
                assert.equal(false, data.response.managementOnly);
                assert.equal(true, data.response.enableAntiSpoofing);
                assert.equal('object', typeof data.response.securityZone);
                assert.equal('string', data.response.activeMACAddress);
                assert.equal(false, data.response.fragmentReassembly);
                assert.equal(true, Array.isArray(data.response.macTable));
                assert.equal('string', data.response.standbyMACAddress);
                assert.equal(5, data.response.maxActivePhysicalInterface);
                assert.equal(true, data.response.enableDNSLookup);
                assert.equal('object', typeof data.response.overrideDefaultFragmentSetting);
                assert.equal('DST_IP', data.response.loadBalancing);
                assert.equal('string', data.response.version);
                assert.equal(1, data.response.erspanFlowId);
                assert.equal('string', data.response.erspanSourceIP);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'createFTDEtherChannelInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesCreateFPLogicalInterfaceBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: false
        },
        timestamp: 6
      },
      securityZone: {
        interfaceMode: 'ROUTED',
        interfaces: [
          {
            metadata: {
              lastUser: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              domain: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              readOnly: {
                reason: {},
                state: false
              },
              timestamp: 8
            },
            name: 'string',
            description: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string',
            version: 'string'
          }
        ],
        metadata: {
          lastUser: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: {},
            state: true
          },
          timestamp: 1
        },
        name: 'string',
        overridable: false,
        description: 'string',
        links: {
          parent: 'string',
          self: 'string'
        },
        overrides: {
          parent: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          target: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          }
        },
        id: 'string',
        type: 'string',
        version: 'string'
      },
      vlanTag: 6,
      virtualSwitch: {
        dropBPDU: false,
        metadata: {
          lastUser: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: {},
            state: false
          },
          timestamp: 4
        },
        sensorPolicy: {
          metadata: {
            lastUser: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            domain: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            readOnly: {
              reason: {},
              state: true
            },
            timestamp: 7
          },
          defaultAction: {
            logEnd: false,
            logBegin: false,
            metadata: {},
            snmpConfig: {},
            intrusionPolicy: {},
            sendEventsToFMC: false,
            description: 'string',
            type: 'string',
            variableSet: {},
            version: 'string',
            syslogConfig: {},
            name: 'string',
            action: {},
            links: {},
            id: 'string'
          },
          name: 'string',
          description: 'string',
          rules: [
            {
              metadata: {},
              snmpConfig: {},
              sourceNetworks: {},
              syslogSeverity: {},
              sourceZones: {},
              destinationZones: {},
              description: 'string',
              originalSourceNetworks: {},
              enableSyslog: true,
              type: 'string',
              safeSearch: {},
              enabled: false,
              syslogConfig: {},
              urls: {},
              endPointDeviceTypes: [
                {}
              ],
              destinationNetworks: {},
              youTube: {},
              action: {},
              networkAccessDeviceIPs: {},
              links: {},
              id: 'string',
              sourceSecurityGroupTags: {},
              logEnd: true,
              logBegin: true,
              sendEventsToFMC: false,
              destinationPorts: {},
              sourcePorts: {},
              ipsPolicy: {},
              variableSet: {},
              version: 'string',
              users: {},
              logFiles: false,
              newComments: [
                'string'
              ],
              commentHistoryList: [
                {}
              ],
              filePolicy: {},
              name: 'string',
              vlanTags: {},
              applications: {}
            }
          ],
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string',
          version: 'string'
        },
        description: 'string',
        type: 'string',
        strictTCPEnforcement: true,
        version: 'string',
        domainId: 9,
        staticMacEntries: [
          {
            interface: {
              metadata: {
                lastUser: {
                  name: 'string',
                  links: {},
                  id: 'string',
                  type: 'string'
                },
                domain: {
                  name: 'string',
                  links: {},
                  id: 'string',
                  type: 'string'
                },
                readOnly: {
                  reason: {},
                  state: true
                },
                timestamp: 1
              },
              name: 'string',
              description: 'string',
              links: {
                parent: 'string',
                self: 'string'
              },
              id: 'string',
              type: 'string',
              version: 'string'
            },
            mac: 'string'
          }
        ],
        name: 'string',
        switchedInterfaces: [
          {
            metadata: {
              lastUser: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              domain: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              readOnly: {
                reason: {},
                state: false
              },
              timestamp: 1
            },
            name: 'string',
            description: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string',
            version: 'string'
          }
        ],
        enableSpanningTreeProtocol: true,
        links: {
          parent: 'string',
          self: 'string'
        },
        id: 'string',
        hybridInterface: {
          metadata: {
            lastUser: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            domain: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            readOnly: {
              reason: {},
              state: false
            },
            timestamp: 1
          },
          name: 'string',
          description: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string',
          version: 'string'
        }
      },
      icmpEnabled: 9,
      physicalInterface: {
        metadata: {
          lastUser: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: {},
            state: true
          },
          timestamp: 5
        },
        description: 'string',
        type: 'string',
        version: 'string',
        enabled: 2,
        mtu: 6,
        mode: 'string',
        interfaceType: 'NONE',
        mdi: 'string',
        staticArpEntries: [
          {
            ip: 'string',
            mac: 'string'
          }
        ],
        name: 'string',
        ipAddresses: [
          'string'
        ],
        links: {
          parent: 'string',
          self: 'string'
        },
        id: 'string'
      },
      description: 'string',
      type: 'string',
      version: 'string',
      enabled: 5,
      mtu: 1,
      mode: 'string',
      interfaceType: 'SWITCHED',
      mdi: 'string',
      name: 'string',
      staticArpEntries: [
        {
          ip: 'string',
          mac: 'string'
        }
      ],
      links: {
        parent: 'string',
        self: 'string'
      },
      ipAddresses: [
        'string'
      ],
      id: 'string'
    };
    describe('#createFPLogicalInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createFPLogicalInterface(devicesContainerUUID, devicesDomainUUID, devicesCreateFPLogicalInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('object', typeof data.response.securityZone);
                assert.equal(4, data.response.vlanTag);
                assert.equal('object', typeof data.response.virtualSwitch);
                assert.equal(3, data.response.icmpEnabled);
                assert.equal('object', typeof data.response.physicalInterface);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal(6, data.response.enabled);
                assert.equal(9, data.response.mtu);
                assert.equal('string', data.response.mode);
                assert.equal('ROUTED', data.response.interfaceType);
                assert.equal('string', data.response.mdi);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.staticArpEntries));
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.ipAddresses));
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'createFPLogicalInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesCreateInlineSetBodyParam = {
      macFiltering: true,
      loadBalancingModeVlan: 'string',
      tapMode: false,
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: false
        },
        timestamp: 8
      },
      standBy: false,
      failOpenSnortBusy: false,
      description: 'string',
      type: 'string',
      version: 'string',
      strictTCPEnforcement: true,
      failopen: true,
      mtu: 8,
      bypass: false,
      failSafe: true,
      loadBalancingMode: 'string',
      failOpenSnortDown: true,
      inlinepairs: [
        {
          first: {
            metadata: {
              lastUser: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              domain: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              readOnly: {
                reason: {},
                state: false
              },
              timestamp: 5
            },
            name: 'string',
            description: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string',
            version: 'string'
          },
          second: {
            metadata: {
              lastUser: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              domain: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              readOnly: {
                reason: {},
                state: false
              },
              timestamp: 7
            },
            name: 'string',
            description: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string',
            version: 'string'
          }
        }
      ],
      name: 'string',
      propogateLinkState: false,
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string'
    };
    describe('#createInlineSet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createInlineSet(devicesContainerUUID, devicesDomainUUID, devicesCreateInlineSetBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.macFiltering);
                assert.equal('string', data.response.loadBalancingModeVlan);
                assert.equal(true, data.response.tapMode);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, data.response.standBy);
                assert.equal(true, data.response.failOpenSnortBusy);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal(false, data.response.strictTCPEnforcement);
                assert.equal(false, data.response.failopen);
                assert.equal(7, data.response.mtu);
                assert.equal(true, data.response.bypass);
                assert.equal(false, data.response.failSafe);
                assert.equal('string', data.response.loadBalancingMode);
                assert.equal(true, data.response.failOpenSnortDown);
                assert.equal(true, Array.isArray(data.response.inlinepairs));
                assert.equal('string', data.response.name);
                assert.equal(true, data.response.propogateLinkState);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'createInlineSet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesCreateInterfaceEventBodyParam = {
      hasPendingChanges: false,
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'SYSTEM',
          state: false
        },
        timestamp: 5
      },
      change: 'string',
      name: 'string',
      action: 'SYNC_WITH_DEVICE',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      state: 'DISASSOCIATED',
      type: 'string',
      version: 'string'
    };
    describe('#createInterfaceEvent - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createInterfaceEvent(devicesContainerUUID, devicesDomainUUID, devicesCreateInterfaceEventBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.hasPendingChanges);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('string', data.response.change);
                assert.equal('string', data.response.name);
                assert.equal('SYNC_WITH_DEVICE', data.response.action);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal('MODIFIED', data.response.state);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'createInterfaceEvent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesCreateFTDRedundantInterfaceBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: false
        },
        timestamp: 4
      },
      description: 'string',
      type: 'string',
      enabled: false,
      MTU: 8,
      mode: 'PASSIVE',
      arpConfig: [
        {
          macAddress: 'string',
          enableAlias: false,
          ipAddress: 'string'
        }
      ],
      ipv4: {
        static: {
          address: 'string',
          netmask: 'string'
        },
        pppoe: {
          pppoePassword: 'string',
          pppoeRouteMetric: 9,
          ipAddress: 'string',
          vpdnGroupName: 'string',
          pppAuth: 'CHAP',
          enableRouteSettings: true,
          storeCredsInFlash: false,
          pppoeUser: 'string'
        },
        dhcp: {
          dhcpRouteMetric: 4,
          enableDefaultRouteDHCP: false
        }
      },
      ifname: 'string',
      ipv6: {
        linkLocalAddress: 'string',
        addresses: [
          {
            address: 'string',
            enforceEUI64: false,
            prefix: 'string'
          }
        ],
        raInterval: 6,
        enableAutoConfig: true,
        nsInterval: 3,
        prefixes: [
          {
            default: false,
            address: 'string',
            advertisement: {
              autoConfig: false,
              offlink: false,
              preferLifeTime: {}
            }
          }
        ],
        enforceEUI64: true,
        enableRA: true,
        dadAttempts: 4,
        enableDHCPAddrConfig: false,
        enableDHCPNonAddrConfig: true,
        raLifeTime: 3,
        enableIPV6: false,
        reachableTime: 7
      },
      links: {
        parent: 'string',
        self: 'string'
      },
      primaryInterface: {
        metadata: {
          lastUser: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: {},
            state: true
          },
          timestamp: 1
        },
        name: 'string',
        description: 'string',
        links: {
          parent: 'string',
          self: 'string'
        },
        id: 'string',
        type: 'string',
        version: 'string'
      },
      id: 'string',
      macLearn: false,
      managementOnly: true,
      enableAntiSpoofing: false,
      securityZone: {
        interfaceMode: 'SWITCHED',
        interfaces: [
          {
            metadata: {
              lastUser: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              domain: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              readOnly: {
                reason: {},
                state: false
              },
              timestamp: 5
            },
            name: 'string',
            description: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string',
            version: 'string'
          }
        ],
        metadata: {
          lastUser: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: {},
            state: false
          },
          timestamp: 8
        },
        name: 'string',
        overridable: false,
        description: 'string',
        links: {
          parent: 'string',
          self: 'string'
        },
        overrides: {
          parent: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          target: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          }
        },
        id: 'string',
        type: 'string',
        version: 'string'
      },
      activeMACAddress: 'string',
      fragmentReassembly: true,
      macTable: [
        'string'
      ],
      standbyMACAddress: 'string',
      enableDNSLookup: false,
      overrideDefaultFragmentSetting: {
        chain: 7,
        size: 9,
        timeout: 2
      },
      version: 'string',
      name: 'string',
      redundantId: 3,
      secondaryInterface: {
        metadata: {
          lastUser: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: {},
            state: false
          },
          timestamp: 8
        },
        name: 'string',
        description: 'string',
        links: {
          parent: 'string',
          self: 'string'
        },
        id: 'string',
        type: 'string',
        version: 'string'
      }
    };
    describe('#createFTDRedundantInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createFTDRedundantInterface(devicesContainerUUID, devicesDomainUUID, devicesCreateFTDRedundantInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.type);
                assert.equal(false, data.response.enabled);
                assert.equal(3, data.response.MTU);
                assert.equal('ERSPAN', data.response.mode);
                assert.equal(true, Array.isArray(data.response.arpConfig));
                assert.equal('object', typeof data.response.ipv4);
                assert.equal('string', data.response.ifname);
                assert.equal('object', typeof data.response.ipv6);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.primaryInterface);
                assert.equal('string', data.response.id);
                assert.equal(true, data.response.macLearn);
                assert.equal(false, data.response.managementOnly);
                assert.equal(false, data.response.enableAntiSpoofing);
                assert.equal('object', typeof data.response.securityZone);
                assert.equal('string', data.response.activeMACAddress);
                assert.equal(true, data.response.fragmentReassembly);
                assert.equal(true, Array.isArray(data.response.macTable));
                assert.equal('string', data.response.standbyMACAddress);
                assert.equal(true, data.response.enableDNSLookup);
                assert.equal('object', typeof data.response.overrideDefaultFragmentSetting);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal(8, data.response.redundantId);
                assert.equal('object', typeof data.response.secondaryInterface);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'createFTDRedundantInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesCreateIPv4StaticRouteModelBodyParam = {
      selectedNetworks: [
        {}
      ],
      metricValue: 5,
      interfaceName: 'string',
      gateway: {}
    };
    describe('#createIPv4StaticRouteModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createIPv4StaticRouteModel(null, devicesContainerUUID, devicesDomainUUID, devicesCreateIPv4StaticRouteModelBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.routeTracking);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, Array.isArray(data.response.selectedNetworks));
                assert.equal(2, data.response.metricValue);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.links);
                assert.equal(false, data.response.isTunneled);
                assert.equal('string', data.response.interfaceName);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.gateway);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'createIPv4StaticRouteModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesCreateIPv6StaticRouteModelBodyParam = {
      selectedNetworks: [
        {}
      ],
      metricValue: 6,
      interfaceName: 'string',
      gateway: {}
    };
    describe('#createIPv6StaticRouteModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createIPv6StaticRouteModel(null, devicesContainerUUID, devicesDomainUUID, devicesCreateIPv6StaticRouteModelBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, Array.isArray(data.response.selectedNetworks));
                assert.equal(7, data.response.metricValue);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, data.response.isTunneled);
                assert.equal('string', data.response.interfaceName);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal('object', typeof data.response.gateway);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'createIPv6StaticRouteModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesCreateFTDSubInterfaceBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: true
        },
        timestamp: 6
      },
      description: 'string',
      type: 'string',
      enabled: false,
      MTU: 2,
      mode: 'ERSPAN',
      subIntfId: 6,
      arpConfig: [
        {
          macAddress: 'string',
          enableAlias: false,
          ipAddress: 'string'
        }
      ],
      ipv4: {
        static: {
          address: 'string',
          netmask: 'string'
        },
        pppoe: {
          pppoePassword: 'string',
          pppoeRouteMetric: 9,
          ipAddress: 'string',
          vpdnGroupName: 'string',
          pppAuth: 'PAP',
          enableRouteSettings: true,
          storeCredsInFlash: true,
          pppoeUser: 'string'
        },
        dhcp: {
          dhcpRouteMetric: 1,
          enableDefaultRouteDHCP: true
        }
      },
      ifname: 'string',
      ipv6: {
        linkLocalAddress: 'string',
        addresses: [
          {
            address: 'string',
            enforceEUI64: false,
            prefix: 'string'
          }
        ],
        raInterval: 10,
        enableAutoConfig: false,
        nsInterval: 9,
        prefixes: [
          {
            default: false,
            address: 'string',
            advertisement: {
              autoConfig: false,
              offlink: false,
              preferLifeTime: {}
            }
          }
        ],
        enforceEUI64: false,
        enableRA: true,
        dadAttempts: 3,
        enableDHCPAddrConfig: true,
        enableDHCPNonAddrConfig: false,
        raLifeTime: 6,
        enableIPV6: false,
        reachableTime: 5
      },
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      macLearn: false,
      managementOnly: false,
      enableAntiSpoofing: false,
      securityZone: {
        interfaceMode: 'ROUTED',
        interfaces: [
          {
            metadata: {
              lastUser: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              domain: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              readOnly: {
                reason: {},
                state: false
              },
              timestamp: 8
            },
            name: 'string',
            description: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string',
            version: 'string'
          }
        ],
        metadata: {
          lastUser: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: {},
            state: true
          },
          timestamp: 8
        },
        name: 'string',
        overridable: true,
        description: 'string',
        links: {
          parent: 'string',
          self: 'string'
        },
        overrides: {
          parent: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          target: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          }
        },
        id: 'string',
        type: 'string',
        version: 'string'
      },
      activeMACAddress: 'string',
      vlanId: 5,
      fragmentReassembly: true,
      macTable: [
        'string'
      ],
      standbyMACAddress: 'string',
      enableDNSLookup: true,
      overrideDefaultFragmentSetting: {
        chain: 4,
        size: 6,
        timeout: 6
      },
      version: 'string',
      name: 'string'
    };
    describe('#createFTDSubInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createFTDSubInterface(null, devicesContainerUUID, devicesDomainUUID, devicesCreateFTDSubInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.type);
                assert.equal(true, data.response.enabled);
                assert.equal(2, data.response.MTU);
                assert.equal('ERSPAN', data.response.mode);
                assert.equal(4, data.response.subIntfId);
                assert.equal(true, Array.isArray(data.response.arpConfig));
                assert.equal('object', typeof data.response.ipv4);
                assert.equal('string', data.response.ifname);
                assert.equal('object', typeof data.response.ipv6);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal(true, data.response.macLearn);
                assert.equal(true, data.response.managementOnly);
                assert.equal(false, data.response.enableAntiSpoofing);
                assert.equal('object', typeof data.response.securityZone);
                assert.equal('string', data.response.activeMACAddress);
                assert.equal(3, data.response.vlanId);
                assert.equal(false, data.response.fragmentReassembly);
                assert.equal(true, Array.isArray(data.response.macTable));
                assert.equal('string', data.response.standbyMACAddress);
                assert.equal(true, data.response.enableDNSLookup);
                assert.equal('object', typeof data.response.overrideDefaultFragmentSetting);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'createFTDSubInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesCreateVirtualSwitchBodyParam = {
      dropBPDU: false,
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: true
        },
        timestamp: 1
      },
      sensorPolicy: {
        metadata: {
          lastUser: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: {},
            state: true
          },
          timestamp: 8
        },
        defaultAction: {
          logEnd: false,
          logBegin: true,
          metadata: {},
          snmpConfig: {},
          intrusionPolicy: {},
          sendEventsToFMC: false,
          description: 'string',
          type: 'string',
          variableSet: {},
          version: 'string',
          syslogConfig: {},
          name: 'string',
          action: {},
          links: {},
          id: 'string'
        },
        name: 'string',
        description: 'string',
        rules: [
          {
            metadata: {},
            snmpConfig: {},
            sourceNetworks: {},
            syslogSeverity: {},
            sourceZones: {},
            destinationZones: {},
            description: 'string',
            originalSourceNetworks: {},
            enableSyslog: true,
            type: 'string',
            safeSearch: {},
            enabled: false,
            syslogConfig: {},
            urls: {},
            endPointDeviceTypes: [
              {}
            ],
            destinationNetworks: {},
            youTube: {},
            action: {},
            networkAccessDeviceIPs: {},
            links: {},
            id: 'string',
            sourceSecurityGroupTags: {},
            logEnd: true,
            logBegin: true,
            sendEventsToFMC: true,
            destinationPorts: {},
            sourcePorts: {},
            ipsPolicy: {},
            variableSet: {},
            version: 'string',
            users: {},
            logFiles: true,
            newComments: [
              'string'
            ],
            commentHistoryList: [
              {}
            ],
            filePolicy: {},
            name: 'string',
            vlanTags: {},
            applications: {}
          }
        ],
        links: {
          parent: 'string',
          self: 'string'
        },
        id: 'string',
        type: 'string',
        version: 'string'
      },
      description: 'string',
      type: 'string',
      version: 'string',
      strictTCPEnforcement: false,
      domainId: 7,
      staticMacEntries: [
        {
          interface: {
            metadata: {
              lastUser: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              domain: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              readOnly: {
                reason: {},
                state: false
              },
              timestamp: 2
            },
            name: 'string',
            description: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string',
            version: 'string'
          },
          mac: 'string'
        }
      ],
      name: 'string',
      switchedInterfaces: [
        {
          metadata: {
            lastUser: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            domain: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            readOnly: {
              reason: {},
              state: true
            },
            timestamp: 9
          },
          name: 'string',
          description: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string',
          version: 'string'
        }
      ],
      enableSpanningTreeProtocol: false,
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      hybridInterface: {
        metadata: {
          lastUser: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: {},
            state: false
          },
          timestamp: 8
        },
        name: 'string',
        description: 'string',
        links: {
          parent: 'string',
          self: 'string'
        },
        id: 'string',
        type: 'string',
        version: 'string'
      }
    };
    describe('#createVirtualSwitch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createVirtualSwitch(devicesContainerUUID, devicesDomainUUID, devicesCreateVirtualSwitchBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.dropBPDU);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('object', typeof data.response.sensorPolicy);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal(false, data.response.strictTCPEnforcement);
                assert.equal(4, data.response.domainId);
                assert.equal(true, Array.isArray(data.response.staticMacEntries));
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.switchedInterfaces));
                assert.equal(true, data.response.enableSpanningTreeProtocol);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.hybridInterface);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'createVirtualSwitch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllDevice(devicesDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getAllDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFTDBridgeGroupInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllFTDBridgeGroupInterface(devicesContainerUUID, devicesDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getAllFTDBridgeGroupInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesObjectId = 'fakedata';
    const devicesUpdateFTDBridgeGroupInterfaceBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: false
        },
        timestamp: 4
      },
      description: 'string',
      type: 'string',
      enabled: false,
      MTU: 7,
      mode: 'ERSPAN',
      arpConfig: [
        {
          macAddress: 'string',
          enableAlias: false,
          ipAddress: 'string'
        }
      ],
      selectedInterfaces: [
        {
          metadata: {
            lastUser: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            domain: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            readOnly: {
              reason: {},
              state: false
            },
            timestamp: 8
          },
          name: 'string',
          description: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string',
          version: 'string'
        }
      ],
      ipv4: {
        static: {
          address: 'string',
          netmask: 'string'
        },
        pppoe: {
          pppoePassword: 'string',
          pppoeRouteMetric: 10,
          ipAddress: 'string',
          vpdnGroupName: 'string',
          pppAuth: 'PAP',
          enableRouteSettings: true,
          storeCredsInFlash: true,
          pppoeUser: 'string'
        },
        dhcp: {
          dhcpRouteMetric: 9,
          enableDefaultRouteDHCP: false
        }
      },
      ifname: 'string',
      ipv6: {
        linkLocalAddress: 'string',
        addresses: [
          {
            address: 'string',
            enforceEUI64: false,
            prefix: 'string'
          }
        ],
        raInterval: 2,
        enableAutoConfig: true,
        nsInterval: 7,
        prefixes: [
          {
            default: true,
            address: 'string',
            advertisement: {
              autoConfig: false,
              offlink: false,
              preferLifeTime: {}
            }
          }
        ],
        enforceEUI64: true,
        enableRA: true,
        dadAttempts: 10,
        enableDHCPAddrConfig: true,
        enableDHCPNonAddrConfig: true,
        raLifeTime: 9,
        enableIPV6: true,
        reachableTime: 4
      },
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      managementOnly: false,
      macLearn: false,
      enableAntiSpoofing: false,
      securityZone: {
        interfaceMode: 'PASSIVE',
        interfaces: [
          {
            metadata: {
              lastUser: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              domain: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              readOnly: {
                reason: {},
                state: true
              },
              timestamp: 6
            },
            name: 'string',
            description: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string',
            version: 'string'
          }
        ],
        metadata: {
          lastUser: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: {},
            state: true
          },
          timestamp: 10
        },
        name: 'string',
        overridable: true,
        description: 'string',
        links: {
          parent: 'string',
          self: 'string'
        },
        overrides: {
          parent: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          target: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          }
        },
        id: 'string',
        type: 'string',
        version: 'string'
      },
      activeMACAddress: 'string',
      fragmentReassembly: false,
      macTable: [
        'string'
      ],
      bridgeGroupId: 3,
      standbyMACAddress: 'string',
      ipAddress: 'string',
      enableDNSLookup: false,
      overrideDefaultFragmentSetting: {
        chain: 10,
        size: 7,
        timeout: 3
      },
      version: 'string',
      name: 'string'
    };
    describe('#updateFTDBridgeGroupInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFTDBridgeGroupInterface(devicesObjectId, devicesContainerUUID, devicesDomainUUID, devicesUpdateFTDBridgeGroupInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'updateFTDBridgeGroupInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFTDBridgeGroupInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFTDBridgeGroupInterface(devicesObjectId, devicesContainerUUID, devicesDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('BridgeGroupInterface', data.response.type);
                assert.equal(true, Array.isArray(data.response.selectedInterfaces));
                assert.equal(true, data.response.enabled);
                assert.equal(1500, data.response.MTU);
                assert.equal('bridgeGroupIntf1', data.response.ifname);
                assert.equal(false, data.response.managementOnly);
                assert.equal('1.2.3.4', data.response.ipAddress);
                assert.equal(39, data.response.bridgeGroupId);
                assert.equal('object', typeof data.response.ipv4);
                assert.equal('bridgegroupinterfaceUUID', data.response.id);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.metadata);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getFTDBridgeGroupInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFTDEtherChannelInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllFTDEtherChannelInterface(devicesContainerUUID, devicesDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getAllFTDEtherChannelInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesUpdateFTDEtherChannelInterfaceBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: false
        },
        timestamp: 6
      },
      minActivePhysicalInterface: 4,
      description: 'string',
      type: 'string',
      enabled: true,
      MTU: 5,
      mode: 'INLINE',
      arpConfig: [
        {
          macAddress: 'string',
          enableAlias: true,
          ipAddress: 'string'
        }
      ],
      selectedInterfaces: [
        {
          metadata: {
            lastUser: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            domain: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            readOnly: {
              reason: {},
              state: false
            },
            timestamp: 3
          },
          name: 'string',
          description: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string',
          version: 'string'
        }
      ],
      etherChannelId: 4,
      ipv4: {
        static: {
          address: 'string',
          netmask: 'string'
        },
        pppoe: {
          pppoePassword: 'string',
          pppoeRouteMetric: 3,
          ipAddress: 'string',
          vpdnGroupName: 'string',
          pppAuth: 'PAP',
          enableRouteSettings: true,
          storeCredsInFlash: false,
          pppoeUser: 'string'
        },
        dhcp: {
          dhcpRouteMetric: 6,
          enableDefaultRouteDHCP: false
        }
      },
      ifname: 'string',
      ipv6: {
        linkLocalAddress: 'string',
        addresses: [
          {
            address: 'string',
            enforceEUI64: true,
            prefix: 'string'
          }
        ],
        raInterval: 1,
        enableAutoConfig: true,
        nsInterval: 9,
        prefixes: [
          {
            default: true,
            address: 'string',
            advertisement: {
              autoConfig: true,
              offlink: false,
              preferLifeTime: {}
            }
          }
        ],
        enforceEUI64: true,
        enableRA: true,
        dadAttempts: 8,
        enableDHCPAddrConfig: true,
        enableDHCPNonAddrConfig: true,
        raLifeTime: 5,
        enableIPV6: false,
        reachableTime: 4
      },
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      lacpMode: 'PASSIVE',
      hardware: {
        duplex: 'FULL',
        speed: 'THOUSAND'
      },
      macLearn: true,
      managementOnly: false,
      enableAntiSpoofing: true,
      securityZone: {
        interfaceMode: 'ASA',
        interfaces: [
          {
            metadata: {
              lastUser: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              domain: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              readOnly: {
                reason: {},
                state: true
              },
              timestamp: 3
            },
            name: 'string',
            description: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string',
            version: 'string'
          }
        ],
        metadata: {
          lastUser: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: {},
            state: true
          },
          timestamp: 10
        },
        name: 'string',
        overridable: false,
        description: 'string',
        links: {
          parent: 'string',
          self: 'string'
        },
        overrides: {
          parent: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          target: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          }
        },
        id: 'string',
        type: 'string',
        version: 'string'
      },
      activeMACAddress: 'string',
      fragmentReassembly: true,
      macTable: [
        'string'
      ],
      standbyMACAddress: 'string',
      maxActivePhysicalInterface: 3,
      enableDNSLookup: true,
      overrideDefaultFragmentSetting: {
        chain: 3,
        size: 1,
        timeout: 2
      },
      loadBalancing: 'VLAN_ONLY',
      version: 'string',
      erspanFlowId: 5,
      erspanSourceIP: 'string',
      name: 'string'
    };
    describe('#updateFTDEtherChannelInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFTDEtherChannelInterface(devicesObjectId, devicesContainerUUID, devicesDomainUUID, devicesUpdateFTDEtherChannelInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'updateFTDEtherChannelInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFTDEtherChannelInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFTDEtherChannelInterface(devicesObjectId, devicesContainerUUID, devicesDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('object', typeof data.response.links);
                assert.equal('EtherChannelInterface', data.response.type);
                assert.equal('NONE', data.response.mode);
                assert.equal('ACTIVE', data.response.lacpMode);
                assert.equal(8, data.response.maxActivePhysicalInterface);
                assert.equal(1, data.response.minActivePhysicalInterface);
                assert.equal(true, Array.isArray(data.response.selectedInterfaces));
                assert.equal('object', typeof data.response.hardware);
                assert.equal('SRC_IP_PORT', data.response.loadBalancing);
                assert.equal(1, data.response.etherChannelId);
                assert.equal(true, data.response.enabled);
                assert.equal(1500, data.response.MTU);
                assert.equal(false, data.response.managementOnly);
                assert.equal('object', typeof data.response.securityZone);
                assert.equal('NewEthChannel', data.response.ifname);
                assert.equal(false, data.response.enableAntiSpoofing);
                assert.equal('object', typeof data.response.overrideDefaultFragmentSetting);
                assert.equal('object', typeof data.response.ipv4);
                assert.equal('object', typeof data.response.ipv6);
                assert.equal('Port-channel1', data.response.name);
                assert.equal('etherChannelIntfUUID1', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getFTDEtherChannelInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFPInterfaceStatistics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFPInterfaceStatistics(devicesContainerUUID, devicesDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getFPInterfaceStatistics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFPLogicalInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllFPLogicalInterface(devicesContainerUUID, devicesDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getAllFPLogicalInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesUpdateFPLogicalInterfaceBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: true
        },
        timestamp: 6
      },
      securityZone: {
        interfaceMode: 'ASA',
        interfaces: [
          {
            metadata: {
              lastUser: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              domain: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              readOnly: {
                reason: {},
                state: true
              },
              timestamp: 6
            },
            name: 'string',
            description: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string',
            version: 'string'
          }
        ],
        metadata: {
          lastUser: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: {},
            state: false
          },
          timestamp: 7
        },
        name: 'string',
        overridable: false,
        description: 'string',
        links: {
          parent: 'string',
          self: 'string'
        },
        overrides: {
          parent: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          target: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          }
        },
        id: 'string',
        type: 'string',
        version: 'string'
      },
      vlanTag: 3,
      virtualSwitch: {
        dropBPDU: true,
        metadata: {
          lastUser: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: {},
            state: true
          },
          timestamp: 8
        },
        sensorPolicy: {
          metadata: {
            lastUser: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            domain: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            readOnly: {
              reason: {},
              state: true
            },
            timestamp: 4
          },
          defaultAction: {
            logEnd: false,
            logBegin: true,
            metadata: {},
            snmpConfig: {},
            intrusionPolicy: {},
            sendEventsToFMC: true,
            description: 'string',
            type: 'string',
            variableSet: {},
            version: 'string',
            syslogConfig: {},
            name: 'string',
            action: {},
            links: {},
            id: 'string'
          },
          name: 'string',
          description: 'string',
          rules: [
            {
              metadata: {},
              snmpConfig: {},
              sourceNetworks: {},
              syslogSeverity: {},
              sourceZones: {},
              destinationZones: {},
              description: 'string',
              originalSourceNetworks: {},
              enableSyslog: true,
              type: 'string',
              safeSearch: {},
              enabled: false,
              syslogConfig: {},
              urls: {},
              endPointDeviceTypes: [
                {}
              ],
              destinationNetworks: {},
              youTube: {},
              action: {},
              networkAccessDeviceIPs: {},
              links: {},
              id: 'string',
              sourceSecurityGroupTags: {},
              logEnd: false,
              logBegin: false,
              sendEventsToFMC: true,
              destinationPorts: {},
              sourcePorts: {},
              ipsPolicy: {},
              variableSet: {},
              version: 'string',
              users: {},
              logFiles: false,
              newComments: [
                'string'
              ],
              commentHistoryList: [
                {}
              ],
              filePolicy: {},
              name: 'string',
              vlanTags: {},
              applications: {}
            }
          ],
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string',
          version: 'string'
        },
        description: 'string',
        type: 'string',
        strictTCPEnforcement: false,
        version: 'string',
        domainId: 7,
        staticMacEntries: [
          {
            interface: {
              metadata: {
                lastUser: {
                  name: 'string',
                  links: {},
                  id: 'string',
                  type: 'string'
                },
                domain: {
                  name: 'string',
                  links: {},
                  id: 'string',
                  type: 'string'
                },
                readOnly: {
                  reason: {},
                  state: true
                },
                timestamp: 5
              },
              name: 'string',
              description: 'string',
              links: {
                parent: 'string',
                self: 'string'
              },
              id: 'string',
              type: 'string',
              version: 'string'
            },
            mac: 'string'
          }
        ],
        name: 'string',
        switchedInterfaces: [
          {
            metadata: {
              lastUser: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              domain: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              readOnly: {
                reason: {},
                state: false
              },
              timestamp: 7
            },
            name: 'string',
            description: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string',
            version: 'string'
          }
        ],
        enableSpanningTreeProtocol: false,
        links: {
          parent: 'string',
          self: 'string'
        },
        id: 'string',
        hybridInterface: {
          metadata: {
            lastUser: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            domain: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            readOnly: {
              reason: {},
              state: true
            },
            timestamp: 7
          },
          name: 'string',
          description: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string',
          version: 'string'
        }
      },
      icmpEnabled: 3,
      physicalInterface: {
        metadata: {
          lastUser: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: {},
            state: true
          },
          timestamp: 3
        },
        description: 'string',
        type: 'string',
        version: 'string',
        enabled: 4,
        mtu: 4,
        mode: 'string',
        interfaceType: 'PASSIVE',
        mdi: 'string',
        staticArpEntries: [
          {
            ip: 'string',
            mac: 'string'
          }
        ],
        name: 'string',
        ipAddresses: [
          'string'
        ],
        links: {
          parent: 'string',
          self: 'string'
        },
        id: 'string'
      },
      description: 'string',
      type: 'string',
      version: 'string',
      enabled: 5,
      mtu: 2,
      mode: 'string',
      interfaceType: 'SWITCHED',
      mdi: 'string',
      name: 'string',
      staticArpEntries: [
        {
          ip: 'string',
          mac: 'string'
        }
      ],
      links: {
        parent: 'string',
        self: 'string'
      },
      ipAddresses: [
        'string'
      ],
      id: 'string'
    };
    describe('#updateFPLogicalInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFPLogicalInterface(devicesObjectId, devicesContainerUUID, devicesDomainUUID, devicesUpdateFPLogicalInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'updateFPLogicalInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFPLogicalInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFPLogicalInterface(devicesObjectId, devicesContainerUUID, devicesDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('s1p3.1', data.response.name);
                assert.equal('fplogicalinterfaceUUID', data.response.id);
                assert.equal('FPLogicalInterface', data.response.type);
                assert.equal('object', typeof data.response.links);
                assert.equal(1, data.response.enabled);
                assert.equal('auto', data.response.mdi);
                assert.equal('autoneg', data.response.mode);
                assert.equal('SWITCHED', data.response.interfaceType);
                assert.equal('object', typeof data.response.physicalInterface);
                assert.equal(1, data.response.vlanTag);
                assert.equal('object', typeof data.response.metadata);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getFPLogicalInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFPPhysicalInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllFPPhysicalInterface(devicesContainerUUID, devicesDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getAllFPPhysicalInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesUpdateFPPhysicalInterfaceBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: true
        },
        timestamp: 5
      },
      description: 'string',
      type: 'string',
      version: 'string',
      enabled: 1,
      mtu: 8,
      mode: 'string',
      interfaceType: 'ROUTED',
      mdi: 'string',
      name: 'string',
      staticArpEntries: [
        {
          ip: 'string',
          mac: 'string'
        }
      ],
      links: {
        parent: 'string',
        self: 'string'
      },
      ipAddresses: [
        'string'
      ],
      id: 'string'
    };
    describe('#updateFPPhysicalInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFPPhysicalInterface(devicesObjectId, devicesContainerUUID, devicesDomainUUID, devicesUpdateFPPhysicalInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'updateFPPhysicalInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFPPhysicalInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFPPhysicalInterface(devicesObjectId, devicesContainerUUID, devicesDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('s1p1', data.response.name);
                assert.equal('fpphysicalinterfaceUUID1', data.response.id);
                assert.equal('FPPhysicalInterface', data.response.type);
                assert.equal('object', typeof data.response.links);
                assert.equal(1, data.response.enabled);
                assert.equal('auto', data.response.mdi);
                assert.equal('autoneg', data.response.mode);
                assert.equal('INLINE', data.response.interfaceType);
                assert.equal('object', typeof data.response.metadata);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getFPPhysicalInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllInlineSet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllInlineSet(devicesContainerUUID, devicesDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getAllInlineSet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesUpdateInlineSetBodyParam = {
      macFiltering: false,
      loadBalancingModeVlan: 'string',
      tapMode: true,
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: true
        },
        timestamp: 1
      },
      standBy: false,
      failOpenSnortBusy: true,
      description: 'string',
      type: 'string',
      version: 'string',
      strictTCPEnforcement: true,
      failopen: false,
      mtu: 5,
      bypass: false,
      failSafe: true,
      loadBalancingMode: 'string',
      failOpenSnortDown: true,
      inlinepairs: [
        {
          first: {
            metadata: {
              lastUser: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              domain: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              readOnly: {
                reason: {},
                state: false
              },
              timestamp: 9
            },
            name: 'string',
            description: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string',
            version: 'string'
          },
          second: {
            metadata: {
              lastUser: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              domain: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              readOnly: {
                reason: {},
                state: true
              },
              timestamp: 3
            },
            name: 'string',
            description: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string',
            version: 'string'
          }
        }
      ],
      name: 'string',
      propogateLinkState: true,
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string'
    };
    describe('#updateInlineSet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateInlineSet(devicesObjectId, devicesContainerUUID, devicesDomainUUID, devicesUpdateInlineSetBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'updateInlineSet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInlineSet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInlineSet(devicesObjectId, devicesContainerUUID, devicesDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Default Inline Set', data.response.name);
                assert.equal('inlinesetUUID', data.response.id);
                assert.equal('InlineSet', data.response.type);
                assert.equal('object', typeof data.response.links);
                assert.equal(false, data.response.failSafe);
                assert.equal(true, data.response.failopen);
                assert.equal(false, data.response.macFiltering);
                assert.equal(true, Array.isArray(data.response.inlinepairs));
                assert.equal('inner', data.response.loadBalancingMode);
                assert.equal('inner', data.response.loadBalancingModeVlan);
                assert.equal(1518, data.response.mtu);
                assert.equal(false, data.response.propogateLinkState);
                assert.equal(false, data.response.strictTCPEnforcement);
                assert.equal('object', typeof data.response.metadata);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getInlineSet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterfaceEvent - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInterfaceEvent(devicesContainerUUID, devicesDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getInterfaceEvent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFTDPhysicalInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllFTDPhysicalInterface(devicesContainerUUID, devicesDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getAllFTDPhysicalInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesUpdateFTDPhysicalInterfaceBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: false
        },
        timestamp: 4
      },
      description: 'string',
      type: 'string',
      enabled: false,
      MTU: 9,
      mode: 'TAP',
      arpConfig: [
        {
          macAddress: 'string',
          enableAlias: false,
          ipAddress: 'string'
        }
      ],
      ipv4: {
        static: {
          address: 'string',
          netmask: 'string'
        },
        pppoe: {
          pppoePassword: 'string',
          pppoeRouteMetric: 8,
          ipAddress: 'string',
          vpdnGroupName: 'string',
          pppAuth: 'MSCHAP',
          enableRouteSettings: true,
          storeCredsInFlash: true,
          pppoeUser: 'string'
        },
        dhcp: {
          dhcpRouteMetric: 9,
          enableDefaultRouteDHCP: false
        }
      },
      ifname: 'string',
      ipv6: {
        linkLocalAddress: 'string',
        addresses: [
          {
            address: 'string',
            enforceEUI64: false,
            prefix: 'string'
          }
        ],
        raInterval: 3,
        enableAutoConfig: true,
        nsInterval: 5,
        prefixes: [
          {
            default: false,
            address: 'string',
            advertisement: {
              autoConfig: false,
              offlink: true,
              preferLifeTime: {}
            }
          }
        ],
        enforceEUI64: false,
        enableRA: true,
        dadAttempts: 3,
        enableDHCPAddrConfig: true,
        enableDHCPNonAddrConfig: false,
        raLifeTime: 5,
        enableIPV6: true,
        reachableTime: 8
      },
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      lacpMode: 'ON',
      hardware: {
        duplex: 'HALF',
        speed: 'AUTO'
      },
      macLearn: false,
      managementOnly: false,
      channelGroupId: 9,
      enableAntiSpoofing: true,
      securityZone: {
        interfaceMode: 'PASSIVE',
        interfaces: [
          {
            metadata: {
              lastUser: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              domain: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              readOnly: {
                reason: {},
                state: false
              },
              timestamp: 2
            },
            name: 'string',
            description: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string',
            version: 'string'
          }
        ],
        metadata: {
          lastUser: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: {},
            state: true
          },
          timestamp: 7
        },
        name: 'string',
        overridable: true,
        description: 'string',
        links: {
          parent: 'string',
          self: 'string'
        },
        overrides: {
          parent: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          target: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          }
        },
        id: 'string',
        type: 'string',
        version: 'string'
      },
      isRedundantMember: true,
      activeMACAddress: 'string',
      fragmentReassembly: false,
      macTable: [
        'string'
      ],
      standbyMACAddress: 'string',
      enableDNSLookup: false,
      overrideDefaultFragmentSetting: {
        chain: 3,
        size: 9,
        timeout: 2
      },
      version: 'string',
      erspanFlowId: 10,
      erspanSourceIP: 'string',
      name: 'string'
    };
    describe('#updateFTDPhysicalInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFTDPhysicalInterface(devicesObjectId, devicesContainerUUID, devicesDomainUUID, devicesUpdateFTDPhysicalInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'updateFTDPhysicalInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFTDPhysicalInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFTDPhysicalInterface(devicesObjectId, devicesContainerUUID, devicesDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('object', typeof data.response.links);
                assert.equal('PhysicalInterface', data.response.type);
                assert.equal('INLINE', data.response.mode);
                assert.equal('object', typeof data.response.hardware);
                assert.equal(false, data.response.enabled);
                assert.equal(1500, data.response.MTU);
                assert.equal(false, data.response.managementOnly);
                assert.equal('object', typeof data.response.securityZone);
                assert.equal('Intf1', data.response.ifname);
                assert.equal(false, data.response.enableAntiSpoofing);
                assert.equal('object', typeof data.response.overrideDefaultFragmentSetting);
                assert.equal('object', typeof data.response.ipv4);
                assert.equal('object', typeof data.response.ipv6);
                assert.equal('GigabitEthernet1/1', data.response.name);
                assert.equal('PhyIntfId1', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getFTDPhysicalInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFTDRedundantInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllFTDRedundantInterface(devicesContainerUUID, devicesDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getAllFTDRedundantInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesUpdateFTDRedundantInterfaceBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: false
        },
        timestamp: 9
      },
      description: 'string',
      type: 'string',
      enabled: true,
      MTU: 7,
      mode: 'TAP',
      arpConfig: [
        {
          macAddress: 'string',
          enableAlias: false,
          ipAddress: 'string'
        }
      ],
      ipv4: {
        static: {
          address: 'string',
          netmask: 'string'
        },
        pppoe: {
          pppoePassword: 'string',
          pppoeRouteMetric: 5,
          ipAddress: 'string',
          vpdnGroupName: 'string',
          pppAuth: 'CHAP',
          enableRouteSettings: true,
          storeCredsInFlash: true,
          pppoeUser: 'string'
        },
        dhcp: {
          dhcpRouteMetric: 2,
          enableDefaultRouteDHCP: false
        }
      },
      ifname: 'string',
      ipv6: {
        linkLocalAddress: 'string',
        addresses: [
          {
            address: 'string',
            enforceEUI64: true,
            prefix: 'string'
          }
        ],
        raInterval: 10,
        enableAutoConfig: true,
        nsInterval: 9,
        prefixes: [
          {
            default: false,
            address: 'string',
            advertisement: {
              autoConfig: true,
              offlink: false,
              preferLifeTime: {}
            }
          }
        ],
        enforceEUI64: false,
        enableRA: false,
        dadAttempts: 3,
        enableDHCPAddrConfig: false,
        enableDHCPNonAddrConfig: false,
        raLifeTime: 9,
        enableIPV6: false,
        reachableTime: 2
      },
      links: {
        parent: 'string',
        self: 'string'
      },
      primaryInterface: {
        metadata: {
          lastUser: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: {},
            state: false
          },
          timestamp: 7
        },
        name: 'string',
        description: 'string',
        links: {
          parent: 'string',
          self: 'string'
        },
        id: 'string',
        type: 'string',
        version: 'string'
      },
      id: 'string',
      macLearn: true,
      managementOnly: false,
      enableAntiSpoofing: true,
      securityZone: {
        interfaceMode: 'PASSIVE',
        interfaces: [
          {
            metadata: {
              lastUser: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              domain: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              readOnly: {
                reason: {},
                state: false
              },
              timestamp: 9
            },
            name: 'string',
            description: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string',
            version: 'string'
          }
        ],
        metadata: {
          lastUser: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: {},
            state: true
          },
          timestamp: 9
        },
        name: 'string',
        overridable: true,
        description: 'string',
        links: {
          parent: 'string',
          self: 'string'
        },
        overrides: {
          parent: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          target: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          }
        },
        id: 'string',
        type: 'string',
        version: 'string'
      },
      activeMACAddress: 'string',
      fragmentReassembly: true,
      macTable: [
        'string'
      ],
      standbyMACAddress: 'string',
      enableDNSLookup: false,
      overrideDefaultFragmentSetting: {
        chain: 7,
        size: 9,
        timeout: 7
      },
      version: 'string',
      name: 'string',
      redundantId: 7,
      secondaryInterface: {
        metadata: {
          lastUser: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: {},
            state: true
          },
          timestamp: 9
        },
        name: 'string',
        description: 'string',
        links: {
          parent: 'string',
          self: 'string'
        },
        id: 'string',
        type: 'string',
        version: 'string'
      }
    };
    describe('#updateFTDRedundantInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFTDRedundantInterface(devicesObjectId, devicesContainerUUID, devicesDomainUUID, devicesUpdateFTDRedundantInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'updateFTDRedundantInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFTDRedundantInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFTDRedundantInterface(devicesObjectId, devicesContainerUUID, devicesDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Redundant4', data.response.name);
                assert.equal('RedundantInterface', data.response.type);
                assert.equal('object', typeof data.response.primaryInterface);
                assert.equal(4, data.response.redundantId);
                assert.equal('object', typeof data.response.secondaryInterface);
                assert.equal(true, data.response.enabled);
                assert.equal(1500, data.response.MTU);
                assert.equal('redInter', data.response.ifname);
                assert.equal(false, data.response.managementOnly);
                assert.equal('object', typeof data.response.ipv4);
                assert.equal('object', typeof data.response.ipv6);
                assert.equal(false, data.response.enableAntiSpoofing);
                assert.equal('object', typeof data.response.overrideDefaultFragmentSetting);
                assert.equal('00000000-0000-0ed3-0000-206158430258', data.response.id);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.metadata);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getFTDRedundantInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllIPv4StaticRouteModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllIPv4StaticRouteModel(devicesContainerUUID, devicesDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getAllIPv4StaticRouteModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesUpdateIPv4StaticRouteModelBodyParam = {
      selectedNetworks: [
        {}
      ],
      metricValue: 5,
      interfaceName: 'string',
      gateway: {}
    };
    describe('#updateIPv4StaticRouteModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateIPv4StaticRouteModel(devicesObjectId, devicesContainerUUID, devicesDomainUUID, devicesUpdateIPv4StaticRouteModelBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'updateIPv4StaticRouteModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPv4StaticRouteModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIPv4StaticRouteModel(devicesObjectId, devicesContainerUUID, devicesDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.routeTracking);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, Array.isArray(data.response.selectedNetworks));
                assert.equal(1, data.response.metricValue);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.links);
                assert.equal(false, data.response.isTunneled);
                assert.equal('string', data.response.interfaceName);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.gateway);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getIPv4StaticRouteModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllIPv6StaticRouteModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllIPv6StaticRouteModel(devicesContainerUUID, devicesDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getAllIPv6StaticRouteModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesUpdateIPv6StaticRouteModelBodyParam = {
      selectedNetworks: [
        {}
      ],
      metricValue: 7,
      interfaceName: 'string',
      gateway: {}
    };
    describe('#updateIPv6StaticRouteModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateIPv6StaticRouteModel(devicesObjectId, devicesContainerUUID, devicesDomainUUID, devicesUpdateIPv6StaticRouteModelBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'updateIPv6StaticRouteModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPv6StaticRouteModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIPv6StaticRouteModel(devicesObjectId, devicesContainerUUID, devicesDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('InterfaceLogicalName', data.response.interfaceName);
                assert.equal(true, Array.isArray(data.response.selectedNetworks));
                assert.equal('object', typeof data.response.gateway);
                assert.equal(1, data.response.metricValue);
                assert.equal(false, data.response.isTunneled);
                assert.equal('IPv6StaticRoute', data.response.type);
                assert.equal('ipv6StaticRouteUuid', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getIPv6StaticRouteModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllStaticRouteModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllStaticRouteModel(devicesContainerUUID, devicesDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getAllStaticRouteModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStaticRouteModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStaticRouteModel(devicesObjectId, devicesContainerUUID, devicesDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, Array.isArray(data.response.selectedNetworks));
                assert.equal(10, data.response.metricValue);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, data.response.isTunneled);
                assert.equal('string', data.response.interfaceName);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal('object', typeof data.response.gateway);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getStaticRouteModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFTDSubInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllFTDSubInterface(devicesContainerUUID, devicesDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getAllFTDSubInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesUpdateFTDSubInterfaceBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: false
        },
        timestamp: 9
      },
      description: 'string',
      type: 'string',
      enabled: false,
      MTU: 1,
      mode: 'NONE',
      subIntfId: 7,
      arpConfig: [
        {
          macAddress: 'string',
          enableAlias: false,
          ipAddress: 'string'
        }
      ],
      ipv4: {
        static: {
          address: 'string',
          netmask: 'string'
        },
        pppoe: {
          pppoePassword: 'string',
          pppoeRouteMetric: 8,
          ipAddress: 'string',
          vpdnGroupName: 'string',
          pppAuth: 'MSCHAP',
          enableRouteSettings: false,
          storeCredsInFlash: false,
          pppoeUser: 'string'
        },
        dhcp: {
          dhcpRouteMetric: 1,
          enableDefaultRouteDHCP: true
        }
      },
      ifname: 'string',
      ipv6: {
        linkLocalAddress: 'string',
        addresses: [
          {
            address: 'string',
            enforceEUI64: true,
            prefix: 'string'
          }
        ],
        raInterval: 4,
        enableAutoConfig: false,
        nsInterval: 7,
        prefixes: [
          {
            default: true,
            address: 'string',
            advertisement: {
              autoConfig: false,
              offlink: true,
              preferLifeTime: {}
            }
          }
        ],
        enforceEUI64: true,
        enableRA: true,
        dadAttempts: 9,
        enableDHCPAddrConfig: false,
        enableDHCPNonAddrConfig: true,
        raLifeTime: 10,
        enableIPV6: true,
        reachableTime: 3
      },
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      macLearn: false,
      managementOnly: false,
      enableAntiSpoofing: true,
      securityZone: {
        interfaceMode: 'PASSIVE',
        interfaces: [
          {
            metadata: {
              lastUser: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              domain: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              readOnly: {
                reason: {},
                state: false
              },
              timestamp: 6
            },
            name: 'string',
            description: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string',
            version: 'string'
          }
        ],
        metadata: {
          lastUser: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: {},
            state: true
          },
          timestamp: 2
        },
        name: 'string',
        overridable: false,
        description: 'string',
        links: {
          parent: 'string',
          self: 'string'
        },
        overrides: {
          parent: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          target: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          }
        },
        id: 'string',
        type: 'string',
        version: 'string'
      },
      activeMACAddress: 'string',
      vlanId: 6,
      fragmentReassembly: true,
      macTable: [
        'string'
      ],
      standbyMACAddress: 'string',
      enableDNSLookup: false,
      overrideDefaultFragmentSetting: {
        chain: 2,
        size: 8,
        timeout: 1
      },
      version: 'string',
      name: 'string'
    };
    describe('#updateFTDSubInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFTDSubInterface(devicesObjectId, devicesContainerUUID, devicesDomainUUID, devicesUpdateFTDSubInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'updateFTDSubInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFTDSubInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFTDSubInterface(devicesObjectId, devicesContainerUUID, devicesDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('object', typeof data.response.links);
                assert.equal('SubInterface', data.response.type);
                assert.equal(2, data.response.vlanId);
                assert.equal(25769804817, data.response.subIntfId);
                assert.equal(true, data.response.enabled);
                assert.equal(1500, data.response.MTU);
                assert.equal('SubIntf2', data.response.ifname);
                assert.equal('object', typeof data.response.ipv4);
                assert.equal('object', typeof data.response.ipv6);
                assert.equal(false, data.response.managementOnly);
                assert.equal(false, data.response.enableAntiSpoofing);
                assert.equal('object', typeof data.response.overrideDefaultFragmentSetting);
                assert.equal('GigabitEthernet1/4', data.response.name);
                assert.equal('subinterfaceUUID', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getFTDSubInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllVirtualSwitch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllVirtualSwitch(devicesContainerUUID, devicesDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getAllVirtualSwitch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesUpdateVirtualSwitchBodyParam = {
      dropBPDU: true,
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: false
        },
        timestamp: 5
      },
      sensorPolicy: {
        metadata: {
          lastUser: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: {},
            state: false
          },
          timestamp: 8
        },
        defaultAction: {
          logEnd: true,
          logBegin: true,
          metadata: {},
          snmpConfig: {},
          intrusionPolicy: {},
          sendEventsToFMC: false,
          description: 'string',
          type: 'string',
          variableSet: {},
          version: 'string',
          syslogConfig: {},
          name: 'string',
          action: {},
          links: {},
          id: 'string'
        },
        name: 'string',
        description: 'string',
        rules: [
          {
            metadata: {},
            snmpConfig: {},
            sourceNetworks: {},
            syslogSeverity: {},
            sourceZones: {},
            destinationZones: {},
            description: 'string',
            originalSourceNetworks: {},
            enableSyslog: true,
            type: 'string',
            safeSearch: {},
            enabled: true,
            syslogConfig: {},
            urls: {},
            endPointDeviceTypes: [
              {}
            ],
            destinationNetworks: {},
            youTube: {},
            action: {},
            networkAccessDeviceIPs: {},
            links: {},
            id: 'string',
            sourceSecurityGroupTags: {},
            logEnd: true,
            logBegin: false,
            sendEventsToFMC: true,
            destinationPorts: {},
            sourcePorts: {},
            ipsPolicy: {},
            variableSet: {},
            version: 'string',
            users: {},
            logFiles: false,
            newComments: [
              'string'
            ],
            commentHistoryList: [
              {}
            ],
            filePolicy: {},
            name: 'string',
            vlanTags: {},
            applications: {}
          }
        ],
        links: {
          parent: 'string',
          self: 'string'
        },
        id: 'string',
        type: 'string',
        version: 'string'
      },
      description: 'string',
      type: 'string',
      version: 'string',
      strictTCPEnforcement: false,
      domainId: 8,
      staticMacEntries: [
        {
          interface: {
            metadata: {
              lastUser: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              domain: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              readOnly: {
                reason: {},
                state: false
              },
              timestamp: 10
            },
            name: 'string',
            description: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string',
            version: 'string'
          },
          mac: 'string'
        }
      ],
      name: 'string',
      switchedInterfaces: [
        {
          metadata: {
            lastUser: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            domain: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            readOnly: {
              reason: {},
              state: true
            },
            timestamp: 5
          },
          name: 'string',
          description: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string',
          version: 'string'
        }
      ],
      enableSpanningTreeProtocol: true,
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      hybridInterface: {
        metadata: {
          lastUser: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: {},
            state: true
          },
          timestamp: 7
        },
        name: 'string',
        description: 'string',
        links: {
          parent: 'string',
          self: 'string'
        },
        id: 'string',
        type: 'string',
        version: 'string'
      }
    };
    describe('#updateVirtualSwitch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateVirtualSwitch(devicesObjectId, devicesContainerUUID, devicesDomainUUID, devicesUpdateVirtualSwitchBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'updateVirtualSwitch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualSwitch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVirtualSwitch(devicesObjectId, devicesContainerUUID, devicesDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('virtual_switch_1', data.response.name);
                assert.equal('virtualswitchUUID1', data.response.id);
                assert.equal('VirtualSwitch', data.response.type);
                assert.equal('object', typeof data.response.links);
                assert.equal(2, data.response.domainId);
                assert.equal('object', typeof data.response.hybridInterface);
                assert.equal(true, Array.isArray(data.response.switchedInterfaces));
                assert.equal(false, data.response.dropBPDU);
                assert.equal(false, data.response.enableSpanningTreeProtocol);
                assert.equal(false, data.response.strictTCPEnforcement);
                assert.equal('object', typeof data.response.metadata);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getVirtualSwitch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesUpdateDeviceBodyParam = {
      hostName: 'string',
      license_caps: [
        'string'
      ],
      regKey: 'string'
    };
    describe('#updateDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDevice(devicesObjectId, devicesDomainUUID, devicesUpdateDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'updateDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFpmDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFpmDevice(devicesObjectId, devicesDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.hostName);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('string', data.response.modelId);
                assert.equal('string', data.response.natID);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.license_caps));
                assert.equal('TRANSPARENT', data.response.ftdMode);
                assert.equal('string', data.response.modelType);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.regKey);
                assert.equal(false, data.response.keepLocalEvents);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.sw_version);
                assert.equal('string', data.response.healthStatus);
                assert.equal('object', typeof data.response.healthPolicy);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.model);
                assert.equal('string', data.response.modelNumber);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.accessPolicy);
                assert.equal(true, data.response.prohibitPacketTransfer);
                assert.equal('object', typeof data.response.deviceGroup);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getFpmDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyAssignmentsDomainUUID = 'fakedata';
    let policyAssignmentsObjectId = 'fakedata';
    const policyAssignmentsCreatePolicyAssignmentBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'SYSTEM',
          state: false
        },
        timestamp: 1
      },
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      type: 'string',
      targets: [
        {
          metadata: {
            lastUser: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            domain: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            readOnly: {
              reason: {},
              state: false
            },
            timestamp: 6
          },
          name: 'string',
          description: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string',
          version: 'string'
        }
      ],
      version: 'string',
      policy: {
        metadata: {
          lastUser: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: {},
            state: false
          },
          timestamp: 6
        },
        name: 'string',
        description: 'string',
        links: {
          parent: 'string',
          self: 'string'
        },
        id: 'string',
        type: 'string',
        version: 'string'
      }
    };
    describe('#createPolicyAssignment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPolicyAssignment(policyAssignmentsDomainUUID, policyAssignmentsCreatePolicyAssignmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal(true, Array.isArray(data.response.targets));
                assert.equal('string', data.response.version);
                assert.equal('object', typeof data.response.policy);
              } else {
                runCommonAsserts(data, error);
              }
              policyAssignmentsObjectId = data.response.id;
              saveMockData('PolicyAssignments', 'createPolicyAssignment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPolicyAssignment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllPolicyAssignment(policyAssignmentsDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PolicyAssignments', 'getAllPolicyAssignment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyAssignmentsUpdatePolicyAssignmentBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'DOMAIN',
          state: false
        },
        timestamp: 4
      },
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      type: 'string',
      targets: [
        {
          metadata: {
            lastUser: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            domain: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            readOnly: {
              reason: {},
              state: false
            },
            timestamp: 1
          },
          name: 'string',
          description: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string',
          version: 'string'
        }
      ],
      version: 'string',
      policy: {
        metadata: {
          lastUser: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: {},
            state: true
          },
          timestamp: 7
        },
        name: 'string',
        description: 'string',
        links: {
          parent: 'string',
          self: 'string'
        },
        id: 'string',
        type: 'string',
        version: 'string'
      }
    };
    describe('#updatePolicyAssignment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePolicyAssignment(policyAssignmentsObjectId, policyAssignmentsDomainUUID, policyAssignmentsUpdatePolicyAssignmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PolicyAssignments', 'updatePolicyAssignment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyAssignment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPolicyAssignment(policyAssignmentsObjectId, policyAssignmentsDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('policyassignmentUUID', data.response.id);
                assert.equal('PolicyAssignment', data.response.type);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.policy);
                assert.equal(true, Array.isArray(data.response.targets));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PolicyAssignments', 'getPolicyAssignment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceHAPairsDomainUUID = 'fakedata';
    const deviceHAPairsCreateFTDHADeviceContainerBodyParam = {
      ftdHABootstrap: {},
      secondary: {},
      primary: {}
    };
    describe('#createFTDHADeviceContainer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createFTDHADeviceContainer(deviceHAPairsDomainUUID, deviceHAPairsCreateFTDHADeviceContainerBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.primary);
                assert.equal('object', typeof data.response.secondary);
                assert.equal('<ha-name>', data.response.name);
                assert.equal('DeviceHAPair', data.response.type);
                assert.equal('object', typeof data.response.ftdHABootstrap);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceHAPairs', 'createFTDHADeviceContainer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceHAPairsContainerUUID = 'fakedata';
    const deviceHAPairsCreateFTDHAInterfaceMACAddressesBodyParam = {
      failoverActiveMac: 'string',
      failoverStandbyMac: 'string',
      physicalInterface: {}
    };
    describe('#createFTDHAInterfaceMACAddresses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createFTDHAInterfaceMACAddresses(deviceHAPairsContainerUUID, deviceHAPairsDomainUUID, deviceHAPairsCreateFTDHAInterfaceMACAddressesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.failoverActiveMac);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('string', data.response.failoverStandbyMac);
                assert.equal('object', typeof data.response.physicalInterface);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceHAPairs', 'createFTDHAInterfaceMACAddresses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFTDHADeviceContainer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllFTDHADeviceContainer(deviceHAPairsDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceHAPairs', 'getAllFTDHADeviceContainer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFTDHAInterfaceMACAddresses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllFTDHAInterfaceMACAddresses(deviceHAPairsContainerUUID, deviceHAPairsDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceHAPairs', 'getAllFTDHAInterfaceMACAddresses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceHAPairsObjectId = 'fakedata';
    const deviceHAPairsUpdateFTDHAInterfaceMACAddressesBodyParam = {
      failoverActiveMac: 'string',
      failoverStandbyMac: 'string',
      physicalInterface: {}
    };
    describe('#updateFTDHAInterfaceMACAddresses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFTDHAInterfaceMACAddresses(deviceHAPairsObjectId, deviceHAPairsContainerUUID, deviceHAPairsDomainUUID, deviceHAPairsUpdateFTDHAInterfaceMACAddressesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceHAPairs', 'updateFTDHAInterfaceMACAddresses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFTDHAInterfaceMACAddresses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFTDHAInterfaceMACAddresses(deviceHAPairsObjectId, deviceHAPairsContainerUUID, deviceHAPairsDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('<failover_interface_mac-address_uuid>', data.response.id);
                assert.equal('FailoverInterfaceMACAddressConfig', data.response.type);
                assert.equal('object', typeof data.response.physicalInterface);
                assert.equal('0050.56a9.603c', data.response.failoverActiveMac);
                assert.equal('0050.56a9.603d', data.response.failoverStandbyMac);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceHAPairs', 'getFTDHAInterfaceMACAddresses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFTDHAMonitoredInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllFTDHAMonitoredInterfaces(deviceHAPairsContainerUUID, deviceHAPairsDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceHAPairs', 'getAllFTDHAMonitoredInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceHAPairsUpdateFTDHAMonitoredInterfacesBodyParam = {
      monitorForFailures: true
    };
    describe('#updateFTDHAMonitoredInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFTDHAMonitoredInterfaces(deviceHAPairsObjectId, deviceHAPairsContainerUUID, deviceHAPairsDomainUUID, deviceHAPairsUpdateFTDHAMonitoredInterfacesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceHAPairs', 'updateFTDHAMonitoredInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFTDHAMonitoredInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFTDHAMonitoredInterfaces(deviceHAPairsObjectId, deviceHAPairsContainerUUID, deviceHAPairsDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('<monitored_interface_policy_uuid>', data.response.id);
                assert.equal('<interface-nameif>', data.response.interfaceLogicalName);
                assert.equal('object', typeof data.response.ipv4Configuration);
                assert.equal('object', typeof data.response.ipv6Configuration);
                assert.equal(true, data.response.monitorForFailures);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceHAPairs', 'getFTDHAMonitoredInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceHAPairsUpdateFTDHADeviceContainerBodyParam = {
      ftdHABootstrap: {},
      secondary: {},
      primary: {}
    };
    describe('#updateFTDHADeviceContainer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFTDHADeviceContainer(deviceHAPairsObjectId, deviceHAPairsDomainUUID, deviceHAPairsUpdateFTDHADeviceContainerBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceHAPairs', 'updateFTDHADeviceContainer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFTDHADeviceContainer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFTDHADeviceContainer(deviceHAPairsObjectId, deviceHAPairsDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.primary);
                assert.equal('object', typeof data.response.secondary);
                assert.equal('<ha-name>', data.response.name);
                assert.equal('DeviceHAPair', data.response.type);
                assert.equal('object', typeof data.response.ftdHAFailoverTriggerCriteria);
                assert.equal('object', typeof data.response.ftdHABootstrap);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceHAPairs', 'getFTDHADeviceContainer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const updatesCreateUpgradeBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: true
        },
        timestamp: 2
      },
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      pushUpgradeFileOnly: true,
      type: 'string',
      targets: [
        {
          metadata: {
            lastUser: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            domain: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            readOnly: {
              reason: {},
              state: true
            },
            timestamp: 4
          },
          name: 'string',
          description: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string',
          version: 'string'
        }
      ],
      version: 'string',
      upgradePackage: {
        metadata: {
          lastUser: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: {},
            state: true
          },
          timestamp: 3
        },
        name: 'string',
        description: 'string',
        links: {
          parent: 'string',
          self: 'string'
        },
        id: 'string',
        type: 'string'
      }
    };
    describe('#createUpgrade - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createUpgrade(updatesCreateUpgradeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal(false, data.response.pushUpgradeFileOnly);
                assert.equal('string', data.response.type);
                assert.equal(true, Array.isArray(data.response.targets));
                assert.equal('string', data.response.version);
                assert.equal('object', typeof data.response.upgradePackage);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Updates', 'createUpgrade', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllUpgradePackage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllUpgradePackage(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Updates', 'getAllUpgradePackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const updatesContainerUUID = 'fakedata';
    describe('#getAllApplicableDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllApplicableDevice(updatesContainerUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Updates', 'getAllApplicableDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const updatesObjectId = 'fakedata';
    describe('#getUpgradePackage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUpgradePackage(updatesObjectId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Updates', 'getUpgradePackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const intelligenceDomainUUID = 'fakedata';
    const intelligenceCreateRESTTaxiiCollectionBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: false
        },
        timestamp: 10
      },
      caCert: 'string',
      feedContent: 'string',
      description: 'string',
      invalidObservables: 10,
      feedStatus: 'string',
      type: 'string',
      downloadOn: false,
      nextRun: 8,
      consumedIndicators: 3,
      totalObservables: 9,
      totalIndicators: 8,
      totalUnsupportedObservables: 3,
      property: {
        llfeedProperty: {
          expirationTime: 10,
          publish: false,
          action: 'string',
          whitelist: false,
          ttl: 6,
          tags: {
            additionalProperties: 'string'
          }
        },
        expirationTime: 10,
        publish: false,
        action: 'string',
        whitelist: false,
        ttl: 4,
        tags: {
          additionalProperties: 'string'
        }
      },
      checksum: 'string',
      llfeedConfig: {
        safeToUpdate: true,
        feedContent: 'string',
        updatedObservables: 10,
        safeToDelete: true,
        feedStatus: 'string',
        type: 'string',
        downloadOn: true,
        nextRun: 3,
        password: 'string',
        totalUnsupportedObservables: 9,
        property: {
          expirationTime: 2,
          publish: true,
          action: 'string',
          whitelist: true,
          ttl: 4,
          tags: {
            additionalProperties: 'string'
          }
        },
        id: 'string',
        clientPrivateKey: 'string',
        totalUpdatedObservables: 2,
        clientCert: 'string',
        subscribedCollections: [
          {
            address: 'string',
            pollIntervalInMinutes: 7,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          }
        ],
        discardedObservables: 1,
        finishTime: 7,
        availableCollections: [
          {
            address: 'string',
            pollIntervalInMinutes: 6,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          }
        ],
        lastRun: 10,
        refreshSec: 1,
        recurring: false,
        refreshMin: 2,
        consumedObservables: 5,
        params: {
          additionalProperties: 'string'
        },
        inRunningState: true,
        version: 'string',
        totalUpdatedIndicators: 8,
        discoveryInfo: [
          {
            address: 'string',
            pollIntervalInMinutes: 6,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          }
        ],
        statusMsg: {
          additionalProperties: {
            additionalProperties: 'string'
          }
        },
        passwd: 'string',
        discardedIndicators: 9,
        consumedUnsupportedObservables: 8,
        name: 'string',
        totalDiscardedObservables: 6,
        caCert: 'string',
        description: 'string',
        consumedIndicators: 7,
        totalIndicators: 2,
        totalObservables: 2,
        checksum: 'string',
        delivery: 'string',
        proxyURL: 'string',
        runNow: false,
        updatedIndicators: 4,
        totalConsumedIndicators: 4,
        refresh: 2,
        totalDiscardedIndicators: 7,
        uri: 'string',
        passwdAsIs: 'string',
        startHour: 6,
        feedType: 'string',
        totalConsumedObservables: 2,
        username: 'string'
      },
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      clientPrivateKey: 'string',
      subscribedCollections: [
        {
          collectionSubType: 'string',
          collectionAddress: 'string',
          collectionContentBinding: 'string',
          collectionPollIntervalInMinutes: 9,
          collectionDescription: 'string',
          llcollectionTopic: {
            address: 'string',
            pollIntervalInMinutes: 9,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          },
          type: 'string',
          collectionProtocolBinding: 'string',
          value: 'string',
          collectionMessageBinding: 'string',
          collectionName: 'string'
        }
      ],
      delivery: 'string',
      clientCert: 'string',
      finishTime: 6,
      availableCollections: [
        {
          collectionSubType: 'string',
          collectionAddress: 'string',
          collectionContentBinding: 'string',
          collectionPollIntervalInMinutes: 9,
          collectionDescription: 'string',
          llcollectionTopic: {
            address: 'string',
            pollIntervalInMinutes: 3,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          },
          type: 'string',
          collectionProtocolBinding: 'string',
          value: 'string',
          collectionMessageBinding: 'string',
          collectionName: 'string'
        }
      ],
      lastRun: 9,
      runNow: false,
      consumedObservables: 7,
      totalDiscardedIndicators: 4,
      refresh: 4,
      params: {
        additionalProperties: 'string'
      },
      uri: 'string',
      feedConfig: {
        safeToUpdate: false,
        feedContent: 'string',
        updatedObservables: 8,
        safeToDelete: true,
        feedStatus: 'string',
        type: 'string',
        downloadOn: true,
        nextRun: 2,
        password: 'string',
        totalUnsupportedObservables: 2,
        property: {
          expirationTime: 9,
          publish: true,
          action: 'string',
          whitelist: false,
          ttl: 1,
          tags: {
            additionalProperties: 'string'
          }
        },
        id: 'string',
        clientPrivateKey: 'string',
        totalUpdatedObservables: 10,
        clientCert: 'string',
        subscribedCollections: [
          {
            address: 'string',
            pollIntervalInMinutes: 7,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          }
        ],
        discardedObservables: 10,
        finishTime: 2,
        availableCollections: [
          {
            address: 'string',
            pollIntervalInMinutes: 6,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          }
        ],
        lastRun: 9,
        refreshSec: 7,
        recurring: false,
        refreshMin: 1,
        consumedObservables: 10,
        params: {
          additionalProperties: 'string'
        },
        inRunningState: false,
        version: 'string',
        totalUpdatedIndicators: 5,
        discoveryInfo: [
          {
            address: 'string',
            pollIntervalInMinutes: 5,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          }
        ],
        statusMsg: {
          additionalProperties: {
            additionalProperties: 'string'
          }
        },
        passwd: 'string',
        discardedIndicators: 10,
        consumedUnsupportedObservables: 7,
        name: 'string',
        totalDiscardedObservables: 5,
        caCert: 'string',
        description: 'string',
        consumedIndicators: 10,
        totalIndicators: 1,
        totalObservables: 2,
        checksum: 'string',
        delivery: 'string',
        proxyURL: 'string',
        runNow: false,
        updatedIndicators: 5,
        totalConsumedIndicators: 2,
        refresh: 1,
        totalDiscardedIndicators: 7,
        uri: 'string',
        passwdAsIs: 'string',
        startHour: 4,
        feedType: 'string',
        totalConsumedObservables: 10,
        username: 'string'
      },
      version: 'string',
      discoveryInfo: [
        {
          collectionSubType: 'string',
          collectionAddress: 'string',
          collectionContentBinding: 'string',
          collectionPollIntervalInMinutes: 9,
          collectionDescription: 'string',
          llcollectionTopic: {
            address: 'string',
            pollIntervalInMinutes: 10,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          },
          type: 'string',
          collectionProtocolBinding: 'string',
          value: 'string',
          collectionMessageBinding: 'string',
          collectionName: 'string'
        }
      ],
      totalInvalidObservables: 10,
      statusMsg: {
        additionalProperties: {
          additionalProperties: 'string'
        }
      },
      passwd: 'string',
      startHour: 1,
      consumedUnsupportedObservables: 8,
      discardedIndicators: 5,
      feedType: 'string',
      name: 'string',
      username: 'string'
    };
    describe('#createRESTTaxiiCollection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createRESTTaxiiCollection(intelligenceDomainUUID, intelligenceCreateRESTTaxiiCollectionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('string', data.response.caCert);
                assert.equal('string', data.response.feedContent);
                assert.equal('string', data.response.description);
                assert.equal(2, data.response.invalidObservables);
                assert.equal('string', data.response.feedStatus);
                assert.equal('string', data.response.type);
                assert.equal(false, data.response.downloadOn);
                assert.equal(9, data.response.nextRun);
                assert.equal(4, data.response.consumedIndicators);
                assert.equal(8, data.response.totalObservables);
                assert.equal(5, data.response.totalIndicators);
                assert.equal(6, data.response.totalUnsupportedObservables);
                assert.equal('object', typeof data.response.property);
                assert.equal('string', data.response.checksum);
                assert.equal('object', typeof data.response.llfeedConfig);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.clientPrivateKey);
                assert.equal(true, Array.isArray(data.response.subscribedCollections));
                assert.equal('string', data.response.delivery);
                assert.equal('string', data.response.clientCert);
                assert.equal(2, data.response.finishTime);
                assert.equal(true, Array.isArray(data.response.availableCollections));
                assert.equal(8, data.response.lastRun);
                assert.equal(false, data.response.runNow);
                assert.equal(8, data.response.consumedObservables);
                assert.equal(10, data.response.totalDiscardedIndicators);
                assert.equal(9, data.response.refresh);
                assert.equal('object', typeof data.response.params);
                assert.equal('string', data.response.uri);
                assert.equal('object', typeof data.response.feedConfig);
                assert.equal('string', data.response.version);
                assert.equal(true, Array.isArray(data.response.discoveryInfo));
                assert.equal(5, data.response.totalInvalidObservables);
                assert.equal('object', typeof data.response.statusMsg);
                assert.equal('string', data.response.passwd);
                assert.equal(2, data.response.startHour);
                assert.equal(2, data.response.consumedUnsupportedObservables);
                assert.equal(6, data.response.discardedIndicators);
                assert.equal('string', data.response.feedType);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.username);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Intelligence', 'createRESTTaxiiCollection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const intelligenceCreateRESTDiscoveryInfoBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: false
        },
        timestamp: 10
      },
      caCert: 'string',
      feedContent: 'string',
      description: 'string',
      invalidObservables: 8,
      feedStatus: 'string',
      type: 'string',
      downloadOn: false,
      nextRun: 3,
      consumedIndicators: 1,
      totalObservables: 4,
      totalIndicators: 5,
      totalUnsupportedObservables: 5,
      property: {
        llfeedProperty: {
          expirationTime: 3,
          publish: true,
          action: 'string',
          whitelist: false,
          ttl: 8,
          tags: {
            additionalProperties: 'string'
          }
        },
        expirationTime: 2,
        publish: false,
        action: 'string',
        whitelist: false,
        ttl: 2,
        tags: {
          additionalProperties: 'string'
        }
      },
      checksum: 'string',
      llfeedConfig: {
        safeToUpdate: false,
        feedContent: 'string',
        updatedObservables: 3,
        safeToDelete: true,
        feedStatus: 'string',
        type: 'string',
        downloadOn: false,
        nextRun: 5,
        password: 'string',
        totalUnsupportedObservables: 7,
        property: {
          expirationTime: 10,
          publish: false,
          action: 'string',
          whitelist: true,
          ttl: 3,
          tags: {
            additionalProperties: 'string'
          }
        },
        id: 'string',
        clientPrivateKey: 'string',
        totalUpdatedObservables: 5,
        clientCert: 'string',
        subscribedCollections: [
          {
            address: 'string',
            pollIntervalInMinutes: 5,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          }
        ],
        discardedObservables: 10,
        finishTime: 6,
        availableCollections: [
          {
            address: 'string',
            pollIntervalInMinutes: 8,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          }
        ],
        lastRun: 7,
        refreshSec: 7,
        recurring: true,
        refreshMin: 5,
        consumedObservables: 6,
        params: {
          additionalProperties: 'string'
        },
        inRunningState: true,
        version: 'string',
        totalUpdatedIndicators: 6,
        discoveryInfo: [
          {
            address: 'string',
            pollIntervalInMinutes: 4,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          }
        ],
        statusMsg: {
          additionalProperties: {
            additionalProperties: 'string'
          }
        },
        passwd: 'string',
        discardedIndicators: 3,
        consumedUnsupportedObservables: 1,
        name: 'string',
        totalDiscardedObservables: 10,
        caCert: 'string',
        description: 'string',
        consumedIndicators: 3,
        totalIndicators: 5,
        totalObservables: 3,
        checksum: 'string',
        delivery: 'string',
        proxyURL: 'string',
        runNow: false,
        updatedIndicators: 6,
        totalConsumedIndicators: 3,
        refresh: 6,
        totalDiscardedIndicators: 5,
        uri: 'string',
        passwdAsIs: 'string',
        startHour: 8,
        feedType: 'string',
        totalConsumedObservables: 10,
        username: 'string'
      },
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      clientPrivateKey: 'string',
      subscribedCollections: [
        {
          collectionSubType: 'string',
          collectionAddress: 'string',
          collectionContentBinding: 'string',
          collectionPollIntervalInMinutes: 3,
          collectionDescription: 'string',
          llcollectionTopic: {
            address: 'string',
            pollIntervalInMinutes: 7,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          },
          type: 'string',
          collectionProtocolBinding: 'string',
          value: 'string',
          collectionMessageBinding: 'string',
          collectionName: 'string'
        }
      ],
      delivery: 'string',
      clientCert: 'string',
      finishTime: 10,
      availableCollections: [
        {
          collectionSubType: 'string',
          collectionAddress: 'string',
          collectionContentBinding: 'string',
          collectionPollIntervalInMinutes: 8,
          collectionDescription: 'string',
          llcollectionTopic: {
            address: 'string',
            pollIntervalInMinutes: 2,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          },
          type: 'string',
          collectionProtocolBinding: 'string',
          value: 'string',
          collectionMessageBinding: 'string',
          collectionName: 'string'
        }
      ],
      lastRun: 1,
      runNow: true,
      consumedObservables: 9,
      totalDiscardedIndicators: 3,
      refresh: 3,
      params: {
        additionalProperties: 'string'
      },
      uri: 'string',
      feedConfig: {
        safeToUpdate: false,
        feedContent: 'string',
        updatedObservables: 10,
        safeToDelete: false,
        feedStatus: 'string',
        type: 'string',
        downloadOn: true,
        nextRun: 3,
        password: 'string',
        totalUnsupportedObservables: 9,
        property: {
          expirationTime: 2,
          publish: true,
          action: 'string',
          whitelist: true,
          ttl: 9,
          tags: {
            additionalProperties: 'string'
          }
        },
        id: 'string',
        clientPrivateKey: 'string',
        totalUpdatedObservables: 8,
        clientCert: 'string',
        subscribedCollections: [
          {
            address: 'string',
            pollIntervalInMinutes: 7,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          }
        ],
        discardedObservables: 8,
        finishTime: 8,
        availableCollections: [
          {
            address: 'string',
            pollIntervalInMinutes: 10,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          }
        ],
        lastRun: 7,
        refreshSec: 3,
        recurring: false,
        refreshMin: 7,
        consumedObservables: 9,
        params: {
          additionalProperties: 'string'
        },
        inRunningState: true,
        version: 'string',
        totalUpdatedIndicators: 3,
        discoveryInfo: [
          {
            address: 'string',
            pollIntervalInMinutes: 9,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          }
        ],
        statusMsg: {
          additionalProperties: {
            additionalProperties: 'string'
          }
        },
        passwd: 'string',
        discardedIndicators: 6,
        consumedUnsupportedObservables: 1,
        name: 'string',
        totalDiscardedObservables: 9,
        caCert: 'string',
        description: 'string',
        consumedIndicators: 2,
        totalIndicators: 5,
        totalObservables: 6,
        checksum: 'string',
        delivery: 'string',
        proxyURL: 'string',
        runNow: false,
        updatedIndicators: 10,
        totalConsumedIndicators: 2,
        refresh: 7,
        totalDiscardedIndicators: 4,
        uri: 'string',
        passwdAsIs: 'string',
        startHour: 9,
        feedType: 'string',
        totalConsumedObservables: 8,
        username: 'string'
      },
      version: 'string',
      discoveryInfo: [
        {
          collectionSubType: 'string',
          collectionAddress: 'string',
          collectionContentBinding: 'string',
          collectionPollIntervalInMinutes: 3,
          collectionDescription: 'string',
          llcollectionTopic: {
            address: 'string',
            pollIntervalInMinutes: 2,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          },
          type: 'string',
          collectionProtocolBinding: 'string',
          value: 'string',
          collectionMessageBinding: 'string',
          collectionName: 'string'
        }
      ],
      totalInvalidObservables: 10,
      statusMsg: {
        additionalProperties: {
          additionalProperties: 'string'
        }
      },
      passwd: 'string',
      startHour: 1,
      consumedUnsupportedObservables: 5,
      discardedIndicators: 8,
      feedType: 'string',
      name: 'string',
      username: 'string'
    };
    describe('#createRESTDiscoveryInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createRESTDiscoveryInfo(intelligenceDomainUUID, intelligenceCreateRESTDiscoveryInfoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('string', data.response.caCert);
                assert.equal('string', data.response.feedContent);
                assert.equal('string', data.response.description);
                assert.equal(10, data.response.invalidObservables);
                assert.equal('string', data.response.feedStatus);
                assert.equal('string', data.response.type);
                assert.equal(true, data.response.downloadOn);
                assert.equal(7, data.response.nextRun);
                assert.equal(5, data.response.consumedIndicators);
                assert.equal(9, data.response.totalObservables);
                assert.equal(5, data.response.totalIndicators);
                assert.equal(3, data.response.totalUnsupportedObservables);
                assert.equal('object', typeof data.response.property);
                assert.equal('string', data.response.checksum);
                assert.equal('object', typeof data.response.llfeedConfig);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.clientPrivateKey);
                assert.equal(true, Array.isArray(data.response.subscribedCollections));
                assert.equal('string', data.response.delivery);
                assert.equal('string', data.response.clientCert);
                assert.equal(7, data.response.finishTime);
                assert.equal(true, Array.isArray(data.response.availableCollections));
                assert.equal(1, data.response.lastRun);
                assert.equal(true, data.response.runNow);
                assert.equal(7, data.response.consumedObservables);
                assert.equal(7, data.response.totalDiscardedIndicators);
                assert.equal(4, data.response.refresh);
                assert.equal('object', typeof data.response.params);
                assert.equal('string', data.response.uri);
                assert.equal('object', typeof data.response.feedConfig);
                assert.equal('string', data.response.version);
                assert.equal(true, Array.isArray(data.response.discoveryInfo));
                assert.equal(7, data.response.totalInvalidObservables);
                assert.equal('object', typeof data.response.statusMsg);
                assert.equal('string', data.response.passwd);
                assert.equal(10, data.response.startHour);
                assert.equal(1, data.response.consumedUnsupportedObservables);
                assert.equal(8, data.response.discardedIndicators);
                assert.equal('string', data.response.feedType);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.username);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Intelligence', 'createRESTDiscoveryInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const intelligenceCreateRESTTidSourceBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: true
        },
        timestamp: 7
      },
      caCert: 'string',
      feedContent: 'string',
      description: 'string',
      invalidObservables: 3,
      feedStatus: 'string',
      type: 'string',
      downloadOn: false,
      nextRun: 5,
      consumedIndicators: 10,
      totalObservables: 4,
      totalIndicators: 5,
      totalUnsupportedObservables: 7,
      property: {
        llfeedProperty: {
          expirationTime: 10,
          publish: false,
          action: 'string',
          whitelist: true,
          ttl: 8,
          tags: {
            additionalProperties: 'string'
          }
        },
        expirationTime: 2,
        publish: true,
        action: 'string',
        whitelist: false,
        ttl: 6,
        tags: {
          additionalProperties: 'string'
        }
      },
      checksum: 'string',
      llfeedConfig: {
        safeToUpdate: false,
        feedContent: 'string',
        updatedObservables: 5,
        safeToDelete: true,
        feedStatus: 'string',
        type: 'string',
        downloadOn: false,
        nextRun: 1,
        password: 'string',
        totalUnsupportedObservables: 6,
        property: {
          expirationTime: 6,
          publish: false,
          action: 'string',
          whitelist: true,
          ttl: 8,
          tags: {
            additionalProperties: 'string'
          }
        },
        id: 'string',
        clientPrivateKey: 'string',
        totalUpdatedObservables: 5,
        clientCert: 'string',
        subscribedCollections: [
          {
            address: 'string',
            pollIntervalInMinutes: 3,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          }
        ],
        discardedObservables: 4,
        finishTime: 7,
        availableCollections: [
          {
            address: 'string',
            pollIntervalInMinutes: 4,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          }
        ],
        lastRun: 4,
        refreshSec: 7,
        recurring: true,
        refreshMin: 4,
        consumedObservables: 9,
        params: {
          additionalProperties: 'string'
        },
        inRunningState: true,
        version: 'string',
        totalUpdatedIndicators: 5,
        discoveryInfo: [
          {
            address: 'string',
            pollIntervalInMinutes: 6,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          }
        ],
        statusMsg: {
          additionalProperties: {
            additionalProperties: 'string'
          }
        },
        passwd: 'string',
        discardedIndicators: 3,
        consumedUnsupportedObservables: 9,
        name: 'string',
        totalDiscardedObservables: 6,
        caCert: 'string',
        description: 'string',
        consumedIndicators: 4,
        totalIndicators: 4,
        totalObservables: 8,
        checksum: 'string',
        delivery: 'string',
        proxyURL: 'string',
        runNow: true,
        updatedIndicators: 2,
        totalConsumedIndicators: 7,
        refresh: 1,
        totalDiscardedIndicators: 3,
        uri: 'string',
        passwdAsIs: 'string',
        startHour: 1,
        feedType: 'string',
        totalConsumedObservables: 3,
        username: 'string'
      },
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      clientPrivateKey: 'string',
      subscribedCollections: [
        {
          collectionSubType: 'string',
          collectionAddress: 'string',
          collectionContentBinding: 'string',
          collectionPollIntervalInMinutes: 3,
          collectionDescription: 'string',
          llcollectionTopic: {
            address: 'string',
            pollIntervalInMinutes: 1,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          },
          type: 'string',
          collectionProtocolBinding: 'string',
          value: 'string',
          collectionMessageBinding: 'string',
          collectionName: 'string'
        }
      ],
      delivery: 'string',
      clientCert: 'string',
      finishTime: 2,
      availableCollections: [
        {
          collectionSubType: 'string',
          collectionAddress: 'string',
          collectionContentBinding: 'string',
          collectionPollIntervalInMinutes: 2,
          collectionDescription: 'string',
          llcollectionTopic: {
            address: 'string',
            pollIntervalInMinutes: 5,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          },
          type: 'string',
          collectionProtocolBinding: 'string',
          value: 'string',
          collectionMessageBinding: 'string',
          collectionName: 'string'
        }
      ],
      lastRun: 8,
      runNow: true,
      consumedObservables: 7,
      totalDiscardedIndicators: 5,
      refresh: 3,
      params: {
        additionalProperties: 'string'
      },
      uri: 'string',
      feedConfig: {
        safeToUpdate: true,
        feedContent: 'string',
        updatedObservables: 4,
        safeToDelete: true,
        feedStatus: 'string',
        type: 'string',
        downloadOn: true,
        nextRun: 5,
        password: 'string',
        totalUnsupportedObservables: 2,
        property: {
          expirationTime: 8,
          publish: true,
          action: 'string',
          whitelist: false,
          ttl: 2,
          tags: {
            additionalProperties: 'string'
          }
        },
        id: 'string',
        clientPrivateKey: 'string',
        totalUpdatedObservables: 6,
        clientCert: 'string',
        subscribedCollections: [
          {
            address: 'string',
            pollIntervalInMinutes: 6,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          }
        ],
        discardedObservables: 1,
        finishTime: 10,
        availableCollections: [
          {
            address: 'string',
            pollIntervalInMinutes: 7,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          }
        ],
        lastRun: 9,
        refreshSec: 1,
        recurring: false,
        refreshMin: 1,
        consumedObservables: 6,
        params: {
          additionalProperties: 'string'
        },
        inRunningState: false,
        version: 'string',
        totalUpdatedIndicators: 1,
        discoveryInfo: [
          {
            address: 'string',
            pollIntervalInMinutes: 10,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          }
        ],
        statusMsg: {
          additionalProperties: {
            additionalProperties: 'string'
          }
        },
        passwd: 'string',
        discardedIndicators: 6,
        consumedUnsupportedObservables: 4,
        name: 'string',
        totalDiscardedObservables: 7,
        caCert: 'string',
        description: 'string',
        consumedIndicators: 8,
        totalIndicators: 1,
        totalObservables: 1,
        checksum: 'string',
        delivery: 'string',
        proxyURL: 'string',
        runNow: false,
        updatedIndicators: 3,
        totalConsumedIndicators: 6,
        refresh: 1,
        totalDiscardedIndicators: 5,
        uri: 'string',
        passwdAsIs: 'string',
        startHour: 4,
        feedType: 'string',
        totalConsumedObservables: 8,
        username: 'string'
      },
      version: 'string',
      discoveryInfo: [
        {
          collectionSubType: 'string',
          collectionAddress: 'string',
          collectionContentBinding: 'string',
          collectionPollIntervalInMinutes: 7,
          collectionDescription: 'string',
          llcollectionTopic: {
            address: 'string',
            pollIntervalInMinutes: 3,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          },
          type: 'string',
          collectionProtocolBinding: 'string',
          value: 'string',
          collectionMessageBinding: 'string',
          collectionName: 'string'
        }
      ],
      totalInvalidObservables: 2,
      statusMsg: {
        additionalProperties: {
          additionalProperties: 'string'
        }
      },
      passwd: 'string',
      startHour: 10,
      consumedUnsupportedObservables: 10,
      discardedIndicators: 8,
      feedType: 'string',
      name: 'string',
      username: 'string'
    };
    describe('#createRESTTidSource - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createRESTTidSource(intelligenceDomainUUID, intelligenceCreateRESTTidSourceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('string', data.response.caCert);
                assert.equal('string', data.response.feedContent);
                assert.equal('string', data.response.description);
                assert.equal(4, data.response.invalidObservables);
                assert.equal('string', data.response.feedStatus);
                assert.equal('string', data.response.type);
                assert.equal(true, data.response.downloadOn);
                assert.equal(2, data.response.nextRun);
                assert.equal(6, data.response.consumedIndicators);
                assert.equal(10, data.response.totalObservables);
                assert.equal(2, data.response.totalIndicators);
                assert.equal(4, data.response.totalUnsupportedObservables);
                assert.equal('object', typeof data.response.property);
                assert.equal('string', data.response.checksum);
                assert.equal('object', typeof data.response.llfeedConfig);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.clientPrivateKey);
                assert.equal(true, Array.isArray(data.response.subscribedCollections));
                assert.equal('string', data.response.delivery);
                assert.equal('string', data.response.clientCert);
                assert.equal(3, data.response.finishTime);
                assert.equal(true, Array.isArray(data.response.availableCollections));
                assert.equal(4, data.response.lastRun);
                assert.equal(true, data.response.runNow);
                assert.equal(1, data.response.consumedObservables);
                assert.equal(5, data.response.totalDiscardedIndicators);
                assert.equal(4, data.response.refresh);
                assert.equal('object', typeof data.response.params);
                assert.equal('string', data.response.uri);
                assert.equal('object', typeof data.response.feedConfig);
                assert.equal('string', data.response.version);
                assert.equal(true, Array.isArray(data.response.discoveryInfo));
                assert.equal(3, data.response.totalInvalidObservables);
                assert.equal('object', typeof data.response.statusMsg);
                assert.equal('string', data.response.passwd);
                assert.equal(10, data.response.startHour);
                assert.equal(1, data.response.consumedUnsupportedObservables);
                assert.equal(6, data.response.discardedIndicators);
                assert.equal('string', data.response.feedType);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.username);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Intelligence', 'createRESTTidSource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRESTElement - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllRESTElement(intelligenceDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Intelligence', 'getAllRESTElement', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const intelligenceObjectId = 'fakedata';
    describe('#getRESTElement - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRESTElement(intelligenceObjectId, intelligenceDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('elementUUID', data.response.id);
                assert.equal('element', data.response.type);
                assert.equal('Sample Element', data.response.name);
                assert.equal('Sample Model', data.response.model);
                assert.equal('-----BEGIN CACERTIFICATE-----\nMIIGLT...\n-----END CACERTIFICATE-----\n', data.response.caCert);
                assert.equal('-----BEGIN CERTIFICATE-----\nMIIGLT...\n-----END CERTIFICATE-----\n', data.response.cert);
                assert.equal('-----BEGIN RSA PRIVATE KEY-----\nMIIJK...\n-----END RSA PRIVATE KEY-----\n', data.response.key);
                assert.equal('SampleStatus', data.response.status);
                assert.equal(1457566762, data.response.registrationDate);
                assert.equal('object', typeof data.response.miscData);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Intelligence', 'getRESTElement', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRESTIncident - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllRESTIncident(intelligenceDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Intelligence', 'getAllRESTIncident', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const intelligenceUpdateRESTIncidentBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: true
        },
        timestamp: 6
      },
      indicatorName: 'string',
      equation: {
        op: 'string',
        item: {
          op: 'string',
          condition: 'string',
          isRealized: false,
          children: [
            {}
          ],
          type: 'string',
          value: 'string',
          applyCondition: 'string'
        },
        condition: 'string',
        isRealized: true,
        children: [
          {}
        ],
        type: 'string',
        value: 'string',
        applyCondition: 'string'
      },
      description: 'string',
      type: 'string',
      version: 'string',
      iteratorId: 'string',
      indicatorId: 'string',
      actionTaken: 'string',
      feedId: 'string',
      observations: [
        {
          elementId: 'string',
          data: {
            actionTaken: 'string',
            llobservationData: {
              actionTaken: 'string',
              miscData: {},
              type: 'string',
              value: 'string'
            },
            miscData: {
              additionalProperties: 'string'
            },
            type: 'string',
            value: 'string'
          },
          count: 7,
          type: 'string',
          timestamp: 8,
          elementName: 'string',
          llobservation: {
            elementId: 'string',
            data: {
              actionTaken: 'string',
              miscData: {},
              type: 'string',
              value: 'string'
            },
            count: 4,
            type: 'string',
            version: 'string',
            elementName: 'string',
            timestamp: 10
          }
        }
      ],
      property: {
        llfeedProperty: {
          expirationTime: 7,
          publish: true,
          action: 'string',
          whitelist: false,
          ttl: 5,
          tags: {
            additionalProperties: 'string'
          }
        },
        expirationTime: 9,
        publish: false,
        action: 'string',
        whitelist: false,
        ttl: 7,
        tags: {
          additionalProperties: 'string'
        }
      },
      name: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      sourceName: 'string',
      realizedAt: 3,
      llincident: {
        indicatorName: 'string',
        equation: {
          op: 'string',
          condition: 'string',
          isRealized: true,
          children: [
            {}
          ],
          type: 'string',
          value: 'string',
          applyCondition: 'string'
        },
        description: 'string',
        type: 'string',
        version: 'string',
        indicatorId: 'string',
        actionTaken: 'string',
        feedId: 'string',
        observations: [
          {
            elementId: 'string',
            data: {
              actionTaken: 'string',
              miscData: {},
              type: 'string',
              value: 'string'
            },
            count: 6,
            type: 'string',
            version: 'string',
            elementName: 'string',
            timestamp: 4
          }
        ],
        publish: false,
        name: 'string',
        property: {
          expirationTime: 9,
          publish: false,
          action: 'string',
          whitelist: true,
          ttl: 1,
          tags: {
            additionalProperties: 'string'
          }
        },
        id: 'string',
        sourceName: 'string',
        realizedAt: 2,
        status: 'string',
        updatedAt: 5
      },
      id: 'string',
      status: 'string',
      updatedAt: 5
    };
    describe('#updateRESTIncident - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateRESTIncident(intelligenceObjectId, intelligenceDomainUUID, intelligenceUpdateRESTIncidentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Intelligence', 'updateRESTIncident', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRESTIncident - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRESTIncident(intelligenceObjectId, intelligenceDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1499839877, data.response.updatedAt);
                assert.equal('Test URL Source', data.response.sourceName);
                assert.equal('object', typeof data.response.equation);
                assert.equal('Test Indicators', data.response.indicatorName);
                assert.equal(true, Array.isArray(data.response.observations));
                assert.equal('indicatorUUID', data.response.indicatorId);
                assert.equal('feedUUID', data.response.feedId);
                assert.equal(0, data.response.realizedAt);
                assert.equal('partiallyBlocked', data.response.actionTaken);
                assert.equal('object', typeof data.response.property);
                assert.equal('new', data.response.status);
                assert.equal('object', typeof data.response.links);
                assert.equal('incidentUUID', data.response.id);
                assert.equal('incident', data.response.type);
                assert.equal('1.0.0', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Intelligence', 'getRESTIncident', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRESTIndicator - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllRESTIndicator(intelligenceDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Intelligence', 'getAllRESTIndicator', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const intelligenceUpdateRESTIndicatorBodyParam = {
      indicator: {
        pending: [
          'string'
        ],
        description: 'string',
        rawData: 'string',
        type: 'string',
        noRealizedIncidents: 10,
        observables: [
          {
            miscData: {
              additionalProperties: 'string'
            },
            type: 'string',
            observableType: 'string',
            version: 'string',
            unEncryptedDBId: 'string',
            indicatorCount: 1,
            effectiveProperty: {
              expirationTime: 8,
              publish: false,
              action: 'string',
              whitelist: true,
              ttl: 10,
              tags: {
                additionalProperties: 'string'
              }
            },
            dbid: 'string',
            id: 'string',
            value: 'string',
            inheritedProperty: {
              expirationTime: 9,
              publish: true,
              action: 'string',
              whitelist: true,
              ttl: 1,
              tags: {
                additionalProperties: 'string'
              }
            },
            customProperty: {
              expirationTime: 4,
              publish: true,
              action: 'string',
              whitelist: true,
              ttl: 1,
              tags: {
                additionalProperties: 'string'
              }
            },
            updatedAt: 4
          }
        ],
        stale: true,
        unsupported: false,
        feedId: 'string',
        effectiveProperty: {
          expirationTime: 8,
          publish: false,
          action: 'string',
          whitelist: false,
          ttl: 7,
          tags: {
            additionalProperties: 'string'
          }
        },
        action: 'string',
        id: 'string',
        noPartialIncidents: 9,
        customProperty: {
          expirationTime: 4,
          publish: true,
          action: 'string',
          whitelist: true,
          ttl: 1,
          tags: {
            additionalProperties: 'string'
          }
        },
        updatedAt: 6,
        equation: {
          op: 'string',
          condition: 'string',
          isRealized: true,
          children: [
            {}
          ],
          type: 'string',
          value: 'string',
          applyCondition: 'string'
        },
        version: 'string',
        ttl: 5,
        tags: {
          additionalProperties: 'string'
        },
        indicatorVersion: 'string',
        expirationTime: 7,
        publish: false,
        name: 'string',
        invalid: false,
        sourceName: 'string',
        inheritedProperty: {
          expirationTime: 8,
          publish: true,
          action: 'string',
          whitelist: false,
          ttl: 1,
          tags: {
            additionalProperties: 'string'
          }
        },
        fileId: 'string'
      },
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: true
        },
        timestamp: 7
      },
      pending: [
        'string'
      ],
      description: 'string',
      rawData: 'string',
      type: 'string',
      noRealizedIncidents: 9,
      observables: [
        {
          metadata: {
            lastUser: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            domain: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            readOnly: {
              reason: {},
              state: false
            },
            timestamp: 8
          },
          description: 'string',
          observableType: 'string',
          type: 'string',
          version: 'string',
          indicatorCount: 5,
          effectiveProperty: {
            llfeedProperty: {
              expirationTime: 2,
              publish: false,
              action: 'string',
              whitelist: false,
              ttl: 1,
              tags: {
                additionalProperties: 'string'
              }
            },
            expirationTime: 8,
            publish: false,
            action: 'string',
            whitelist: true,
            ttl: 1,
            tags: {
              additionalProperties: 'string'
            }
          },
          observable: {
            miscData: {
              additionalProperties: 'string'
            },
            type: 'string',
            observableType: 'string',
            version: 'string',
            unEncryptedDBId: 'string',
            indicatorCount: 3,
            effectiveProperty: {
              expirationTime: 5,
              publish: true,
              action: 'string',
              whitelist: false,
              ttl: 3,
              tags: {
                additionalProperties: 'string'
              }
            },
            dbid: 'string',
            id: 'string',
            value: 'string',
            inheritedProperty: {
              expirationTime: 4,
              publish: false,
              action: 'string',
              whitelist: false,
              ttl: 5,
              tags: {
                additionalProperties: 'string'
              }
            },
            customProperty: {
              expirationTime: 4,
              publish: false,
              action: 'string',
              whitelist: false,
              ttl: 9,
              tags: {
                additionalProperties: 'string'
              }
            },
            updatedAt: 10
          },
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          value: 'string',
          inheritedProperty: {
            llfeedProperty: {
              expirationTime: 10,
              publish: true,
              action: 'string',
              whitelist: false,
              ttl: 10,
              tags: {
                additionalProperties: 'string'
              }
            },
            expirationTime: 10,
            publish: false,
            action: 'string',
            whitelist: false,
            ttl: 7,
            tags: {
              additionalProperties: 'string'
            }
          },
          customProperty: {
            llfeedProperty: {
              expirationTime: 8,
              publish: true,
              action: 'string',
              whitelist: true,
              ttl: 5,
              tags: {
                additionalProperties: 'string'
              }
            },
            expirationTime: 1,
            publish: true,
            action: 'string',
            whitelist: true,
            ttl: 9,
            tags: {
              additionalProperties: 'string'
            }
          },
          updatedAt: 2
        }
      ],
      feedId: 'string',
      effectiveProperty: {
        llfeedProperty: {
          expirationTime: 9,
          publish: false,
          action: 'string',
          whitelist: false,
          ttl: 2,
          tags: {
            additionalProperties: 'string'
          }
        },
        expirationTime: 2,
        publish: false,
        action: 'string',
        whitelist: false,
        ttl: 8,
        tags: {
          additionalProperties: 'string'
        }
      },
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      noPartialIncidents: 1,
      customProperty: {
        llfeedProperty: {
          expirationTime: 8,
          publish: false,
          action: 'string',
          whitelist: true,
          ttl: 7,
          tags: {
            additionalProperties: 'string'
          }
        },
        expirationTime: 3,
        publish: true,
        action: 'string',
        whitelist: true,
        ttl: 9,
        tags: {
          additionalProperties: 'string'
        }
      },
      updatedAt: 1,
      equation: {
        op: 'string',
        item: {
          op: 'string',
          condition: 'string',
          isRealized: false,
          children: [
            {}
          ],
          type: 'string',
          value: 'string',
          applyCondition: 'string'
        },
        condition: 'string',
        isRealized: true,
        children: [
          {}
        ],
        type: 'string',
        value: 'string',
        applyCondition: 'string'
      },
      containsUnsupported: false,
      version: 'string',
      iteratorId: 'string',
      indicatorVersion: 'string',
      containsInvalid: false,
      name: 'string',
      sourceName: 'string',
      inheritedProperty: {
        llfeedProperty: {
          expirationTime: 7,
          publish: true,
          action: 'string',
          whitelist: true,
          ttl: 9,
          tags: {
            additionalProperties: 'string'
          }
        },
        expirationTime: 10,
        publish: false,
        action: 'string',
        whitelist: true,
        ttl: 4,
        tags: {
          additionalProperties: 'string'
        }
      },
      fileId: 'string'
    };
    describe('#updateRESTIndicator - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateRESTIndicator(intelligenceObjectId, intelligenceDomainUUID, intelligenceUpdateRESTIndicatorBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Intelligence', 'updateRESTIndicator', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRESTIndicator - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRESTIndicator(intelligenceObjectId, intelligenceDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.inheritedProperty);
                assert.equal('object', typeof data.response.effectiveProperty);
                assert.equal('object', typeof data.response.customProperty);
                assert.equal('feedUUID', data.response.feedId);
                assert.equal('object', typeof data.response.equation);
                assert.equal(1499842559, data.response.updatedAt);
                assert.equal('Test Flat File IPV4', data.response.sourceName);
                assert.equal(false, data.response.containsUnsupported);
                assert.equal(false, data.response.containsInvalid);
                assert.equal(true, Array.isArray(data.response.observables));
                assert.equal('1.0.0', data.response.indicatorVersion);
                assert.equal(0, data.response.noRealizedIncidents);
                assert.equal(0, data.response.noPartialIncidents);
                assert.equal('object', typeof data.response.links);
                assert.equal('indicatorUUID', data.response.id);
                assert.equal('indicator', data.response.type);
                assert.equal('Sample Indicator', data.response.name);
                assert.equal('1.0.0', data.response.version);
                assert.equal('Indicator description', data.response.description);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Intelligence', 'getRESTIndicator', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRESTObservable - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllRESTObservable(intelligenceDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Intelligence', 'getAllRESTObservable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const intelligenceUpdateRESTObservableBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: true
        },
        timestamp: 5
      },
      description: 'string',
      observableType: 'string',
      type: 'string',
      version: 'string',
      indicatorCount: 5,
      effectiveProperty: {
        llfeedProperty: {
          expirationTime: 9,
          publish: false,
          action: 'string',
          whitelist: true,
          ttl: 10,
          tags: {
            additionalProperties: 'string'
          }
        },
        expirationTime: 6,
        publish: true,
        action: 'string',
        whitelist: false,
        ttl: 1,
        tags: {
          additionalProperties: 'string'
        }
      },
      observable: {
        miscData: {
          additionalProperties: 'string'
        },
        type: 'string',
        observableType: 'string',
        version: 'string',
        unEncryptedDBId: 'string',
        indicatorCount: 5,
        effectiveProperty: {
          expirationTime: 3,
          publish: false,
          action: 'string',
          whitelist: false,
          ttl: 1,
          tags: {
            additionalProperties: 'string'
          }
        },
        dbid: 'string',
        id: 'string',
        value: 'string',
        inheritedProperty: {
          expirationTime: 9,
          publish: false,
          action: 'string',
          whitelist: true,
          ttl: 2,
          tags: {
            additionalProperties: 'string'
          }
        },
        customProperty: {
          expirationTime: 2,
          publish: true,
          action: 'string',
          whitelist: true,
          ttl: 6,
          tags: {
            additionalProperties: 'string'
          }
        },
        updatedAt: 3
      },
      name: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      value: 'string',
      inheritedProperty: {
        llfeedProperty: {
          expirationTime: 1,
          publish: false,
          action: 'string',
          whitelist: true,
          ttl: 7,
          tags: {
            additionalProperties: 'string'
          }
        },
        expirationTime: 10,
        publish: false,
        action: 'string',
        whitelist: false,
        ttl: 10,
        tags: {
          additionalProperties: 'string'
        }
      },
      customProperty: {
        llfeedProperty: {
          expirationTime: 8,
          publish: false,
          action: 'string',
          whitelist: false,
          ttl: 8,
          tags: {
            additionalProperties: 'string'
          }
        },
        expirationTime: 7,
        publish: false,
        action: 'string',
        whitelist: false,
        ttl: 4,
        tags: {
          additionalProperties: 'string'
        }
      },
      updatedAt: 4
    };
    describe('#updateRESTObservable - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateRESTObservable(intelligenceObjectId, intelligenceDomainUUID, intelligenceUpdateRESTObservableBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Intelligence', 'updateRESTObservable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRESTObservable - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRESTObservable(intelligenceObjectId, intelligenceDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.inheritedProperty);
                assert.equal('IPV_4_ADDR', data.response.observableType);
                assert.equal('object', typeof data.response.effectiveProperty);
                assert.equal(1, data.response.indicatorCount);
                assert.equal(1486153252, data.response.updatedAt);
                assert.equal('ipAddressValue', data.response.value);
                assert.equal('object', typeof data.response.links);
                assert.equal('observableUUID', data.response.id);
                assert.equal('observable', data.response.type);
                assert.equal('Observable name', data.response.name);
                assert.equal('1.0.0', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Intelligence', 'getRESTObservable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const intelligenceUpdateRESTSettingsBodyParam = {
      settings: {
        additionalProperties: {}
      },
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: true
        },
        timestamp: 1
      },
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      type: 'string',
      version: 'string'
    };
    describe('#updateRESTSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateRESTSettings(intelligenceObjectId, intelligenceDomainUUID, intelligenceUpdateRESTSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Intelligence', 'updateRESTSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRESTSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRESTSettings(intelligenceObjectId, intelligenceDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('publish_observables', data.response.id);
                assert.equal('settings', data.response.type);
                assert.equal('1.0.0', data.response.version);
                assert.equal('object', typeof data.response.settings);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Intelligence', 'getRESTSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRESTTidSource - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllRESTTidSource(intelligenceDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Intelligence', 'getAllRESTTidSource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const intelligenceUpdateRESTTidSourceBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: true
        },
        timestamp: 10
      },
      caCert: 'string',
      feedContent: 'string',
      description: 'string',
      invalidObservables: 9,
      feedStatus: 'string',
      type: 'string',
      downloadOn: true,
      nextRun: 9,
      consumedIndicators: 5,
      totalObservables: 7,
      totalIndicators: 4,
      totalUnsupportedObservables: 9,
      property: {
        llfeedProperty: {
          expirationTime: 1,
          publish: true,
          action: 'string',
          whitelist: false,
          ttl: 1,
          tags: {
            additionalProperties: 'string'
          }
        },
        expirationTime: 8,
        publish: true,
        action: 'string',
        whitelist: true,
        ttl: 9,
        tags: {
          additionalProperties: 'string'
        }
      },
      checksum: 'string',
      llfeedConfig: {
        safeToUpdate: true,
        feedContent: 'string',
        updatedObservables: 9,
        safeToDelete: true,
        feedStatus: 'string',
        type: 'string',
        downloadOn: true,
        nextRun: 2,
        password: 'string',
        totalUnsupportedObservables: 4,
        property: {
          expirationTime: 8,
          publish: false,
          action: 'string',
          whitelist: true,
          ttl: 8,
          tags: {
            additionalProperties: 'string'
          }
        },
        id: 'string',
        clientPrivateKey: 'string',
        totalUpdatedObservables: 2,
        clientCert: 'string',
        subscribedCollections: [
          {
            address: 'string',
            pollIntervalInMinutes: 4,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          }
        ],
        discardedObservables: 6,
        finishTime: 6,
        availableCollections: [
          {
            address: 'string',
            pollIntervalInMinutes: 1,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          }
        ],
        lastRun: 4,
        refreshSec: 1,
        recurring: true,
        refreshMin: 7,
        consumedObservables: 10,
        params: {
          additionalProperties: 'string'
        },
        inRunningState: false,
        version: 'string',
        totalUpdatedIndicators: 5,
        discoveryInfo: [
          {
            address: 'string',
            pollIntervalInMinutes: 2,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          }
        ],
        statusMsg: {
          additionalProperties: {
            additionalProperties: 'string'
          }
        },
        passwd: 'string',
        discardedIndicators: 3,
        consumedUnsupportedObservables: 5,
        name: 'string',
        totalDiscardedObservables: 2,
        caCert: 'string',
        description: 'string',
        consumedIndicators: 10,
        totalIndicators: 9,
        totalObservables: 3,
        checksum: 'string',
        delivery: 'string',
        proxyURL: 'string',
        runNow: true,
        updatedIndicators: 10,
        totalConsumedIndicators: 6,
        refresh: 7,
        totalDiscardedIndicators: 9,
        uri: 'string',
        passwdAsIs: 'string',
        startHour: 2,
        feedType: 'string',
        totalConsumedObservables: 4,
        username: 'string'
      },
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      clientPrivateKey: 'string',
      subscribedCollections: [
        {
          collectionSubType: 'string',
          collectionAddress: 'string',
          collectionContentBinding: 'string',
          collectionPollIntervalInMinutes: 6,
          collectionDescription: 'string',
          llcollectionTopic: {
            address: 'string',
            pollIntervalInMinutes: 4,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          },
          type: 'string',
          collectionProtocolBinding: 'string',
          value: 'string',
          collectionMessageBinding: 'string',
          collectionName: 'string'
        }
      ],
      delivery: 'string',
      clientCert: 'string',
      finishTime: 7,
      availableCollections: [
        {
          collectionSubType: 'string',
          collectionAddress: 'string',
          collectionContentBinding: 'string',
          collectionPollIntervalInMinutes: 8,
          collectionDescription: 'string',
          llcollectionTopic: {
            address: 'string',
            pollIntervalInMinutes: 4,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          },
          type: 'string',
          collectionProtocolBinding: 'string',
          value: 'string',
          collectionMessageBinding: 'string',
          collectionName: 'string'
        }
      ],
      lastRun: 5,
      runNow: false,
      consumedObservables: 6,
      totalDiscardedIndicators: 10,
      refresh: 8,
      params: {
        additionalProperties: 'string'
      },
      uri: 'string',
      feedConfig: {
        safeToUpdate: true,
        feedContent: 'string',
        updatedObservables: 9,
        safeToDelete: false,
        feedStatus: 'string',
        type: 'string',
        downloadOn: false,
        nextRun: 5,
        password: 'string',
        totalUnsupportedObservables: 1,
        property: {
          expirationTime: 6,
          publish: false,
          action: 'string',
          whitelist: true,
          ttl: 8,
          tags: {
            additionalProperties: 'string'
          }
        },
        id: 'string',
        clientPrivateKey: 'string',
        totalUpdatedObservables: 5,
        clientCert: 'string',
        subscribedCollections: [
          {
            address: 'string',
            pollIntervalInMinutes: 3,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          }
        ],
        discardedObservables: 2,
        finishTime: 1,
        availableCollections: [
          {
            address: 'string',
            pollIntervalInMinutes: 4,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          }
        ],
        lastRun: 4,
        refreshSec: 9,
        recurring: false,
        refreshMin: 7,
        consumedObservables: 4,
        params: {
          additionalProperties: 'string'
        },
        inRunningState: false,
        version: 'string',
        totalUpdatedIndicators: 8,
        discoveryInfo: [
          {
            address: 'string',
            pollIntervalInMinutes: 9,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          }
        ],
        statusMsg: {
          additionalProperties: {
            additionalProperties: 'string'
          }
        },
        passwd: 'string',
        discardedIndicators: 8,
        consumedUnsupportedObservables: 10,
        name: 'string',
        totalDiscardedObservables: 9,
        caCert: 'string',
        description: 'string',
        consumedIndicators: 8,
        totalIndicators: 10,
        totalObservables: 3,
        checksum: 'string',
        delivery: 'string',
        proxyURL: 'string',
        runNow: true,
        updatedIndicators: 8,
        totalConsumedIndicators: 7,
        refresh: 4,
        totalDiscardedIndicators: 9,
        uri: 'string',
        passwdAsIs: 'string',
        startHour: 10,
        feedType: 'string',
        totalConsumedObservables: 10,
        username: 'string'
      },
      version: 'string',
      discoveryInfo: [
        {
          collectionSubType: 'string',
          collectionAddress: 'string',
          collectionContentBinding: 'string',
          collectionPollIntervalInMinutes: 3,
          collectionDescription: 'string',
          llcollectionTopic: {
            address: 'string',
            pollIntervalInMinutes: 10,
            name: 'string',
            contentBindings: 'string',
            description: 'string',
            subType: 'string',
            type: 'string',
            messageBinding: 'string',
            protocolBinding: 'string',
            hashMap: {
              additionalProperties: 'string'
            }
          },
          type: 'string',
          collectionProtocolBinding: 'string',
          value: 'string',
          collectionMessageBinding: 'string',
          collectionName: 'string'
        }
      ],
      totalInvalidObservables: 9,
      statusMsg: {
        additionalProperties: {
          additionalProperties: 'string'
        }
      },
      passwd: 'string',
      startHour: 9,
      consumedUnsupportedObservables: 1,
      discardedIndicators: 1,
      feedType: 'string',
      name: 'string',
      username: 'string'
    };
    describe('#updateRESTTidSource - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateRESTTidSource(intelligenceObjectId, intelligenceDomainUUID, intelligenceUpdateRESTTidSourceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Intelligence', 'updateRESTTidSource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRESTTidSource - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRESTTidSource(intelligenceObjectId, intelligenceDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('http://somehost/feeds/domain.txt', data.response.uri);
                assert.equal('object', typeof data.response.params);
                assert.equal(1499922000, data.response.nextRun);
                assert.equal(0, data.response.consumedUnsupportedObservables);
                assert.equal('6A330EFFD42314B74C030C0038BAB3352F70CC5344D6CE24774BD04EFDEDB7BD', data.response.checksum);
                assert.equal(1499836832, data.response.lastRun);
                assert.equal(0, data.response.totalUnsupportedObservables);
                assert.equal(0, data.response.totalInvalidObservables);
                assert.equal(true, data.response.downloadOn);
                assert.equal(false, data.response.runNow);
                assert.equal('parsing', data.response.feedStatus);
                assert.equal(0, data.response.consumedIndicators);
                assert.equal(0, data.response.totalIndicators);
                assert.equal(0, data.response.discardedIndicators);
                assert.equal(0, data.response.totalDiscardedIndicators);
                assert.equal(0, data.response.totalObservables);
                assert.equal(0, data.response.invalidObservables);
                assert.equal(501, data.response.consumedObservables);
                assert.equal('flatfile', data.response.feedType);
                assert.equal('DomainNameObjectType', data.response.feedContent);
                assert.equal('url', data.response.delivery);
                assert.equal(1440, data.response.refresh);
                assert.equal('object', typeof data.response.property);
                assert.equal('sourceUUID', data.response.id);
                assert.equal('source', data.response.type);
                assert.equal('Test URL Source', data.response.name);
                assert.equal('Test URL Source', data.response.description);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Intelligence', 'getRESTTidSource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const auditDomainUUID = 'fakedata';
    describe('#getAllAuditModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllAuditModel(auditDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Audit', 'getAllAuditModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const auditObjectId = 'fakedata';
    describe('#getAuditModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAuditModel(auditObjectId, auditDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('string', data.response.subSystem);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.source);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.message);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.domain);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.startTime);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.endTime);
                assert.equal('string', data.response.id);
                assert.equal(10, data.response.time);
                assert.equal('string', data.response.username);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Audit', 'getAuditModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceGroupsDomainUUID = 'fakedata';
    let deviceGroupsObjectId = 'fakedata';
    const deviceGroupsCreateDeviceGroupBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: true
        },
        timestamp: 4
      },
      members: [
        {
          metadata: {
            lastUser: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            domain: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            readOnly: {
              reason: {},
              state: true
            },
            timestamp: 7
          },
          modelId: 'string',
          description: 'string',
          modelType: 'string',
          type: 'string',
          version: 'string',
          sw_version: 'string',
          healthStatus: 'string',
          healthPolicy: {
            name: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string'
          },
          name: 'string',
          model: 'string',
          modelNumber: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          accessPolicy: {
            metadata: {
              lastUser: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              domain: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              readOnly: {
                reason: {},
                state: true
              },
              timestamp: 8
            },
            name: 'string',
            description: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string',
            version: 'string'
          },
          id: 'string'
        }
      ],
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      type: 'string',
      version: 'string'
    };
    describe('#createDeviceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDeviceGroup(deviceGroupsDomainUUID, deviceGroupsCreateDeviceGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, Array.isArray(data.response.members));
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              deviceGroupsObjectId = data.response.id;
              saveMockData('DeviceGroups', 'createDeviceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllDeviceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllDeviceGroup(deviceGroupsDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'getAllDeviceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceGroupsUpdateDeviceGroupBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: false
        },
        timestamp: 3
      },
      members: [
        {
          metadata: {
            lastUser: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            domain: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            readOnly: {
              reason: {},
              state: false
            },
            timestamp: 2
          },
          modelId: 'string',
          description: 'string',
          modelType: 'string',
          type: 'string',
          version: 'string',
          sw_version: 'string',
          healthStatus: 'string',
          healthPolicy: {
            name: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string'
          },
          name: 'string',
          model: 'string',
          modelNumber: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          accessPolicy: {
            metadata: {
              lastUser: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              domain: {
                name: 'string',
                links: {},
                id: 'string',
                type: 'string'
              },
              readOnly: {
                reason: {},
                state: false
              },
              timestamp: 8
            },
            name: 'string',
            description: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string',
            version: 'string'
          },
          id: 'string'
        }
      ],
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      type: 'string',
      version: 'string'
    };
    describe('#updateDeviceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceGroup(deviceGroupsObjectId, deviceGroupsDomainUUID, deviceGroupsUpdateDeviceGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'updateDeviceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroup(deviceGroupsObjectId, deviceGroupsDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('deviceGroupUUID', data.response.id);
                assert.equal('DeviceGroup', data.response.type);
                assert.equal('zoom2', data.response.name);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.members));
                assert.equal('object', typeof data.response.metadata);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'getDeviceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const integrationDomainUUID = 'fakedata';
    const integrationCreateExternalLookupBodyParam = {
      template: 'string',
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: false
        },
        timestamp: 6
      },
      intValue: 10,
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      type: 'string',
      version: 'string',
      longValue: 5,
      enabled: false,
      network: {
        metadata: {
          lastUser: {
            name: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: 'SYSTEM',
            state: false
          },
          timestamp: 1
        },
        overridable: false,
        name: 'string',
        description: 'string',
        links: {
          parent: 'string',
          self: 'string'
        },
        overrides: {
          parent: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          target: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          }
        },
        id: 'string',
        type: 'string',
        value: 'string',
        version: 'string',
        overrideTargetId: 'string'
      }
    };
    describe('#createExternalLookup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createExternalLookup(integrationDomainUUID, integrationCreateExternalLookupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.template);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(6, data.response.intValue);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal(9, data.response.longValue);
                assert.equal(false, data.response.enabled);
                assert.equal('object', typeof data.response.network);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Integration', 'createExternalLookup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const integrationCreatePacketAnalyzerDeviceBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: true
        },
        timestamp: 6
      },
      intValue: 6,
      description: 'string',
      type: 'string',
      version: 'string',
      longValue: 9,
      network: {
        metadata: {
          lastUser: {
            name: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: 'DOMAIN',
            state: true
          },
          timestamp: 4
        },
        overridable: false,
        name: 'string',
        description: 'string',
        links: {
          parent: 'string',
          self: 'string'
        },
        overrides: {
          parent: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          target: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          }
        },
        id: 'string',
        type: 'string',
        value: 'string',
        version: 'string',
        overrideTargetId: 'string'
      },
      password: 'string',
      port: 7,
      cacert: 'string',
      name: 'string',
      host: 'string',
      insecure: 9,
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      captureSession: 'string',
      username: 'string'
    };
    describe('#createPacketAnalyzerDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPacketAnalyzerDevice(integrationDomainUUID, integrationCreatePacketAnalyzerDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(10, data.response.intValue);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal(1, data.response.longValue);
                assert.equal('object', typeof data.response.network);
                assert.equal('string', data.response.password);
                assert.equal(10, data.response.port);
                assert.equal('string', data.response.cacert);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.host);
                assert.equal(5, data.response.insecure);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.captureSession);
                assert.equal('string', data.response.username);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Integration', 'createPacketAnalyzerDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllCloudEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllCloudEvents(integrationDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Integration', 'getAllCloudEvents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const integrationObjectId = 'fakedata';
    const integrationUpdateCloudEventsBodyParam = {
      sendIntrusionEvents: false,
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: false
        },
        timestamp: 7
      },
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      type: 'string',
      version: 'string'
    };
    describe('#updateCloudEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateCloudEvents(integrationObjectId, integrationDomainUUID, integrationUpdateCloudEventsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Integration', 'updateCloudEvents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCloudEvents(integrationObjectId, integrationDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('CloudEventsConfigsUUID', data.response.id);
                assert.equal('CloudEventsConfig', data.response.type);
                assert.equal(true, data.response.sendIntrusionEvents);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Integration', 'getCloudEvents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllExternalLookup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllExternalLookup(integrationDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Integration', 'getAllExternalLookup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const integrationUpdateExternalLookupBodyParam = {
      template: 'string',
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: false
        },
        timestamp: 7
      },
      intValue: 1,
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      type: 'string',
      version: 'string',
      longValue: 7,
      enabled: false,
      network: {
        metadata: {
          lastUser: {
            name: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: 'RBAC',
            state: false
          },
          timestamp: 10
        },
        overridable: false,
        name: 'string',
        description: 'string',
        links: {
          parent: 'string',
          self: 'string'
        },
        overrides: {
          parent: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          target: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          }
        },
        id: 'string',
        type: 'string',
        value: 'string',
        version: 'string',
        overrideTargetId: 'string'
      }
    };
    describe('#updateExternalLookup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateExternalLookup(integrationObjectId, integrationDomainUUID, integrationUpdateExternalLookupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Integration', 'updateExternalLookup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExternalLookup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExternalLookup(integrationObjectId, integrationDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('ExternalLookupUUID', data.response.id);
                assert.equal('ExternalLookup', data.response.type);
                assert.equal('test1055', data.response.name);
                assert.equal('object', typeof data.response.links);
                assert.equal('http://www.example.com/{ip}', data.response.template);
                assert.equal(true, data.response.enabled);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Integration', 'getExternalLookup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPacketAnalyzerDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllPacketAnalyzerDevice(integrationDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Integration', 'getAllPacketAnalyzerDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const integrationUpdatePacketAnalyzerDeviceBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: true
        },
        timestamp: 7
      },
      intValue: 9,
      description: 'string',
      type: 'string',
      version: 'string',
      longValue: 7,
      network: {
        metadata: {
          lastUser: {
            name: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {
              parent: 'string',
              self: 'string'
            },
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: 'DOMAIN',
            state: true
          },
          timestamp: 9
        },
        overridable: false,
        name: 'string',
        description: 'string',
        links: {
          parent: 'string',
          self: 'string'
        },
        overrides: {
          parent: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          target: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          }
        },
        id: 'string',
        type: 'string',
        value: 'string',
        version: 'string',
        overrideTargetId: 'string'
      },
      password: 'string',
      port: 9,
      cacert: 'string',
      name: 'string',
      host: 'string',
      insecure: 2,
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      captureSession: 'string',
      username: 'string'
    };
    describe('#updatePacketAnalyzerDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePacketAnalyzerDevice(integrationObjectId, integrationDomainUUID, integrationUpdatePacketAnalyzerDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Integration', 'updatePacketAnalyzerDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPacketAnalyzerDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPacketAnalyzerDevice(integrationObjectId, integrationDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('admin', data.response.username);
                assert.equal('PacketAnalyzerDeviceUUID', data.response.id);
                assert.equal('PacketAnalyzerDevice', data.response.type);
                assert.equal('test1055', data.response.name);
                assert.equal('object', typeof data.response.links);
                assert.equal('PacketAnalyzerHost', data.response.host);
                assert.equal(443, data.response.port);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Integration', 'getPacketAnalyzerDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statusObjectId = 'fakedata';
    const statusDomainUUID = 'fakedata';
    describe('#getTaskStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTaskStatus(statusObjectId, statusDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('DEVICE_REGISTRATION', data.response.taskType);
                assert.equal(true, Array.isArray(data.response.subTasks));
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.message);
                assert.equal('string', data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Status', 'getTaskStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceClustersDomainUUID = 'fakedata';
    describe('#getAllFTDClusterDeviceContainer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllFTDClusterDeviceContainer(deviceClustersDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceClusters', 'getAllFTDClusterDeviceContainer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceClustersObjectId = 'fakedata';
    describe('#getFTDClusterDeviceContainer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFTDClusterDeviceContainer(deviceClustersObjectId, deviceClustersDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.masterDevice);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('string', data.response.modelId);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.slaveDevices));
                assert.equal('string', data.response.modelType);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.sw_version);
                assert.equal('string', data.response.healthStatus);
                assert.equal('object', typeof data.response.healthPolicy);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.model);
                assert.equal('string', data.response.modelNumber);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.accessPolicy);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceClusters', 'getFTDClusterDeviceContainer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllServerVersion - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllServerVersion(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemInformation', 'getAllServerVersion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemInformationObjectId = 'fakedata';
    describe('#getServerVersion - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServerVersion(systemInformationObjectId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('ServerVersion', data.response.type);
                assert.equal('System Version', data.response.description);
                assert.equal('Version Number', data.response.serverversion);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemInformation', 'getServerVersion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyDomainUUID = 'fakedata';
    let policyObjectId = 'fakedata';
    const policyCreateAccessPolicyBodyParam = {
      defaultAction: {}
    };
    describe('#createAccessPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createAccessPolicy(policyDomainUUID, policyCreateAccessPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('object', typeof data.response.defaultAction);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.rules);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              policyObjectId = data.response.id;
              saveMockData('Policy', 'createAccessPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyBulk = 'fakedata';
    const policyContainerUUID = 'fakedata';
    const policyCreateAccessRuleBodyParam = {
      action: 'BLOCK_RESET_INTERACTIVE'
    };
    describe('#createAccessRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createAccessRule(policyBulk, null, null, null, null, policyContainerUUID, policyDomainUUID, policyCreateAccessRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('object', typeof data.response.snmpConfig);
                assert.equal('object', typeof data.response.sourceNetworks);
                assert.equal('DEBUG', data.response.syslogSeverity);
                assert.equal('object', typeof data.response.sourceZones);
                assert.equal('object', typeof data.response.destinationZones);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.originalSourceNetworks);
                assert.equal('string', data.response.type);
                assert.equal(false, data.response.enableSyslog);
                assert.equal('object', typeof data.response.safeSearch);
                assert.equal(false, data.response.enabled);
                assert.equal('object', typeof data.response.syslogConfig);
                assert.equal('object', typeof data.response.urls);
                assert.equal(true, Array.isArray(data.response.endPointDeviceTypes));
                assert.equal('object', typeof data.response.destinationNetworks);
                assert.equal('object', typeof data.response.youTube);
                assert.equal('BLOCK', data.response.action);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.networkAccessDeviceIPs);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.sourceSecurityGroupTags);
                assert.equal(true, data.response.logEnd);
                assert.equal(true, data.response.logBegin);
                assert.equal(true, data.response.sendEventsToFMC);
                assert.equal('object', typeof data.response.destinationPorts);
                assert.equal('object', typeof data.response.sourcePorts);
                assert.equal('object', typeof data.response.ipsPolicy);
                assert.equal('string', data.response.version);
                assert.equal('object', typeof data.response.variableSet);
                assert.equal('object', typeof data.response.users);
                assert.equal(false, data.response.logFiles);
                assert.equal(true, Array.isArray(data.response.commentHistoryList));
                assert.equal('object', typeof data.response.filePolicy);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.vlanTags);
                assert.equal('object', typeof data.response.applications);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'createAccessRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyCreateFTDNatPolicyBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'DOMAIN',
          state: true
        },
        timestamp: 6
      },
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      rules: {},
      id: 'string',
      type: 'string',
      version: 'string'
    };
    describe('#createFTDNatPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createFTDNatPolicy(policyDomainUUID, policyCreateFTDNatPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.rules);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'createFTDNatPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyCreateFTDAutoNatRuleBodyParam = {
      natType: 'DYNAMIC',
      translatedNetwork: {},
      originalNetwork: {}
    };
    describe('#createFTDAutoNatRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createFTDAutoNatRule(policyBulk, null, policyContainerUUID, policyDomainUUID, policyCreateFTDAutoNatRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, data.response.fallThrough);
                assert.equal('DYNAMIC', data.response.natType);
                assert.equal('UDP', data.response.serviceProtocol);
                assert.equal(true, data.response.dns);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.section);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.patOptions);
                assert.equal('string', data.response.version);
                assert.equal(true, data.response.noProxyArp);
                assert.equal('object', typeof data.response.translatedNetwork);
                assert.equal('object', typeof data.response.sourceInterface);
                assert.equal(false, data.response.interfaceInTranslatedNetwork);
                assert.equal(true, data.response.routeLookup);
                assert.equal('string', data.response.name);
                assert.equal(2, data.response.translatedPort);
                assert.equal(false, data.response.netToNet);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.destinationInterface);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.originalNetwork);
                assert.equal(true, data.response.interfaceIpv6);
                assert.equal(9, data.response.originalPort);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'createFTDAutoNatRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyCreateFTDManualNatRuleBodyParam = {
      natType: 'STATIC',
      originalSource: {}
    };
    describe('#createFTDManualNatRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createFTDManualNatRule(policyBulk, null, null, policyContainerUUID, policyDomainUUID, policyCreateFTDManualNatRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('object', typeof data.response.translatedDestinationPort);
                assert.equal(false, data.response.fallThrough);
                assert.equal('DYNAMIC', data.response.natType);
                assert.equal(9, data.response.targetIndex);
                assert.equal('object', typeof data.response.translatedSourcePort);
                assert.equal('object', typeof data.response.translatedDestination);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.section);
                assert.equal('string', data.response.type);
                assert.equal(false, data.response.enabled);
                assert.equal('object', typeof data.response.originalSourcePort);
                assert.equal('object', typeof data.response.sourceInterface);
                assert.equal(false, data.response.interfaceInOriginalDestination);
                assert.equal(false, data.response.netToNet);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.originalSource);
                assert.equal(true, data.response.dns);
                assert.equal('object', typeof data.response.patOptions);
                assert.equal('string', data.response.version);
                assert.equal(true, data.response.noProxyArp);
                assert.equal('object', typeof data.response.translatedSource);
                assert.equal(false, data.response.interfaceInTranslatedSource);
                assert.equal(true, data.response.routeLookup);
                assert.equal('object', typeof data.response.originalDestination);
                assert.equal(false, data.response.unidirectional);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.destinationInterface);
                assert.equal('object', typeof data.response.originalDestinationPort);
                assert.equal(true, data.response.interfaceIpv6);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'createFTDManualNatRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyCreateFTDS2SVpnModelBodyParam = {
      endpoints: {},
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: true
        },
        timestamp: 3
      },
      description: 'string',
      type: 'string',
      ipsecSettings: {
        name: 'string',
        links: {},
        id: 'string',
        type: 'string'
      },
      version: 'string',
      ikeV2Enabled: true,
      ikeV1Enabled: true,
      topologyType: 'string',
      advancedSettings: {
        name: 'string',
        links: {},
        id: 'string',
        type: 'string'
      },
      name: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      ikeSettings: {
        name: 'string',
        links: {},
        id: 'string',
        type: 'string'
      },
      id: 'string'
    };
    describe('#createFTDS2SVpnModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createFTDS2SVpnModel(policyDomainUUID, policyCreateFTDS2SVpnModelBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.endpoints);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.ipsecSettings);
                assert.equal('string', data.response.version);
                assert.equal(true, data.response.ikeV2Enabled);
                assert.equal(true, data.response.ikeV1Enabled);
                assert.equal('string', data.response.topologyType);
                assert.equal('object', typeof data.response.advancedSettings);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.ikeSettings);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'createFTDS2SVpnModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyCreateVpnEndpointBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'SYSTEM',
          state: false
        },
        timestamp: 6
      },
      extranet: true,
      description: 'string',
      type: 'string',
      interface: {
        name: 'string',
        links: {},
        id: 'string',
        type: 'string'
      },
      connectionType: 'string',
      version: 'string',
      extranetInfo: {
        isDynamicIP: false,
        name: 'string',
        ipAddress: 'string'
      },
      protectedNetworks: {
        acl: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        networks: [
          {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          }
        ]
      },
      nattedInterfaceAddress: 'string',
      name: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      peerType: 'string',
      device: {
        name: 'string',
        links: {},
        id: 'string',
        type: 'string'
      },
      ipv6InterfaceAddress: 'string'
    };
    describe('#createVpnEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createVpnEndpoint(policyContainerUUID, policyDomainUUID, policyCreateVpnEndpointBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, data.response.extranet);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.interface);
                assert.equal('string', data.response.connectionType);
                assert.equal('string', data.response.version);
                assert.equal('object', typeof data.response.extranetInfo);
                assert.equal('object', typeof data.response.protectedNetworks);
                assert.equal('string', data.response.nattedInterfaceAddress);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.peerType);
                assert.equal('object', typeof data.response.device);
                assert.equal('string', data.response.ipv6InterfaceAddress);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'createVpnEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllAccessPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllAccessPolicy(policyDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getAllAccessPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyUpdateAccessRule1BodyParam = {
      action: 'ALLOW'
    };
    describe('#updateAccessRule1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAccessRule1(policyBulk, policyContainerUUID, policyDomainUUID, policyUpdateAccessRule1BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'updateAccessRule1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllAccessRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllAccessRule(policyContainerUUID, policyDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getAllAccessRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyUpdateAccessRuleBodyParam = {
      action: 'BLOCK_RESET'
    };
    describe('#updateAccessRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAccessRule(policyObjectId, policyContainerUUID, policyDomainUUID, policyUpdateAccessRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'updateAccessRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccessRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccessRule(policyObjectId, policyContainerUUID, policyDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('object', typeof data.response.links);
                assert.equal('ALLOW', data.response.action);
                assert.equal(true, data.response.enabled);
                assert.equal('AccessRule', data.response.type);
                assert.equal('Rule2', data.response.name);
                assert.equal(false, data.response.sendEventsToFMC);
                assert.equal('object', typeof data.response.ipsPolicy);
                assert.equal('object', typeof data.response.originalSourceNetworks);
                assert.equal('accessruleUUID', data.response.id);
                assert.equal('object', typeof data.response.vlanTags);
                assert.equal('object', typeof data.response.urls);
                assert.equal('object', typeof data.response.sourceZones);
                assert.equal('object', typeof data.response.destinationZones);
                assert.equal(false, data.response.logFiles);
                assert.equal(false, data.response.logBegin);
                assert.equal(false, data.response.logEnd);
                assert.equal('object', typeof data.response.variableSet);
                assert.equal('object', typeof data.response.sourcePorts);
                assert.equal('object', typeof data.response.destinationPorts);
                assert.equal('object', typeof data.response.applications);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getAccessRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllDefaultAction - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllDefaultAction(policyContainerUUID, policyDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getAllDefaultAction', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyUpdateDefaultActionBodyParam = {
      action: 'NETWORK_DISCOVERY'
    };
    describe('#updateDefaultAction - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDefaultAction(policyObjectId, policyContainerUUID, policyDomainUUID, policyUpdateDefaultActionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'updateDefaultAction', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDefaultAction - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDefaultAction(policyObjectId, policyContainerUUID, policyDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.logEnd);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, data.response.logBegin);
                assert.equal('object', typeof data.response.snmpConfig);
                assert.equal('object', typeof data.response.intrusionPolicy);
                assert.equal(true, data.response.sendEventsToFMC);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.variableSet);
                assert.equal('string', data.response.version);
                assert.equal('object', typeof data.response.syslogConfig);
                assert.equal('string', data.response.name);
                assert.equal('BLOCK', data.response.action);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getDefaultAction', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllAccessPolicyLoggingSettingModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllAccessPolicyLoggingSettingModel(policyContainerUUID, policyDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getAllAccessPolicyLoggingSettingModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyUpdateAccessPolicyLoggingSettingModelBodyParam = {
      fileAndMalwareSyslogConfig: {
        metadata: {
          lastUser: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: {},
            state: false
          },
          timestamp: 3
        },
        name: 'string',
        description: 'string',
        links: {
          parent: 'string',
          self: 'string'
        },
        id: 'string',
        type: 'string',
        version: 'string'
      },
      syslogConfigFromPlatformSetting: false,
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'DOMAIN',
          state: false
        },
        timestamp: 10
      },
      description: 'string',
      enableFileAndMalwareSyslog: true,
      type: 'string',
      version: 'string',
      syslogConfig: {
        metadata: {
          lastUser: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          domain: {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          },
          readOnly: {
            reason: {},
            state: false
          },
          timestamp: 5
        },
        name: 'string',
        description: 'string',
        links: {
          parent: 'string',
          self: 'string'
        },
        id: 'string',
        type: 'string',
        version: 'string'
      },
      name: 'string',
      fileAndMalwareSyslogSeverity: 'INFO',
      severityForPlatformSettingSyslogConfig: 'WARNING',
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string'
    };
    describe('#updateAccessPolicyLoggingSettingModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAccessPolicyLoggingSettingModel(policyObjectId, policyContainerUUID, policyDomainUUID, policyUpdateAccessPolicyLoggingSettingModelBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'updateAccessPolicyLoggingSettingModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccessPolicyLoggingSettingModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccessPolicyLoggingSettingModel(policyObjectId, policyContainerUUID, policyDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getAccessPolicyLoggingSettingModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyFilter = 'fakedata';
    describe('#updateHitCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateHitCount(policyFilter, policyContainerUUID, policyDomainUUID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowermanagementcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'updateHitCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHitCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHitCount(policyFilter, policyContainerUUID, policyDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getHitCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyUpdateAccessPolicyBodyParam = {
      defaultAction: {}
    };
    describe('#updateAccessPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAccessPolicy(policyObjectId, policyDomainUUID, policyUpdateAccessPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'updateAccessPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccessPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccessPolicy(policyObjectId, policyDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('object', typeof data.response.defaultAction);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.rules);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getAccessPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFilePolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllFilePolicy(policyDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getAllFilePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFilePolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFilePolicy(policyObjectId, policyDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Test-TestDomain', data.response.name);
                assert.equal('File-policy-UUID-1', data.response.id);
                assert.equal('FilePolicy', data.response.type);
                assert.equal('Test-TestDomain', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.metadata);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getFilePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFTDNatPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllFTDNatPolicy(policyDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getAllFTDNatPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFTDAutoNatRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllFTDAutoNatRule(policyContainerUUID, policyDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getAllFTDAutoNatRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyUpdateFTDAutoNatRuleBodyParam = {
      natType: 'STATIC',
      translatedNetwork: {},
      originalNetwork: {}
    };
    describe('#updateFTDAutoNatRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFTDAutoNatRule(policyObjectId, null, policyContainerUUID, policyDomainUUID, policyUpdateFTDAutoNatRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'updateFTDAutoNatRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFTDAutoNatRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFTDAutoNatRule(policyObjectId, null, policyContainerUUID, policyDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('object', typeof data.response.links);
                assert.equal('TCP', data.response.serviceProtocol);
                assert.equal('object', typeof data.response.translatedNetwork);
                assert.equal(1234, data.response.translatedPort);
                assert.equal('object', typeof data.response.originalNetwork);
                assert.equal(345, data.response.originalPort);
                assert.equal('FTDAutoNatRule', data.response.type);
                assert.equal('object', typeof data.response.destinationInterface);
                assert.equal(false, data.response.dns);
                assert.equal(false, data.response.interfaceIpv6);
                assert.equal(false, data.response.noProxyArp);
                assert.equal(false, data.response.netToNet);
                assert.equal('STATIC', data.response.natType);
                assert.equal('object', typeof data.response.sourceInterface);
                assert.equal('autoNatRuleUuid', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getFTDAutoNatRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFTDManualNatRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllFTDManualNatRule(policyContainerUUID, policyDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getAllFTDManualNatRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyUpdateFTDManualNatRuleBodyParam = {
      natType: 'STATIC',
      originalSource: {}
    };
    describe('#updateFTDManualNatRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFTDManualNatRule(policyObjectId, null, null, policyContainerUUID, policyDomainUUID, policyUpdateFTDManualNatRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'updateFTDManualNatRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFTDManualNatRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFTDManualNatRule(policyObjectId, null, policyContainerUUID, policyDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, data.response.unidirectional);
                assert.equal('object', typeof data.response.originalSource);
                assert.equal(false, data.response.interfaceInOriginalDestination);
                assert.equal(true, data.response.interfaceInTranslatedSource);
                assert.equal(true, data.response.enabled);
                assert.equal('FTDManualNatRule', data.response.type);
                assert.equal('object', typeof data.response.destinationInterface);
                assert.equal(false, data.response.dns);
                assert.equal(false, data.response.interfaceIpv6);
                assert.equal(false, data.response.noProxyArp);
                assert.equal(false, data.response.netToNet);
                assert.equal('DYNAMIC', data.response.natType);
                assert.equal('object', typeof data.response.sourceInterface);
                assert.equal('manualNatRuleUuid1', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getFTDManualNatRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFTDNatRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllFTDNatRule(policyContainerUUID, policyDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getAllFTDNatRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFTDNatRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFTDNatRule(policyObjectId, null, policyContainerUUID, policyDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, data.response.fallThrough);
                assert.equal('DYNAMIC', data.response.natType);
                assert.equal(false, data.response.dns);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.section);
                assert.equal('object', typeof data.response.patOptions);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal(false, data.response.noProxyArp);
                assert.equal('object', typeof data.response.sourceInterface);
                assert.equal(true, data.response.routeLookup);
                assert.equal('string', data.response.name);
                assert.equal(false, data.response.netToNet);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.destinationInterface);
                assert.equal('string', data.response.id);
                assert.equal(false, data.response.interfaceIpv6);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getFTDNatRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyUpdateFTDNatPolicyBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'DOMAIN',
          state: true
        },
        timestamp: 1
      },
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      rules: {},
      id: 'string',
      type: 'string',
      version: 'string'
    };
    describe('#updateFTDNatPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFTDNatPolicy(policyObjectId, policyDomainUUID, policyUpdateFTDNatPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'updateFTDNatPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFTDNatPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFTDNatPolicy(policyObjectId, policyDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('FTDNatPolicy', data.response.type);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.rules);
                assert.equal('NatPol', data.response.name);
                assert.equal('nat policy for testing', data.response.description);
                assert.equal('natPolUuid1', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getFTDNatPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFTDS2SVpnModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllFTDS2SVpnModel(policyDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getAllFTDS2SVpnModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllVpnAdvancedSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllVpnAdvancedSettings(policyContainerUUID, policyDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getAllVpnAdvancedSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyUpdateVpnAdvancedSettingsBodyParam = {
      advancedIkeSetting: {
        peerIdentityValidation: 'IF_SUPPORTED_BY_CERT',
        enableNotificationOnTunnelDisconnect: false,
        thresholdToChallengeIncomingCookies: 4,
        ikeKeepaliveSettings: {
          threshold: 9,
          retryInterval: 4,
          ikeKeepalive: 'ENABLED'
        },
        identitySentToPeer: 'HOST_NAME',
        enableAggressiveMode: true,
        cookieChallenge: 'CUSTOM',
        maximumNumberOfSAsAllowed: 4,
        percentageOfSAsAllowedInNegotiation: 8
      },
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: false
        },
        timestamp: 5
      },
      name: 'string',
      advancedTunnelSetting: {
        natKeepaliveMessageTraversal: {
          enabled: true,
          intervalSeconds: 5
        },
        enableSpokeToSpokeConnectivityThroughHub: true,
        certificateMapSettings: {
          useCertMapConfiguredInEndpointToDetermineTunnel: true,
          usePeerIpAddressToDetermineTunnel: false,
          useCertificateOuToDetermineTunnel: true,
          useIkeIdentityOuToDetermineTunnel: true
        },
        bypassAccessControlTrafficForDecryptedTraffic: false
      },
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      advancedIpsecSetting: {
        enableFragmentationBeforeEncryption: true,
        maximumTransmissionUnitAging: {
          resetIntervalMinutes: 1,
          enabled: true
        }
      },
      id: 'string',
      type: 'string',
      version: 'string'
    };
    describe('#updateVpnAdvancedSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateVpnAdvancedSettings(policyObjectId, policyContainerUUID, policyDomainUUID, policyUpdateVpnAdvancedSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'updateVpnAdvancedSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVpnAdvancedSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVpnAdvancedSettings(policyObjectId, policyContainerUUID, policyDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.advancedIkeSetting);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.advancedTunnelSetting);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.advancedIpsecSetting);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getVpnAdvancedSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllVpnEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllVpnEndpoint(policyContainerUUID, policyDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getAllVpnEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyUpdateVpnEndpointBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'SYSTEM',
          state: false
        },
        timestamp: 2
      },
      extranet: true,
      description: 'string',
      type: 'string',
      interface: {
        name: 'string',
        links: {},
        id: 'string',
        type: 'string'
      },
      connectionType: 'string',
      version: 'string',
      extranetInfo: {
        isDynamicIP: true,
        name: 'string',
        ipAddress: 'string'
      },
      protectedNetworks: {
        acl: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        networks: [
          {
            name: 'string',
            links: {},
            id: 'string',
            type: 'string'
          }
        ]
      },
      nattedInterfaceAddress: 'string',
      name: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      peerType: 'string',
      device: {
        name: 'string',
        links: {},
        id: 'string',
        type: 'string'
      },
      ipv6InterfaceAddress: 'string'
    };
    describe('#updateVpnEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateVpnEndpoint(policyObjectId, policyContainerUUID, policyDomainUUID, policyUpdateVpnEndpointBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'updateVpnEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVpnEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVpnEndpoint(policyObjectId, policyContainerUUID, policyDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, data.response.extranet);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.interface);
                assert.equal('string', data.response.connectionType);
                assert.equal('string', data.response.version);
                assert.equal('object', typeof data.response.extranetInfo);
                assert.equal('object', typeof data.response.protectedNetworks);
                assert.equal('string', data.response.nattedInterfaceAddress);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.peerType);
                assert.equal('object', typeof data.response.device);
                assert.equal('string', data.response.ipv6InterfaceAddress);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getVpnEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllVpnIkeSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllVpnIkeSettings(policyContainerUUID, policyDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getAllVpnIkeSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyUpdateVpnIkeSettingsBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'RBAC',
          state: true
        },
        timestamp: 4
      },
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      ikeV1Settings: {
        automaticPreSharedKeyLength: 10,
        certificateAuth: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        authenticationType: 'MANUAL_PRE_SHARED_KEY',
        manualPreSharedKey: 'string',
        policy: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      type: 'string',
      ikeV2Settings: {
        automaticPreSharedKeyLength: 2,
        certificateAuth: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        enforceHexBasedPreSharedKeyOnly: false,
        authenticationType: 'MANUAL_PRE_SHARED_KEY',
        manualPreSharedKey: 'string',
        policy: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      version: 'string'
    };
    describe('#updateVpnIkeSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateVpnIkeSettings(policyObjectId, policyContainerUUID, policyDomainUUID, policyUpdateVpnIkeSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'updateVpnIkeSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVpnIkeSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVpnIkeSettings(policyObjectId, policyContainerUUID, policyDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.ikeV1Settings);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.ikeV2Settings);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getVpnIkeSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllVpnIPSecSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllVpnIPSecSettings(policyContainerUUID, policyDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getAllVpnIPSecSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyUpdateVpnIPSecSettingsBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'SYSTEM',
          state: false
        },
        timestamp: 5
      },
      ikeV1IpsecProposal: [
        {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      ],
      perfectForwardSecrecy: {
        enabled: true,
        modulusGroup: 10
      },
      lifetimeSeconds: 9,
      ikeV2IpsecProposal: [
        {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      ],
      tfcPackets: {
        burstBytes: 4,
        timeoutSeconds: 2,
        enabled: true,
        payloadBytes: 7
      },
      ikeV2Mode: 'TRANSPORT_PREFERRED',
      description: 'string',
      cryptoMapType: 'DYNAMIC',
      enableSaStrengthEnforcement: false,
      type: 'string',
      version: 'string',
      lifetimeKilobytes: 5,
      validateIncomingIcmpErrorMessage: false,
      doNotFragmentPolicy: 'NONE',
      name: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      enableRRI: true
    };
    describe('#updateVpnIPSecSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateVpnIPSecSettings(policyObjectId, policyContainerUUID, policyDomainUUID, policyUpdateVpnIPSecSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'updateVpnIPSecSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVpnIPSecSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVpnIPSecSettings(policyObjectId, policyContainerUUID, policyDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, Array.isArray(data.response.ikeV1IpsecProposal));
                assert.equal('object', typeof data.response.perfectForwardSecrecy);
                assert.equal(3, data.response.lifetimeSeconds);
                assert.equal(true, Array.isArray(data.response.ikeV2IpsecProposal));
                assert.equal('object', typeof data.response.tfcPackets);
                assert.equal('TRANSPORT_REQUIRE', data.response.ikeV2Mode);
                assert.equal('string', data.response.description);
                assert.equal('DYNAMIC', data.response.cryptoMapType);
                assert.equal(true, data.response.enableSaStrengthEnforcement);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal(6, data.response.lifetimeKilobytes);
                assert.equal(false, data.response.validateIncomingIcmpErrorMessage);
                assert.equal('CLEAR', data.response.doNotFragmentPolicy);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal(true, data.response.enableRRI);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getVpnIPSecSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyUpdateFTDS2SVpnModelBodyParam = {
      endpoints: {},
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: true
        },
        timestamp: 2
      },
      description: 'string',
      type: 'string',
      ipsecSettings: {
        name: 'string',
        links: {},
        id: 'string',
        type: 'string'
      },
      version: 'string',
      ikeV2Enabled: false,
      ikeV1Enabled: true,
      topologyType: 'string',
      advancedSettings: {
        name: 'string',
        links: {},
        id: 'string',
        type: 'string'
      },
      name: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      ikeSettings: {
        name: 'string',
        links: {},
        id: 'string',
        type: 'string'
      },
      id: 'string'
    };
    describe('#updateFTDS2SVpnModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFTDS2SVpnModel(policyObjectId, policyDomainUUID, policyUpdateFTDS2SVpnModelBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'updateFTDS2SVpnModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFTDS2SVpnModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFTDS2SVpnModel(policyObjectId, policyDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.endpoints);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.ipsecSettings);
                assert.equal('string', data.response.version);
                assert.equal(false, data.response.ikeV2Enabled);
                assert.equal(false, data.response.ikeV1Enabled);
                assert.equal('string', data.response.topologyType);
                assert.equal('object', typeof data.response.advancedSettings);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.ikeSettings);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getFTDS2SVpnModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllIntrusionPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllIntrusionPolicy(policyDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getAllIntrusionPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntrusionPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIntrusionPolicy(policyObjectId, policyDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Security Over Connectivity', data.response.name);
                assert.equal('intrusionPolicyUUID', data.response.id);
                assert.equal('object', typeof data.response.links);
                assert.equal('IntrusionPolicy', data.response.type);
                assert.equal('object', typeof data.response.metadata);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getIntrusionPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPrefilterPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllPrefilterPolicy(policyDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getAllPrefilterPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePrefilterHitCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePrefilterHitCount(policyFilter, policyContainerUUID, policyDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'updatePrefilterHitCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPrefilterHitCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPrefilterHitCount(policyFilter, policyContainerUUID, policyDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getPrefilterHitCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPrefilterPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPrefilterPolicy(policyObjectId, policyDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('PrefilterPolicy', data.response.type);
                assert.equal('PrefilterPolicy_01', data.response.name);
                assert.equal('String', data.response.id);
                assert.equal('policy to test FMC implementation', data.response.description);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getPrefilterPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSNMPConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllSNMPConfig(policyDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getAllSNMPConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSNMPConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSNMPConfig(policyObjectId, policyDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Test-SNMP-Global-v2', data.response.name);
                assert.equal('SNMP-alert-UUID-1', data.response.id);
                assert.equal('SNMPAlert', data.response.type);
                assert.equal('2.0', data.response.version);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.metadata);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getSNMPConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSyslogConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllSyslogConfig(policyDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getAllSyslogConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSyslogConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSyslogConfig(policyObjectId, policyDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Test-Syslog-Global', data.response.name);
                assert.equal('Syslog-alert-UUID-1', data.response.id);
                assert.equal('SyslogAlert', data.response.type);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.metadata);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getSyslogConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectDomainUUID = 'fakedata';
    const objectCreateDNSServerGroupObjectBodyParam = {
      retries: 8,
      defaultdomain: 'string',
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'RBAC',
          state: true
        },
        timestamp: 5
      },
      dnsservers: [
        {
          'name-server': 'string'
        }
      ],
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      type: 'string',
      version: 'string',
      timeout: 2
    };
    describe('#createDNSServerGroupObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDNSServerGroupObject(objectDomainUUID, objectCreateDNSServerGroupObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.retries);
                assert.equal('string', data.response.defaultdomain);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, Array.isArray(data.response.dnsservers));
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal(2, data.response.timeout);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'createDNSServerGroupObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectCreateFQDNObjectBodyParam = {
      dnsResolution: 'IPV4_ONLY'
    };
    describe('#createFQDNObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createFQDNObject(null, objectDomainUUID, objectCreateFQDNObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('IPV4_AND_IPV6', data.response.dnsResolution);
                assert.equal(true, data.response.overridable);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.value);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.overrideTargetId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'createFQDNObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectCreateHostObjectBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'DOMAIN',
          state: false
        },
        timestamp: 2
      },
      overridable: true,
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      id: 'string',
      type: 'string',
      value: 'string',
      version: 'string',
      overrideTargetId: 'string'
    };
    describe('#createHostObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createHostObject(null, objectDomainUUID, objectCreateHostObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(false, data.response.overridable);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.value);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.overrideTargetId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'createHostObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectCreateICMPV4ObjectBodyParam = {
      icmpType: 'string',
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'SYSTEM',
          state: false
        },
        parentType: 'string',
        timestamp: 1
      },
      code: 8,
      overridable: false,
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      id: 'string',
      type: 'string',
      version: 'string',
      overrideTargetId: 'string'
    };
    describe('#createICMPV4Object - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createICMPV4Object(null, objectDomainUUID, objectCreateICMPV4ObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.icmpType);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(2, data.response.code);
                assert.equal(false, data.response.overridable);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.overrideTargetId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'createICMPV4Object', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectCreateICMPV6ObjectBodyParam = {
      icmpType: 'string',
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'SYSTEM',
          state: false
        },
        parentType: 'string',
        timestamp: 1
      },
      code: 8,
      overridable: true,
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      id: 'string',
      type: 'string',
      version: 'string',
      overrideTargetId: 'string'
    };
    describe('#createICMPV6Object - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createICMPV6Object(null, objectDomainUUID, objectCreateICMPV6ObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.icmpType);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(10, data.response.code);
                assert.equal(false, data.response.overridable);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.overrideTargetId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'createICMPV6Object', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectCreateIKEv1IPsecProposalBodyParam = {
      espEncryption: 'string',
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'DOMAIN',
          state: false
        },
        timestamp: 10
      },
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      type: 'string',
      version: 'string',
      espHash: 'string'
    };
    describe('#createIKEv1IPsecProposal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createIKEv1IPsecProposal(objectDomainUUID, objectCreateIKEv1IPsecProposalBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.espEncryption);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.espHash);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'createIKEv1IPsecProposal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectCreateIkev1PolicyObjectBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'RBAC',
          state: true
        },
        timestamp: 5
      },
      description: 'string',
      priority: 9,
      type: 'string',
      version: 'string',
      encryption: 'string',
      diffieHellmanGroup: 2,
      authenticationMethod: 'string',
      name: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      lifetimeInSeconds: 4,
      id: 'string',
      hash: 'string'
    };
    describe('#createIkev1PolicyObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createIkev1PolicyObject(objectDomainUUID, objectCreateIkev1PolicyObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('string', data.response.description);
                assert.equal(4, data.response.priority);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.encryption);
                assert.equal(7, data.response.diffieHellmanGroup);
                assert.equal('string', data.response.authenticationMethod);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.links);
                assert.equal(3, data.response.lifetimeInSeconds);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.hash);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'createIkev1PolicyObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectCreateIKEv2IPsecProposalBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'DOMAIN',
          state: true
        },
        timestamp: 10
      },
      encryptionAlgorithms: [
        'string'
      ],
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      type: 'string',
      version: 'string',
      integrityAlgorithms: [
        'string'
      ]
    };
    describe('#createIKEv2IPsecProposal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createIKEv2IPsecProposal(objectDomainUUID, objectCreateIKEv2IPsecProposalBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, Array.isArray(data.response.encryptionAlgorithms));
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal(true, Array.isArray(data.response.integrityAlgorithms));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'createIKEv2IPsecProposal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectCreateIkev2PolicyObjectBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'DOMAIN',
          state: true
        },
        timestamp: 10
      },
      encryptionAlgorithms: [
        'string'
      ],
      description: 'string',
      priority: 3,
      type: 'string',
      diffieHellmanGroups: [
        5
      ],
      version: 'string',
      prfIntegrityAlgorithms: [
        'string'
      ],
      name: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      lifetimeInSeconds: 6,
      id: 'string',
      integrityAlgorithms: [
        'string'
      ]
    };
    describe('#createIkev2PolicyObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createIkev2PolicyObject(objectDomainUUID, objectCreateIkev2PolicyObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, Array.isArray(data.response.encryptionAlgorithms));
                assert.equal('string', data.response.description);
                assert.equal(6, data.response.priority);
                assert.equal('string', data.response.type);
                assert.equal(true, Array.isArray(data.response.diffieHellmanGroups));
                assert.equal('string', data.response.version);
                assert.equal(true, Array.isArray(data.response.prfIntegrityAlgorithms));
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.links);
                assert.equal(8, data.response.lifetimeInSeconds);
                assert.equal('string', data.response.id);
                assert.equal(true, Array.isArray(data.response.integrityAlgorithms));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'createIkev2PolicyObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectCreateInterfaceGroupObjectBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: false
        },
        timestamp: 9
      },
      interfaces: [
        {
          metadata: {
            lastUser: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            domain: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            readOnly: {
              reason: {},
              state: true
            },
            timestamp: 2
          },
          name: 'string',
          description: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string',
          version: 'string'
        }
      ],
      interfaceMode: 'SWITCHED',
      name: 'string',
      overridable: false,
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      type: 'string',
      version: 'string',
      interfaceGroup: 4
    };
    describe('#createInterfaceGroupObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createInterfaceGroupObject(null, objectDomainUUID, objectCreateInterfaceGroupObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, Array.isArray(data.response.interfaces));
                assert.equal('PASSIVE', data.response.interfaceMode);
                assert.equal('string', data.response.name);
                assert.equal(false, data.response.overridable);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal(3, data.response.interfaceGroup);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'createInterfaceGroupObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectCreateKeyChainObjectBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'RBAC',
          state: false
        },
        timestamp: 2
      },
      keys: [
        {
          acceptLifeTime: {
            endLifeTimeValue: 'string',
            startLifeTimeValue: 'string',
            timeZone: 'string',
            endLifetimeType: 'INFINITE'
          },
          authAlgorithm: 'string',
          keyId: 4,
          authString: {
            cryptoKeyString: 'string',
            cryptoEncryptionType: 'PLAINTEXT'
          },
          sendLifeTime: {
            endLifeTimeValue: 'string',
            startLifeTimeValue: 'string',
            timeZone: 'string',
            endLifetimeType: 'DURATION'
          }
        }
      ],
      name: 'string',
      overridable: false,
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      type: 'string',
      version: 'string',
      overrideTargetId: 'string'
    };
    describe('#createKeyChainObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createKeyChainObject(null, objectDomainUUID, objectCreateKeyChainObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, Array.isArray(data.response.keys));
                assert.equal('string', data.response.name);
                assert.equal(false, data.response.overridable);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.overrideTargetId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'createKeyChainObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectCreateNetworkGroupBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'SYSTEM',
          state: false
        },
        timestamp: 4
      },
      objects: [
        {
          metadata: {
            lastUser: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            domain: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            readOnly: {
              reason: {},
              state: true
            },
            timestamp: 3
          },
          overridable: false,
          name: 'string',
          description: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          overrides: {
            parent: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            target: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            }
          },
          id: 'string',
          type: 'string',
          version: 'string'
        }
      ],
      literals: [
        {
          type: 'string',
          value: 'string'
        }
      ],
      overridable: true,
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      id: 'string',
      type: 'string',
      version: 'string',
      overrideTargetId: 'string'
    };
    describe('#createNetworkGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createNetworkGroup(null, objectDomainUUID, objectCreateNetworkGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, Array.isArray(data.response.objects));
                assert.equal(true, Array.isArray(data.response.literals));
                assert.equal(false, data.response.overridable);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.overrideTargetId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'createNetworkGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectCreateNetworkObjectBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'SYSTEM',
          state: false
        },
        timestamp: 3
      },
      overridable: false,
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      id: 'string',
      type: 'string',
      value: 'string',
      version: 'string',
      overrideTargetId: 'string'
    };
    describe('#createNetworkObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createNetworkObject(null, objectDomainUUID, objectCreateNetworkObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, data.response.overridable);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.value);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.overrideTargetId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'createNetworkObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectCreatePortObjectGroupBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'RBAC',
          state: true
        },
        parentType: 'string',
        timestamp: 3
      },
      objects: [
        {
          metadata: {
            lastUser: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            domain: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            readOnly: {
              reason: {},
              state: true
            },
            timestamp: 3
          },
          overridable: true,
          name: 'string',
          description: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          overrides: {
            parent: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            target: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            }
          },
          id: 'string',
          type: 'string',
          version: 'string'
        }
      ],
      overridable: true,
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      id: 'string',
      type: 'string',
      version: 'string',
      overrideTargetId: 'string'
    };
    describe('#createPortObjectGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPortObjectGroup(null, objectDomainUUID, objectCreatePortObjectGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, Array.isArray(data.response.objects));
                assert.equal(false, data.response.overridable);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.overrideTargetId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'createPortObjectGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectCreateProtocolPortObjectBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'SYSTEM',
          state: true
        },
        parentType: 'string',
        timestamp: 9
      },
      protocol: 'string',
      port: 'string',
      overridable: false,
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      id: 'string',
      type: 'string',
      version: 'string',
      overrideTargetId: 'string'
    };
    describe('#createProtocolPortObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createProtocolPortObject(null, objectDomainUUID, objectCreateProtocolPortObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('string', data.response.protocol);
                assert.equal('string', data.response.port);
                assert.equal(false, data.response.overridable);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.overrideTargetId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'createProtocolPortObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectCreateRangeObjectBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'DOMAIN',
          state: true
        },
        timestamp: 4
      },
      overridable: true,
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      id: 'string',
      type: 'string',
      value: 'string',
      version: 'string',
      overrideTargetId: 'string'
    };
    describe('#createRangeObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createRangeObject(null, objectDomainUUID, objectCreateRangeObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(false, data.response.overridable);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.value);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.overrideTargetId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'createRangeObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectCreateSecurityZoneObjectBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: false
        },
        timestamp: 8
      },
      interfaces: [
        {
          metadata: {
            lastUser: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            domain: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            readOnly: {
              reason: {},
              state: false
            },
            timestamp: 2
          },
          name: 'string',
          description: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string',
          version: 'string'
        }
      ],
      interfaceMode: 'INLINE',
      name: 'string',
      overridable: true,
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      type: 'string',
      version: 'string'
    };
    describe('#createSecurityZoneObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSecurityZoneObject(null, objectDomainUUID, objectCreateSecurityZoneObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, Array.isArray(data.response.interfaces));
                assert.equal('ASA', data.response.interfaceMode);
                assert.equal('string', data.response.name);
                assert.equal(false, data.response.overridable);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'createSecurityZoneObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectCreateSLAMonitorObjectModelBodyParam = {
      slaId: 2,
      interfaceObjects: [
        {}
      ],
      monitorAddress: 'string'
    };
    describe('#createSLAMonitorObjectModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSLAMonitorObjectModel(null, objectDomainUUID, objectCreateSLAMonitorObjectModelBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(6, data.response.noOfPackets);
                assert.equal(4, data.response.slaId);
                assert.equal(6, data.response.dataSize);
                assert.equal('string', data.response.description);
                assert.equal(4, data.response.threshold);
                assert.equal('object', typeof data.response.overrides);
                assert.equal(true, Array.isArray(data.response.interfaceNames));
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal(10, data.response.timeout);
                assert.equal(true, Array.isArray(data.response.interfaceObjects));
                assert.equal(10, data.response.frequency);
                assert.equal('string', data.response.monitorAddress);
                assert.equal(true, data.response.overridable);
                assert.equal('string', data.response.name);
                assert.equal(8, data.response.tos);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'createSLAMonitorObjectModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectCreateURLGroupObjectBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'DOMAIN',
          state: true
        },
        timestamp: 3
      },
      objects: [
        {
          metadata: {
            lastUser: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            domain: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            readOnly: {
              reason: {},
              state: true
            },
            timestamp: 1
          },
          overridable: false,
          name: 'string',
          description: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          overrides: {
            parent: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            target: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            }
          },
          id: 'string',
          type: 'string',
          version: 'string',
          url: 'string'
        }
      ],
      literals: [
        {
          type: 'string',
          url: 'string'
        }
      ],
      overridable: true,
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      id: 'string',
      type: 'string',
      version: 'string',
      overrideTargetId: 'string'
    };
    describe('#createURLGroupObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createURLGroupObject(null, objectDomainUUID, objectCreateURLGroupObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, Array.isArray(data.response.objects));
                assert.equal(true, Array.isArray(data.response.literals));
                assert.equal(true, data.response.overridable);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.overrideTargetId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'createURLGroupObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectCreateURLObjectBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'SYSTEM',
          state: false
        },
        timestamp: 1
      },
      overridable: false,
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      id: 'string',
      type: 'string',
      version: 'string',
      url: 'string',
      overrideTargetId: 'string'
    };
    describe('#createURLObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createURLObject(null, objectDomainUUID, objectCreateURLObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, data.response.overridable);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.overrideTargetId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'createURLObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectCreateVlanTagGroupBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'RBAC',
          state: true
        },
        timestamp: 3
      },
      objects: [
        {
          metadata: {
            lastUser: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            domain: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            readOnly: {
              reason: {},
              state: true
            },
            timestamp: 2
          },
          data: {
            startTag: 5,
            endTag: 4,
            type: 'string'
          },
          overridable: false,
          name: 'string',
          description: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          overrides: {
            parent: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            target: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            }
          },
          id: 'string',
          type: 'string',
          version: 'string'
        }
      ],
      literals: [
        {
          startTag: 10,
          endTag: 4,
          type: 'string'
        }
      ],
      overridable: false,
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      id: 'string',
      type: 'string',
      version: 'string',
      overrideTargetId: 'string'
    };
    describe('#createVlanTagGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createVlanTagGroup(null, objectDomainUUID, objectCreateVlanTagGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, Array.isArray(data.response.objects));
                assert.equal(true, Array.isArray(data.response.literals));
                assert.equal(false, data.response.overridable);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.overrideTargetId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'createVlanTagGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectCreateVlanTagBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'RBAC',
          state: true
        },
        timestamp: 8
      },
      data: {
        startTag: 4,
        endTag: 5,
        type: 'string'
      },
      overridable: true,
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      id: 'string',
      type: 'string',
      version: 'string',
      overrideTargetId: 'string'
    };
    describe('#createVlanTag - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createVlanTag(null, objectDomainUUID, objectCreateVlanTagBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('object', typeof data.response.data);
                assert.equal(false, data.response.overridable);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.overrideTargetId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'createVlanTag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllAnyProtocolPortObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllAnyProtocolPortObject(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('AnyProtocolPortObject', data.response.type);
                assert.equal('myAny', data.response.name);
                assert.equal(' ', data.response.description);
                assert.equal('anyProtocolPortObjUUID', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllAnyProtocolPortObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectObjectId = 'fakedata';
    describe('#getAnyProtocolPortObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAnyProtocolPortObject(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, data.response.overridable);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.overrideTargetId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAnyProtocolPortObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllApplicationCategory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllApplicationCategory(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllApplicationCategory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationCategory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApplicationCategory(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('ApplicationCategory', data.response.type);
                assert.equal('050plus', data.response.name);
                assert.equal('2325', data.response.id);
                assert.equal('object', typeof data.response.metadata);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getApplicationCategory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllApplicationFilter - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllApplicationFilter(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllApplicationFilter', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationFilter - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApplicationFilter(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.appConditions));
                assert.equal('test1', data.response.name);
                assert.equal('907b5452-e4e0-11e5-87ba-df5089dc85eb', data.response.id);
                assert.equal('object', typeof data.response.links);
                assert.equal('ApplicationFilter', data.response.type);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, Array.isArray(data.response.applications));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getApplicationFilter', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllApplicationProductivity - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllApplicationProductivity(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllApplicationProductivity', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationProductivity - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApplicationProductivity(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('ApplicationProductivity', data.response.type);
                assert.equal('Very Low', data.response.name);
                assert.equal('VERY_LOW', data.response.id);
                assert.equal('object', typeof data.response.metadata);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getApplicationProductivity', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllApplicationRisk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllApplicationRisk(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllApplicationRisk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationRisk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApplicationRisk(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('ApplicationRisk', data.response.type);
                assert.equal('Very Low', data.response.name);
                assert.equal('VERY_LOW', data.response.id);
                assert.equal('object', typeof data.response.metadata);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getApplicationRisk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllApplication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllApplication(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApplication(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('Application', data.response.type);
                assert.equal('050plus', data.response.name);
                assert.equal('2325', data.response.id);
                assert.equal('object', typeof data.response.metadata);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllApplicationTag - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllApplicationTag(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllApplicationTag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationTag - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApplicationTag(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('ApplicationTag', data.response.type);
                assert.equal('tag1', data.response.name);
                assert.equal('2325', data.response.id);
                assert.equal('object', typeof data.response.metadata);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getApplicationTag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllApplicationType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllApplicationType(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllApplicationType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApplicationType(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('ApplicationType', data.response.type);
                assert.equal('type1', data.response.name);
                assert.equal('2325', data.response.id);
                assert.equal('object', typeof data.response.metadata);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getApplicationType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllVpnPKIEnrollmentModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllVpnPKIEnrollmentModel(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllVpnPKIEnrollmentModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVpnPKIEnrollmentModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVpnPKIEnrollmentModel(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('string', data.response.name);
                assert.equal(false, data.response.overridable);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getVpnPKIEnrollmentModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllContinentObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllContinentObject(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllContinentObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getContinentObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getContinentObject(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('2', data.response.id);
                assert.equal('Antarctica', data.response.name);
                assert.equal('Continent', data.response.type);
                assert.equal(true, Array.isArray(data.response.countries));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getContinentObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllCountryObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllCountryObject(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllCountryObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCountryObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCountryObject(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('countryUUID', data.response.id);
                assert.equal('Aruba', data.response.name);
                assert.equal('aw', data.response.iso2);
                assert.equal('abw', data.response.iso3);
                assert.equal('object', typeof data.response.links);
                assert.equal('Country', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getCountryObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllDNSServerGroupObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllDNSServerGroupObject(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllDNSServerGroupObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectUpdateDNSServerGroupObjectBodyParam = {
      retries: 10,
      defaultdomain: 'string',
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'SYSTEM',
          state: true
        },
        timestamp: 5
      },
      dnsservers: [
        {
          'name-server': 'string'
        }
      ],
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      type: 'string',
      version: 'string',
      timeout: 2
    };
    describe('#updateDNSServerGroupObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDNSServerGroupObject(objectObjectId, objectDomainUUID, objectUpdateDNSServerGroupObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'updateDNSServerGroupObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDNSServerGroupObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDNSServerGroupObject(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.dnsservers));
                assert.equal(1, data.response.retries);
                assert.equal('cisco.com', data.response.defaultdomain);
                assert.equal('DNSServerGroupObject', data.response.type);
                assert.equal('DNSServerGroupObjectName1', data.response.name);
                assert.equal(1, data.response.timeout);
                assert.equal('DNSServerGroupObjectUUID', data.response.id);
                assert.equal('object', typeof data.response.metadata);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getDNSServerGroupObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllEndPointDeviceType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllEndPointDeviceType(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllEndPointDeviceType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEndPointDeviceType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getEndPointDeviceType(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('string', data.response.name);
                assert.equal(true, data.response.overridable);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.iseId);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.fqName);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getEndPointDeviceType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllExtendedAccessListModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllExtendedAccessListModel(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllExtendedAccessListModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtendedAccessListModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExtendedAccessListModel(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('string', data.response.name);
                assert.equal(true, data.response.overridable);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getExtendedAccessListModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFQDNObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllFQDNObject(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllFQDNObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectContainerUUID = 'fakedata';
    describe('#getAllFQDNOverride - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllFQDNOverride(objectContainerUUID, objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllFQDNOverride', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFQDNOverride - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFQDNOverride(objectObjectId, objectContainerUUID, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(false, data.response.overridable);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getFQDNOverride', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectUpdateFQDNObjectBodyParam = {
      dnsResolution: 'IPV4_AND_IPV6'
    };
    describe('#updateFQDNObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFQDNObject(objectObjectId, objectDomainUUID, objectUpdateFQDNObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'updateFQDNObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFQDNObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFQDNObject(objectObjectId, null, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('FQDN', data.response.type);
                assert.equal('www.cisco.com', data.response.value);
                assert.equal('IPV4_ONLY', data.response.dnsResolution);
                assert.equal(false, data.response.overridable);
                assert.equal('Test Description', data.response.description);
                assert.equal('5555-6666-7777-8888', data.response.id);
                assert.equal('TestFQDN', data.response.name);
                assert.equal('object', typeof data.response.metadata);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getFQDNObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllGeoLocationObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllGeoLocationObject(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllGeoLocationObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGeoLocationObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGeoLocationObject(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.continentUUID);
                assert.equal(true, Array.isArray(data.response.countries));
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal(true, Array.isArray(data.response.continents));
                assert.equal(3, data.response.continentId);
                assert.equal('string', data.response.name);
                assert.equal(true, data.response.overridable);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getGeoLocationObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllHostObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllHostObject(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllHostObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllHostOverride - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllHostOverride(objectContainerUUID, objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllHostOverride', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostOverride - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHostOverride(objectObjectId, objectContainerUUID, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(false, data.response.overridable);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getHostOverride', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectUpdateHostObjectBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'SYSTEM',
          state: true
        },
        timestamp: 8
      },
      overridable: true,
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      id: 'string',
      type: 'string',
      value: 'string',
      version: 'string',
      overrideTargetId: 'string'
    };
    describe('#updateHostObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateHostObject(objectObjectId, objectDomainUUID, objectUpdateHostObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'updateHostObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHostObject(objectObjectId, null, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('Host', data.response.type);
                assert.equal('10.5.3.28', data.response.value);
                assert.equal(false, data.response.overridable);
                assert.equal('Test Description', data.response.description);
                assert.equal('hostObject3UUID', data.response.id);
                assert.equal('TestHost', data.response.name);
                assert.equal('object', typeof data.response.metadata);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getHostObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllICMPV4Object - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllICMPV4Object(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllICMPV4Object', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllICMPV4ObjectOverride - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllICMPV4ObjectOverride(objectContainerUUID, objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllICMPV4ObjectOverride', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getICMPV4ObjectOverride - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getICMPV4ObjectOverride(objectObjectId, objectContainerUUID, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(false, data.response.overridable);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getICMPV4ObjectOverride', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectUpdateICMPV4ObjectBodyParam = {
      icmpType: 'string',
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'RBAC',
          state: true
        },
        parentType: 'string',
        timestamp: 3
      },
      code: 7,
      overridable: false,
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      id: 'string',
      type: 'string',
      version: 'string',
      overrideTargetId: 'string'
    };
    describe('#updateICMPV4Object - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateICMPV4Object(objectObjectId, objectDomainUUID, objectUpdateICMPV4ObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'updateICMPV4Object', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getICMPV4Object - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getICMPV4Object(objectObjectId, null, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('icmpv4Object1UUID', data.response.id);
                assert.equal('icmpv4_obj1', data.response.name);
                assert.equal('ICMPV4Object', data.response.type);
                assert.equal('3', data.response.icmpType);
                assert.equal(0, data.response.code);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getICMPV4Object', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllICMPV6Object - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllICMPV6Object(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllICMPV6Object', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllICMPV6ObjectOverride - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllICMPV6ObjectOverride(objectContainerUUID, objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllICMPV6ObjectOverride', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getICMPV6ObjectOverride - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getICMPV6ObjectOverride(objectObjectId, objectContainerUUID, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(false, data.response.overridable);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getICMPV6ObjectOverride', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectUpdateICMPV6ObjectBodyParam = {
      icmpType: 'string',
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'RBAC',
          state: false
        },
        parentType: 'string',
        timestamp: 5
      },
      code: 5,
      overridable: false,
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      id: 'string',
      type: 'string',
      version: 'string',
      overrideTargetId: 'string'
    };
    describe('#updateICMPV6Object - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateICMPV6Object(objectObjectId, objectDomainUUID, objectUpdateICMPV6ObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'updateICMPV6Object', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getICMPV6Object - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getICMPV6Object(objectObjectId, null, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('icmpv6ObjectUUID', data.response.id);
                assert.equal('icmpv6_obj1', data.response.name);
                assert.equal('ICMPV6Object', data.response.type);
                assert.equal('3', data.response.icmpType);
                assert.equal(0, data.response.code);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getICMPV6Object', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllIKEv1IPsecProposal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllIKEv1IPsecProposal(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllIKEv1IPsecProposal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectUpdateIKEv1IPsecProposalBodyParam = {
      espEncryption: 'string',
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'DOMAIN',
          state: false
        },
        timestamp: 8
      },
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      type: 'string',
      version: 'string',
      espHash: 'string'
    };
    describe('#updateIKEv1IPsecProposal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateIKEv1IPsecProposal(objectObjectId, objectDomainUUID, objectUpdateIKEv1IPsecProposalBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'updateIKEv1IPsecProposal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIKEv1IPsecProposal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIKEv1IPsecProposal(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('AES-192', data.response.espEncryption);
                assert.equal('NONE', data.response.espHash);
                assert.equal('ikev1ipsecproposal-1', data.response.name);
                assert.equal('IKEv1 IPsec Proposal object description', data.response.description);
                assert.equal('ikev1ipsecproposalUUID', data.response.id);
                assert.equal('IKEv1IPsecProposal', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getIKEv1IPsecProposal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllIkev1PolicyObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllIkev1PolicyObject(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllIkev1PolicyObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectUpdateIkev1PolicyObjectBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'RBAC',
          state: false
        },
        timestamp: 6
      },
      description: 'string',
      priority: 3,
      type: 'string',
      version: 'string',
      encryption: 'string',
      diffieHellmanGroup: 3,
      authenticationMethod: 'string',
      name: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      lifetimeInSeconds: 10,
      id: 'string',
      hash: 'string'
    };
    describe('#updateIkev1PolicyObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateIkev1PolicyObject(objectObjectId, objectDomainUUID, objectUpdateIkev1PolicyObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'updateIkev1PolicyObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIkev1PolicyObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIkev1PolicyObject(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(20, data.response.priority);
                assert.equal(86400, data.response.lifetimeInSeconds);
                assert.equal(5, data.response.diffieHellmanGroup);
                assert.equal('Preshared Key', data.response.authenticationMethod);
                assert.equal('AES-128', data.response.encryption);
                assert.equal('SHA', data.response.hash);
                assert.equal('ikev1_object', data.response.name);
                assert.equal('Get ikev1 policy object by id', data.response.description);
                assert.equal('ikev1policyUUID', data.response.id);
                assert.equal('IKEv1Policy', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getIkev1PolicyObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllIKEv2IPsecProposal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllIKEv2IPsecProposal(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllIKEv2IPsecProposal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectUpdateIKEv2IPsecProposalBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'DOMAIN',
          state: false
        },
        timestamp: 7
      },
      encryptionAlgorithms: [
        'string'
      ],
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      type: 'string',
      version: 'string',
      integrityAlgorithms: [
        'string'
      ]
    };
    describe('#updateIKEv2IPsecProposal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateIKEv2IPsecProposal(objectObjectId, objectDomainUUID, objectUpdateIKEv2IPsecProposalBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'updateIKEv2IPsecProposal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIKEv2IPsecProposal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIKEv2IPsecProposal(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.encryptionAlgorithms));
                assert.equal(true, Array.isArray(data.response.integrityAlgorithms));
                assert.equal('ikev2ipsecproposal-1', data.response.name);
                assert.equal('IKEv2 IPsec Proposal object description', data.response.description);
                assert.equal('ikev2ipsecproposalUUID', data.response.id);
                assert.equal('IKEv2IPsecProposal', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getIKEv2IPsecProposal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllIkev2PolicyObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllIkev2PolicyObject(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
                assert.equal(200, data.response.expectStatus);
                assert.equal(true, Array.isArray(data.response.expectHeaderContains));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllIkev2PolicyObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectUpdateIkev2PolicyObjectBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'DOMAIN',
          state: false
        },
        timestamp: 4
      },
      encryptionAlgorithms: [
        'string'
      ],
      description: 'string',
      priority: 6,
      type: 'string',
      diffieHellmanGroups: [
        5
      ],
      version: 'string',
      prfIntegrityAlgorithms: [
        'string'
      ],
      name: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      lifetimeInSeconds: 6,
      id: 'string',
      integrityAlgorithms: [
        'string'
      ]
    };
    describe('#updateIkev2PolicyObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateIkev2PolicyObject(objectObjectId, objectDomainUUID, objectUpdateIkev2PolicyObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'updateIkev2PolicyObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIkev2PolicyObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIkev2PolicyObject(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getIkev2PolicyObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllInterfaceGroupObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllInterfaceGroupObject(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllInterfaceGroupObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectUpdateInterfaceGroupObjectBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: true
        },
        timestamp: 6
      },
      interfaces: [
        {
          metadata: {
            lastUser: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            domain: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            readOnly: {
              reason: {},
              state: true
            },
            timestamp: 3
          },
          name: 'string',
          description: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string',
          version: 'string'
        }
      ],
      interfaceMode: 'SWITCHED',
      name: 'string',
      overridable: false,
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      type: 'string',
      version: 'string',
      interfaceGroup: 7
    };
    describe('#updateInterfaceGroupObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateInterfaceGroupObject(objectObjectId, objectDomainUUID, objectUpdateInterfaceGroupObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'updateInterfaceGroupObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterfaceGroupObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInterfaceGroupObject(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('InterfaceGroupObject5', data.response.name);
                assert.equal('Interface-group-UUID-1', data.response.id);
                assert.equal('InterfaceGroup', data.response.type);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('INLINE', data.response.interfaceMode);
                assert.equal(true, Array.isArray(data.response.interfaces));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getInterfaceGroupObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllInterfaceObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllInterfaceObject(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllInterfaceObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterfaceObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInterfaceObject(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, Array.isArray(data.response.interfaces));
                assert.equal('string', data.response.name);
                assert.equal(false, data.response.overridable);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getInterfaceObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllISESecurityGroupTag - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllISESecurityGroupTag(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllISESecurityGroupTag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getISESecurityGroupTag - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getISESecurityGroupTag(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('string', data.response.name);
                assert.equal(false, data.response.overridable);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal(10, data.response.tag);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getISESecurityGroupTag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllKeyChainObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllKeyChainObject(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllKeyChainObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllKeyChainObjectOverride - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllKeyChainObjectOverride(objectContainerUUID, objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllKeyChainObjectOverride', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getKeyChainObjectOverride - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getKeyChainObjectOverride(objectObjectId, objectContainerUUID, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(false, data.response.overridable);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getKeyChainObjectOverride', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectUpdateKeyChainObjectBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'RBAC',
          state: false
        },
        timestamp: 5
      },
      keys: [
        {
          acceptLifeTime: {
            endLifeTimeValue: 'string',
            startLifeTimeValue: 'string',
            timeZone: 'string',
            endLifetimeType: 'DATETIME'
          },
          authAlgorithm: 'string',
          keyId: 10,
          authString: {
            cryptoKeyString: 'string',
            cryptoEncryptionType: 'PLAINTEXT'
          },
          sendLifeTime: {
            endLifeTimeValue: 'string',
            startLifeTimeValue: 'string',
            timeZone: 'string',
            endLifetimeType: 'INFINITE'
          }
        }
      ],
      name: 'string',
      overridable: false,
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      type: 'string',
      version: 'string',
      overrideTargetId: 'string'
    };
    describe('#updateKeyChainObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateKeyChainObject(objectObjectId, objectDomainUUID, objectUpdateKeyChainObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'updateKeyChainObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getKeyChainObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getKeyChainObject(objectObjectId, null, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.keys));
                assert.equal('KeyChainObject', data.response.type);
                assert.equal('KeyChainObjectName1', data.response.name);
                assert.equal('KeyChainUUID', data.response.id);
                assert.equal('description for keychain object', data.response.description);
                assert.equal(false, data.response.overridable);
                assert.equal('object', typeof data.response.metadata);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getKeyChainObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkAddress - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkAddress(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getNetworkAddress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllNetworkGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllNetworkGroup(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllNetworkGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllNetworkGroupOverride - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllNetworkGroupOverride(objectContainerUUID, objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllNetworkGroupOverride', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkGroupOverride - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkGroupOverride(objectObjectId, objectContainerUUID, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(false, data.response.overridable);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getNetworkGroupOverride', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectUpdateNetworkGroupBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'RBAC',
          state: true
        },
        timestamp: 5
      },
      objects: [
        {
          metadata: {
            lastUser: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            domain: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            readOnly: {
              reason: {},
              state: false
            },
            timestamp: 5
          },
          overridable: true,
          name: 'string',
          description: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          overrides: {
            parent: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            target: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            }
          },
          id: 'string',
          type: 'string',
          version: 'string'
        }
      ],
      literals: [
        {
          type: 'string',
          value: 'string'
        }
      ],
      overridable: false,
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      id: 'string',
      type: 'string',
      version: 'string',
      overrideTargetId: 'string'
    };
    describe('#updateNetworkGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkGroup(objectObjectId, objectDomainUUID, objectUpdateNetworkGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'updateNetworkGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkGroup(objectObjectId, null, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('networkGroup1UUID', data.response.id);
                assert.equal('networkgroup_obj1', data.response.name);
                assert.equal('NetworkGroup', data.response.type);
                assert.equal(true, Array.isArray(data.response.objects));
                assert.equal(true, Array.isArray(data.response.literals));
                assert.equal('object', typeof data.response.metadata);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getNetworkGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllNetworkObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllNetworkObject(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllNetworkObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllNetworkOverride - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllNetworkOverride(objectContainerUUID, objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllNetworkOverride', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectUpdateNetworkObjectBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'SYSTEM',
          state: false
        },
        timestamp: 2
      },
      overridable: false,
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      id: 'string',
      type: 'string',
      value: 'string',
      version: 'string',
      overrideTargetId: 'string'
    };
    describe('#updateNetworkObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkObject(objectObjectId, objectDomainUUID, objectUpdateNetworkObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'updateNetworkObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkObject(objectObjectId, null, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('Host', data.response.type);
                assert.equal('1.2.3.4', data.response.value);
                assert.equal('object', typeof data.response.overrides);
                assert.equal(true, data.response.overridable);
                assert.equal(' ', data.response.description);
                assert.equal('pvs_global', data.response.name);
                assert.equal('networkObjectUUID', data.response.id);
                assert.equal('object', typeof data.response.metadata);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getNetworkObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPortObjectGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllPortObjectGroup(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllPortObjectGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPortObjectGroupOverride - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllPortObjectGroupOverride(objectContainerUUID, objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllPortObjectGroupOverride', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPortObjectGroupOverride - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPortObjectGroupOverride(objectObjectId, objectContainerUUID, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, data.response.overridable);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getPortObjectGroupOverride', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectUpdatePortObjectGroupBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'RBAC',
          state: true
        },
        parentType: 'string',
        timestamp: 10
      },
      objects: [
        {
          metadata: {
            lastUser: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            domain: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            readOnly: {
              reason: {},
              state: false
            },
            timestamp: 1
          },
          overridable: false,
          name: 'string',
          description: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          overrides: {
            parent: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            target: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            }
          },
          id: 'string',
          type: 'string',
          version: 'string'
        }
      ],
      overridable: false,
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      id: 'string',
      type: 'string',
      version: 'string',
      overrideTargetId: 'string'
    };
    describe('#updatePortObjectGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePortObjectGroup(objectObjectId, objectDomainUUID, objectUpdatePortObjectGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'updatePortObjectGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPortObjectGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPortObjectGroup(objectObjectId, null, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('PortGroupObjUUID', data.response.id);
                assert.equal('portgroup_obj1', data.response.name);
                assert.equal('PortObjectGroup', data.response.type);
                assert.equal(true, Array.isArray(data.response.objects));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getPortObjectGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPortObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPortObject(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getPortObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllProtocolPortObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllProtocolPortObject(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllProtocolPortObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllProtocolPortObjectOverride - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllProtocolPortObjectOverride(objectContainerUUID, objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllProtocolPortObjectOverride', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProtocolPortObjectOverride - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getProtocolPortObjectOverride(objectObjectId, objectContainerUUID, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(false, data.response.overridable);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getProtocolPortObjectOverride', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectUpdateProtocolPortObjectBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'SYSTEM',
          state: true
        },
        parentType: 'string',
        timestamp: 8
      },
      protocol: 'string',
      port: 'string',
      overridable: true,
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      id: 'string',
      type: 'string',
      version: 'string',
      overrideTargetId: 'string'
    };
    describe('#updateProtocolPortObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateProtocolPortObject(objectObjectId, objectDomainUUID, objectUpdateProtocolPortObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'updateProtocolPortObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProtocolPortObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getProtocolPortObject(objectObjectId, null, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('protocoPortObj1UUID', data.response.id);
                assert.equal('protocolport_obj1', data.response.name);
                assert.equal('ProtocolPortObject', data.response.type);
                assert.equal('TCP', data.response.protocol);
                assert.equal('123', data.response.port);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getProtocolPortObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRangeObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllRangeObject(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllRangeObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRangeOverride - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllRangeOverride(objectContainerUUID, objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllRangeOverride', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRangeOverride - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRangeOverride(objectObjectId, objectContainerUUID, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, data.response.overridable);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getRangeOverride', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectUpdateRangeObjectBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'SYSTEM',
          state: true
        },
        timestamp: 5
      },
      overridable: false,
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      id: 'string',
      type: 'string',
      value: 'string',
      version: 'string',
      overrideTargetId: 'string'
    };
    describe('#updateRangeObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateRangeObject(objectObjectId, objectDomainUUID, objectUpdateRangeObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'updateRangeObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRangeObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRangeObject(objectObjectId, null, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('Range', data.response.type);
                assert.equal('2003::3-2003::4', data.response.value);
                assert.equal(false, data.response.overridable);
                assert.equal('Test Description', data.response.description);
                assert.equal('TestRange2', data.response.name);
                assert.equal('Range-obj-UUID-1', data.response.id);
                assert.equal('object', typeof data.response.metadata);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getRangeObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRealm - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllRealm(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllRealm', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRealm - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRealm(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('Realm', data.response.type);
                assert.equal('Realm_1', data.response.name);
                assert.equal('RealmUUID', data.response.id);
                assert.equal('object', typeof data.response.metadata);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getRealm', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRealmUserGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllRealmUserGroup(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllRealmUserGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectRealmUuid = 'fakedata';
    describe('#getRealmUserGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRealmUserGroup(objectObjectId, objectRealmUuid, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('RealmUserGroup', data.response.type);
                assert.equal('Realm user group name', data.response.name);
                assert.equal('RealmUUID_RealmUserGroupUUID', data.response.id);
                assert.equal('object', typeof data.response.realm);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getRealmUserGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRealmUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllRealmUser(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllRealmUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRealmUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRealmUser(objectObjectId, objectRealmUuid, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('RealmUser', data.response.type);
                assert.equal('Realm Name_3', data.response.name);
                assert.equal('RealmUUID_RealmUserUUID', data.response.id);
                assert.equal('object', typeof data.response.realm);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getRealmUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSecurityGroupTag - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllSecurityGroupTag(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllSecurityGroupTag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityGroupTag - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecurityGroupTag(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('string', data.response.name);
                assert.equal(false, data.response.overridable);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal(7, data.response.tag);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getSecurityGroupTag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSecurityZoneObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllSecurityZoneObject(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllSecurityZoneObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectUpdateSecurityZoneObjectBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: {},
          state: false
        },
        timestamp: 9
      },
      interfaces: [
        {
          metadata: {
            lastUser: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            domain: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            readOnly: {
              reason: {},
              state: false
            },
            timestamp: 4
          },
          name: 'string',
          description: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string',
          version: 'string'
        }
      ],
      interfaceMode: 'SWITCHED',
      name: 'string',
      overridable: true,
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      type: 'string',
      version: 'string'
    };
    describe('#updateSecurityZoneObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateSecurityZoneObject(objectObjectId, objectDomainUUID, objectUpdateSecurityZoneObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'updateSecurityZoneObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityZoneObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecurityZoneObject(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('SecurityZoneObject5', data.response.name);
                assert.equal('Sec-zone-UUID-1', data.response.id);
                assert.equal('SecurityZone', data.response.type);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('INLINE', data.response.interfaceMode);
                assert.equal(true, Array.isArray(data.response.interfaces));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getSecurityZoneObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSIURLFeed - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllSIURLFeed(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllSIURLFeed', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSIURLFeed - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSIURLFeed(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('SIURLFeed', data.response.type);
                assert.equal('SIURLFeedId', data.response.id);
                assert.equal('URLFeed1', data.response.name);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getSIURLFeed', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSIURLList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllSIURLList(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllSIURLList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSIURLList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSIURLList(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('SIURLList', data.response.type);
                assert.equal('SIURLListId', data.response.id);
                assert.equal('URL', data.response.name);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getSIURLList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSLAMonitorObjectModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllSLAMonitorObjectModel(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.paging);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllSLAMonitorObjectModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectUpdateSLAMonitorObjectModelBodyParam = {
      slaId: 10,
      interfaceObjects: [
        {}
      ],
      monitorAddress: 'string'
    };
    describe('#updateSLAMonitorObjectModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateSLAMonitorObjectModel(objectObjectId, objectDomainUUID, objectUpdateSLAMonitorObjectModelBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'updateSLAMonitorObjectModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSLAMonitorObjectModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSLAMonitorObjectModel(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.interfaceNames));
                assert.equal(5000, data.response.timeout);
                assert.equal('SLAMonitor', data.response.type);
                assert.equal(2, data.response.threshold);
                assert.equal(60, data.response.frequency);
                assert.equal(1, data.response.slaId);
                assert.equal(28, data.response.dataSize);
                assert.equal(1, data.response.tos);
                assert.equal(1, data.response.noOfPackets);
                assert.equal('1.1.1.1', data.response.monitorAddress);
                assert.equal(true, Array.isArray(data.response.interfaceObjects));
                assert.equal('Sla monitor description', data.response.description);
                assert.equal('SLA1', data.response.name);
                assert.equal('slaMonitorObjectUUID', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getSLAMonitorObjectModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllTunnelTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllTunnelTags(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllTunnelTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTunnelTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTunnelTags(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('tunnelTagObj1UUID', data.response.id);
                assert.equal('TT_obj1', data.response.name);
                assert.equal('TunnelTag', data.response.type);
                assert.equal('some description', data.response.description);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getTunnelTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllURLCategory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllURLCategory(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllURLCategory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getURLCategory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getURLCategory(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('URLCategory', data.response.type);
                assert.equal('urlCategoryUUID', data.response.id);
                assert.equal('Auctions', data.response.name);
                assert.equal('object', typeof data.response.links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getURLCategory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllURLGroupObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllURLGroupObject(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllURLGroupObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllUrlGroupOverride - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllUrlGroupOverride(objectContainerUUID, objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllUrlGroupOverride', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUrlGroupOverride - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUrlGroupOverride(objectObjectId, objectContainerUUID, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(false, data.response.overridable);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getUrlGroupOverride', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectUpdateURLGroupObjectBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'SYSTEM',
          state: true
        },
        timestamp: 1
      },
      objects: [
        {
          metadata: {
            lastUser: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            domain: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            readOnly: {
              reason: {},
              state: false
            },
            timestamp: 10
          },
          overridable: false,
          name: 'string',
          description: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          overrides: {
            parent: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            target: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            }
          },
          id: 'string',
          type: 'string',
          version: 'string',
          url: 'string'
        }
      ],
      literals: [
        {
          type: 'string',
          url: 'string'
        }
      ],
      overridable: false,
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      id: 'string',
      type: 'string',
      version: 'string',
      overrideTargetId: 'string'
    };
    describe('#updateURLGroupObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateURLGroupObject(objectObjectId, objectDomainUUID, objectUpdateURLGroupObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'updateURLGroupObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getURLGroupObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getURLGroupObject(objectObjectId, null, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('UrlGroupUUID1', data.response.id);
                assert.equal('urlgroup_obj1', data.response.name);
                assert.equal('UrlGroup', data.response.type);
                assert.equal(true, Array.isArray(data.response.objects));
                assert.equal(true, Array.isArray(data.response.literals));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getURLGroupObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllURLObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllURLObject(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllURLObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllUrlOverride - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllUrlOverride(objectContainerUUID, objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllUrlOverride', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUrlOverride - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUrlOverride(objectObjectId, objectContainerUUID, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, data.response.overridable);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getUrlOverride', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectUpdateURLObjectBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'DOMAIN',
          state: true
        },
        timestamp: 4
      },
      overridable: true,
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      id: 'string',
      type: 'string',
      version: 'string',
      url: 'string',
      overrideTargetId: 'string'
    };
    describe('#updateURLObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateURLObject(objectObjectId, objectDomainUUID, objectUpdateURLObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'updateURLObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getURLObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getURLObject(objectObjectId, null, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('object', typeof data.response.links);
                assert.equal('Url', data.response.type);
                assert.equal('www.cisco.com', data.response.url);
                assert.equal(true, data.response.overridable);
                assert.equal('object', typeof data.response.overrides);
                assert.equal(' ', data.response.description);
                assert.equal('pvs_global_url', data.response.name);
                assert.equal('urlObjectUUID', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getURLObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllVariableSet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllVariableSet(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllVariableSet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVariableSet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVariableSet(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Default-Set-TestDomain', data.response.name);
                assert.equal('Variable-Set-UUID-1', data.response.id);
                assert.equal('variableset', data.response.type);
                assert.equal('This Variable Set is system-provided.', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.metadata);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getVariableSet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllVlanTagGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllVlanTagGroup(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllVlanTagGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllVlanTagGroupOverride - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllVlanTagGroupOverride(objectContainerUUID, objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllVlanTagGroupOverride', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVlanTagGroupOverride - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVlanTagGroupOverride(objectObjectId, objectContainerUUID, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(false, data.response.overridable);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getVlanTagGroupOverride', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectUpdateVlanTagGroupBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'DOMAIN',
          state: false
        },
        timestamp: 5
      },
      objects: [
        {
          metadata: {
            lastUser: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            domain: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            readOnly: {
              reason: {},
              state: true
            },
            timestamp: 1
          },
          data: {
            startTag: 5,
            endTag: 5,
            type: 'string'
          },
          overridable: true,
          name: 'string',
          description: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          overrides: {
            parent: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            },
            target: {
              name: 'string',
              links: {},
              id: 'string',
              type: 'string'
            }
          },
          id: 'string',
          type: 'string',
          version: 'string'
        }
      ],
      literals: [
        {
          startTag: 7,
          endTag: 5,
          type: 'string'
        }
      ],
      overridable: true,
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      id: 'string',
      type: 'string',
      version: 'string',
      overrideTargetId: 'string'
    };
    describe('#updateVlanTagGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateVlanTagGroup(objectObjectId, objectDomainUUID, objectUpdateVlanTagGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'updateVlanTagGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVlanTagGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVlanTagGroup(objectObjectId, null, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, Array.isArray(data.response.objects));
                assert.equal(true, Array.isArray(data.response.literals));
                assert.equal(true, data.response.overridable);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.overrideTargetId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getVlanTagGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllVlanTag - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllVlanTag(objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllVlanTag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllVlanOverride - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllVlanOverride(objectContainerUUID, objectDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('object', typeof data.response.paging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getAllVlanOverride', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVlanOverride - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVlanOverride(objectObjectId, objectContainerUUID, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, data.response.overridable);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.overrides);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getVlanOverride', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectUpdateVlanTagBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'SYSTEM',
          state: false
        },
        timestamp: 5
      },
      data: {
        startTag: 1,
        endTag: 1,
        type: 'string'
      },
      overridable: false,
      name: 'string',
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      overrides: {
        parent: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        },
        target: {
          name: 'string',
          links: {},
          id: 'string',
          type: 'string'
        }
      },
      id: 'string',
      type: 'string',
      version: 'string',
      overrideTargetId: 'string'
    };
    describe('#updateVlanTag - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateVlanTag(objectObjectId, objectDomainUUID, objectUpdateVlanTagBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'updateVlanTag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVlanTag - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVlanTag(objectObjectId, null, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.data);
                assert.equal('VlanTag', data.response.type);
                assert.equal('object', typeof data.response.overrides);
                assert.equal(true, data.response.overridable);
                assert.equal(' ', data.response.description);
                assert.equal('pvs_vlan', data.response.name);
                assert.equal('vlanObjectUUID', data.response.id);
                assert.equal('object', typeof data.response.metadata);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getVlanTag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deploymentDomainUUID = 'fakedata';
    const deploymentCreateDeploymentRequestBodyParam = {
      metadata: {
        lastUser: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        task: {
          taskType: 'DEVICE_DEPLOYMENT',
          subTasks: [
            {
              message: 'string',
              target: {},
              status: 'string'
            }
          ],
          name: 'string',
          description: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string',
          message: 'string',
          status: 'string'
        },
        domain: {
          name: 'string',
          links: {
            parent: 'string',
            self: 'string'
          },
          id: 'string',
          type: 'string'
        },
        readOnly: {
          reason: 'SYSTEM',
          state: true
        },
        timestamp: 4
      },
      forceDeploy: true,
      name: 'string',
      deviceList: [
        'string'
      ],
      ignoreWarning: false,
      description: 'string',
      links: {
        parent: 'string',
        self: 'string'
      },
      id: 'string',
      type: 'string',
      version: 'string'
    };
    describe('#createDeploymentRequest - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDeploymentRequest(deploymentDomainUUID, deploymentCreateDeploymentRequestBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadata);
                assert.equal(true, data.response.forceDeploy);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.deviceList));
                assert.equal(true, data.response.ignoreWarning);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Deployment', 'createDeploymentRequest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeployableDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDeployableDevice(deploymentDomainUUID, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowermanagementcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Deployment', 'getDeployableDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFTDBridgeGroupInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteFTDBridgeGroupInterface(devicesObjectId, devicesContainerUUID, devicesDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'deleteFTDBridgeGroupInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFTDEtherChannelInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteFTDEtherChannelInterface(devicesObjectId, devicesContainerUUID, devicesDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'deleteFTDEtherChannelInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFPLogicalInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteFPLogicalInterface(devicesObjectId, devicesContainerUUID, devicesDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'deleteFPLogicalInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInlineSet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteInlineSet(devicesObjectId, devicesContainerUUID, devicesDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'deleteInlineSet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFTDRedundantInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteFTDRedundantInterface(devicesObjectId, devicesContainerUUID, devicesDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'deleteFTDRedundantInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIPv4StaticRouteModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteIPv4StaticRouteModel(devicesObjectId, devicesContainerUUID, devicesDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'deleteIPv4StaticRouteModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIPv6StaticRouteModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteIPv6StaticRouteModel(devicesObjectId, devicesContainerUUID, devicesDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'deleteIPv6StaticRouteModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFTDSubInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteFTDSubInterface(devicesObjectId, devicesContainerUUID, devicesDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'deleteFTDSubInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualSwitch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteVirtualSwitch(devicesObjectId, devicesContainerUUID, devicesDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'deleteVirtualSwitch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDevice(devicesObjectId, devicesDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'deleteDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFTDHAInterfaceMACAddresses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteFTDHAInterfaceMACAddresses(deviceHAPairsObjectId, deviceHAPairsContainerUUID, deviceHAPairsDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceHAPairs', 'deleteFTDHAInterfaceMACAddresses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFTDHADeviceContainer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteFTDHADeviceContainer(deviceHAPairsObjectId, deviceHAPairsDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceHAPairs', 'deleteFTDHADeviceContainer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUpgradePackage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteUpgradePackage(updatesObjectId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Updates', 'deleteUpgradePackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRESTIncident - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteRESTIncident(intelligenceObjectId, intelligenceDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Intelligence', 'deleteRESTIncident', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRESTTidSource - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteRESTTidSource(intelligenceObjectId, intelligenceDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Intelligence', 'deleteRESTTidSource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDeviceGroup(deviceGroupsObjectId, deviceGroupsDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'deleteDeviceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExternalLookup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteExternalLookup(integrationObjectId, integrationDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Integration', 'deleteExternalLookup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePacketAnalyzerDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deletePacketAnalyzerDevice(integrationObjectId, integrationDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Integration', 'deletePacketAnalyzerDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAccessRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteAccessRule(policyObjectId, policyContainerUUID, policyDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'deleteAccessRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHitCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteHitCount(policyFilter, policyContainerUUID, policyDomainUUID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowermanagementcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'deleteHitCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAccessPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteAccessPolicy(policyObjectId, policyDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'deleteAccessPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFTDAutoNatRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteFTDAutoNatRule(policyObjectId, policyContainerUUID, policyDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'deleteFTDAutoNatRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFTDManualNatRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteFTDManualNatRule(policyObjectId, policyContainerUUID, policyDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'deleteFTDManualNatRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFTDNatPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteFTDNatPolicy(policyObjectId, policyDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'deleteFTDNatPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVpnEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteVpnEndpoint(policyObjectId, policyContainerUUID, policyDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'deleteVpnEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFTDS2SVpnModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteFTDS2SVpnModel(policyObjectId, policyDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'deleteFTDS2SVpnModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePrefilterHitCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePrefilterHitCount(policyFilter, policyContainerUUID, policyDomainUUID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_firepowermanagementcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'deletePrefilterHitCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDNSServerGroupObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDNSServerGroupObject(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'deleteDNSServerGroupObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFQDNObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteFQDNObject(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'deleteFQDNObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHostObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteHostObject(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'deleteHostObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteICMPV4Object - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteICMPV4Object(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'deleteICMPV4Object', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteICMPV6Object - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteICMPV6Object(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'deleteICMPV6Object', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIKEv1IPsecProposal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteIKEv1IPsecProposal(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'deleteIKEv1IPsecProposal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIkev1PolicyObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteIkev1PolicyObject(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'deleteIkev1PolicyObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIKEv2IPsecProposal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteIKEv2IPsecProposal(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'deleteIKEv2IPsecProposal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIkev2PolicyObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteIkev2PolicyObject(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'deleteIkev2PolicyObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInterfaceGroupObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteInterfaceGroupObject(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'deleteInterfaceGroupObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteKeyChainObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteKeyChainObject(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'deleteKeyChainObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteNetworkGroup(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'deleteNetworkGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteNetworkObject(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'deleteNetworkObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePortObjectGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deletePortObjectGroup(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'deletePortObjectGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteProtocolPortObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteProtocolPortObject(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'deleteProtocolPortObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRangeObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteRangeObject(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'deleteRangeObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecurityZoneObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteSecurityZoneObject(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'deleteSecurityZoneObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSLAMonitorObjectModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteSLAMonitorObjectModel(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'deleteSLAMonitorObjectModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteURLGroupObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteURLGroupObject(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'deleteURLGroupObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteURLObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteURLObject(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'deleteURLObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVlanTagGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteVlanTagGroup(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'deleteVlanTagGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVlanTag - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteVlanTag(objectObjectId, objectDomainUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'deleteVlanTag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
