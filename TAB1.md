# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Firepower_management_center System. The API that was used to build the adapter for Firepower_management_center is usually available in the report directory of this adapter. The adapter utilizes the Firepower_management_center API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Cisco Firepower Management Center adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Cisco Firepower Management Center. With this adapter you have the ability to perform operations such as:

- Automate the building and management of Cisco network security solutions.
- Objects
- Policies

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
