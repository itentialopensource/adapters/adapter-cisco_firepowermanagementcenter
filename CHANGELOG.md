
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:40PM

See merge request itentialopensource/adapters/adapter-cisco_firepowermanagementcenter!17

---

## 0.4.3 [08-24-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-cisco_firepowermanagementcenter!15

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:56PM

See merge request itentialopensource/adapters/adapter-cisco_firepowermanagementcenter!14

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_20:08PM

See merge request itentialopensource/adapters/adapter-cisco_firepowermanagementcenter!13

---

## 0.4.0 [05-15-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/security/adapter-cisco_firepowermanagementcenter!12

---

## 0.3.6 [03-27-2024]

* Changes made at 2024.03.27_13:45PM

See merge request itentialopensource/adapters/security/adapter-cisco_firepowermanagementcenter!11

---

## 0.3.5 [03-21-2024]

* Changes made at 2024.03.21_14:49PM

See merge request itentialopensource/adapters/security/adapter-cisco_firepowermanagementcenter!10

---

## 0.3.4 [03-13-2024]

* Changes made at 2024.03.13_11:09AM

See merge request itentialopensource/adapters/security/adapter-cisco_firepowermanagementcenter!9

---

## 0.3.3 [03-11-2024]

* Changes made at 2024.03.11_16:16PM

See merge request itentialopensource/adapters/security/adapter-cisco_firepowermanagementcenter!7

---

## 0.3.2 [02-27-2024]

* Changes made at 2024.02.27_11:51AM

See merge request itentialopensource/adapters/security/adapter-cisco_firepowermanagementcenter!6

---

## 0.3.1 [12-25-2023]

* update axios and metadata

See merge request itentialopensource/adapters/security/adapter-cisco_firepowermanagementcenter!5

---

## 0.3.0 [11-10-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-cisco_firepowermanagementcenter!3

---

## 0.2.1 [10-26-2023]

* add metadata file

See merge request itentialopensource/adapters/security/adapter-cisco_firepowermanagementcenter!4

---

## 0.2.0 [05-27-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/security/adapter-cisco_firepowermanagementcenter!2

---

## 0.1.1 [03-03-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/security/adapter-cisco_firepowermanagementcenter!1

---
