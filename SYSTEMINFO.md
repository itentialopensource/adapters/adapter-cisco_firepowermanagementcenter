# Cisco Firepower Management Center

Vendor: Cisco
Homepage: https://www.cisco.com/

Product: Firepower Management Center (FMC)
Product Page: https://www.cisco.com/site/us/en/products/security/firewalls/firewall-management-center/index.html

## Introduction
We classify Cisco Firepower Management Center into the Security domain as it provides the capability to get information about and make changes to security devices, policies, rules, and systems.

"Cisco Firepower Management Center provides complete and unified management over firewalls, intrusion prevention, URL filtering, application control, URL filtering, and advanced malware protection." 

## Why Integrate
The Cisco Firepower Management Center adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Cisco Firepower Management Center. With this adapter you have the ability to perform operations such as:

- Automate the building and management of Cisco network security solutions.
- Objects
- Policies

## Additional Product Documentation
The [API documents for Cisco Firepower Management Center](https://www.cisco.com/c/en/us/support/security/defense-center/products-programming-reference-guides-list.html)